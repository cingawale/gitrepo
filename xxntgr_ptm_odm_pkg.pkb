create or replace PACKAGE BODY      xxntgr_ptm_odm_pkg
IS
-----
-- +********************************************************************+
-- *Name    : XXNTGR_PTM_ODM_PKG PACKAGE                                *
-- *Purpose : This program is used to perform business validations, and *
-- *          import shipment data from ODM into PTM.                   *
-- *                                                                    *
-- *Change Record:                                                      *
-- *===============                                                     *
-- *Version   Date          Author             Remarks                  *
-- *========  ===========  ==============   ============================*
-- *1.0    31-OCT-2011  Roopak Rai        Created*
-- *1.1    25-JUL-2012  Geetha Pathapati  Added Davinci related
-- *                                      procedures
-- *       07-AUG-12    Geetha            Delete ASNs without trim in ASNMISS PROC*
-- *       13-AUG-12    Geetha            Archive data from STG table even when DCs are disabled*
-- *                                      XXNTGR_PTM_DC_GENSNFILE PROC updated*
-- *       22-AUG-12    Geetha            WO# 00132267 - Set last_update_date to SYSDATE on table update etc*
-- *       04-DEC-12    Geetha            Serial Number Validation to accept 15 chars in length as well.*
-- *       16-JAN-12    geetha            Foxconn sending new attributes - used attribute6,7 and 8
-- *       16-APR-13    Geetha            Enable attribute columns to map to xxntgr_ptm_item_instances table
-- *       01-JUL-13    Geetha            Added Davinci enhancements phase II changes
-- *       05-SEP-13    Geetha            Enabled attribute columns to DSV file extract *
-- *       11-DEC-2013  Geetha            Inserting all APL N10 shipments to repository table *
-- *       30-DEC-13    Geetha            Generate Po Receipt file and send to APL, same as DSV. *
-- *                                           DC ship proc modified to take dc name as a parameter *
-- *       10-Jan-2014  Geetha            DC ship procedure fixed to avoid processing APL shipment deliveries
-- *2.0    06-MAR-2014  Geetha            Exclude AIRCARD items for Outbound file creation.
-- *2.0    07-MAR-2014  Chandrakant       Move AC Data from xxntgr_ptm_dc_shipment_stg to xxntgr_ptm_ac_sfs_stg
-- *2.1    08-APR-2014  Geetha            Fixed issue with loading to repository table when there are multiple SKUs in ASN *
-- *2.2    12-APR-2014  Poonam            Fixed issues related to SN length Validation
-- *2.3    14-MAY-2014  Geetha            Added new procedure to send email notification for successfully processed PTM ODM files *
-- *2.4    20-MAY-2014  Geetha            Added a seperate lookup codes for storing email list for sending successful emails *
-- *2.5    07-JUL-2014  Geetha            Added call to xxntgr_insert_odm_file_header *
-- *2.6    14-JUL-2014  Geetha            Fixed issue - retain leading zeroes in MACIDs. *
-- *2.7    14-AUG-2014  Geetha            Validate attributes based on item setup,trim asn number, allow rework items, *
-- *                                      allow delayed ptm files, notify if manufacturer name is incorrect*
-- *2.8    13-OCT-2014  mmore             Added Procedure to log When Other erros to netgear support
-- *2.9    05-NOV-2014  mmore             XXNTGR_PTM_ODM_SHIP in UPDATE mode set end_date_active to null in notification table
-- *3.0    20-NOV-2014  mmore             Added column names in INSERT statement of table XXNTGR_PTM_ODM_SHIP_NOTIF
-- *3.1    20-NOV-2014  PPATIL             Added Bill-To Customer Name in Email Subject Lines *
-- *3.2    21-NOV-2014  PPATIL             Enhancement done for logic Related to Delivery Detail ID
-- *3.3    01-DEC-2014  PPATIL             Serial number count is compared against picked_qty or shipped_qty
-- *3.4    05-DEC-2014  mmore             Added ORAERROR notifications and restrict AIRCARD SKU processing from this interface.
-- *3.5    06-JAN-2015  mmore             INC00240166 - ODM Import program fix.
-- *3.6    08-JAN-2015  Poonam            Logic change for DSV files failing with error DUPLICATE SN2.
-- *3.7    24-FEB-2015  mmore             INC00245914 - Fix the issue with the DSV PTMDCI Inbound Program
-- *3.8    10-MAR-2015  mmore             INC00247851 - Modify the error message from DC with Netgear SKU Name
-- *3.9    01-APR-2015  mmore             INC00251287 - Fix issue with the DC Shipment program for DSV
-- *3.10    11-MAR-2015  PPATIL            INC00217543 -code changes for Update functionality of PTM and Davinci tables
--  *3.11    20-APR-2015  PPATIL            INC00254148 - Code changes to process Rework units file
--  *3.12    06-MAY-2015  mmore            INC00255923 - Fix ODM Shipment Program to handle same ASN numbers from different manufacturers
--  *3.13    21-MAY-2015  PPATIL            INC00257544 --LOG_ERROR_EMAIL Changes
--  *3.14    28-MAY-2015  PPATIL              Removed query not in use causing error
--   *3.15   18-JUN-2015   PPATIL            Enhancement to DC Shipment program to exclude non-serialized items based on a lookup
--   *3.16   19-JUN-2015   PPATIL            Enhancement to DC Shipment program to exclude bundled items based on a lookup
--   *3.17   07-JUL-2015   Subbareddy        Enhancement to In case of short shipment, we need to defer the processing of the PTMDCI files till the order line is ship confirmed
--  *3.18    11-AUG-2015   SKONANKI            Added validations for Pallet ID and Carton ID.
-- * 3.19    24-SEP-2015   RSANTHANAM       Passed the manufacturer name to procedures to get po_header_id and shipment_header_id
--  *4.0    06-OCT-2015  Subbareddy          Added procedure of SKU Serialization Automation Program
--  *4.1    18-NOV-2015   SKONANKI         Added new functionality to insert data into xxntgr_ptm_dc_file_header
--  *4.2    14-Oct-2015   rsanthanam       Added Kerry to the PTMDCO file generation
--  *4.3    27-JAN-2016  SKONANKI         Enhancement to sending mail for invalid manufacturer name
--  *4.4    31-MAR-2016  SKONANKI         Enhancement for Arlo program to insert multiple serial numbers in item instance table
--  *4.5    06-JUN-2016  SKONANKI          INC00316523 Fix the DC Shipment program to exclude items in the value set if they are disabled
--  *4.6    28-JUN-2016  rsanthanam        INC00317928 Fix the DSV inbound program to validate multiple Delivery IDs in a file
--  *4.7    27-MAY-2016   SKONANKI         INC00281389 Added serial number validation logic for DC Shipment program
--  *4.8    27-MAY-2016  rsanthanam/SKONANKI    INC00281389 Added Serial number validation logic for ODM Shipment program
--  *5.0    30-MAY-2016  SKONANKI           INC00281389  Serial Number Validation Logic
--  *5.1    26-sep-2016  rsanthanam         INC00330472 Capture outbound serial numbers for Hard Drives
--  *5.2    13-Oct-2016  Amit Bhatka        INC00343060 Chnages for PTM validation - Multiple files for same ASN NUmber
--  *5.3    21-Dec-2016  rsanthanam         INC00358455 Delete header record created for unprocessed PTMDCI files (deliveries that were not ship confirmed yet)
--  *5.4    20-FEB-2017  Amit Bhatka        INC00371507 Added new logic to Validate duplicate Pallet and Carton id
--  *5.5    17-MAR-2017  Amit Bhatka        INC00376690 Added New Logic to check Po Type is NON-TRADE
--  *5.6    17-MAR-2017  Amit Bhatka        Change in Logic to check Po Type is NON-TRADE as we have Multiplae PO Against One ASN
--  *5.7    03-MAY-2017  Amit Bhatka        INC00390456 Validate PO DATA In PO Base Tables
--  *5.8    25-MAY-2017  Amit Bhatka        INC00396443 Remove INVALID_SN_LENGTH oll Validation As We have New Serial Number Validation
--  *5.9    04-AUG-2017  Amit Bhatka        INC00414621 Addeing Filter condition to Exclude Refurbished SKU File
--  *6.0    02-NOV-2017  Ram Santhanam      INC00438480 PTM Bundle Enhancement
--  *6.1    21-DEC-2017  Ram Santhanam      INC00452462 Fix issue with processing of Non-Trade PO PTM files
--  *6.2    24-JAN-2018  Ram Santhanam      INC00459886 Fix issues with PTM ODM Inbound Program
--  *6.3    26-JAN-2018  Ram Santhanam
--  *6.4    05-MAR-2018  Amit Bhatka        INC00469372 PTM Enhancement to support Coupon Code
--  *6.5    14-AUG-2018  Amit Bhatka        INC00518430 PTM Enable DaVinci Validation depends on Profile Option
--  *6.6    29-AUG-2018  Amit Bhatka        INC00521684 Add SPKR_BOX_SN and LICENSE_KEY column to PTM files
--  *6.7    22-NOV-2018  Amit Bhatka		INC00537942 And INC00540085 Handle ORA Error for Both ODM and DC
--  *6.8    29-NOV-2018  Amit Bhatka		INC00540583 Add column MEURAL_PROD_KEY to PTM file
--  *6.9    13-FEB-2019  Madhuri Joshi		INC00552461  Add column RFID to PTM file
-- +***************************************************************************************************************************************+
------
---
-- GP START --
  serialnumber_in_master        VARCHAR2 (100) := 'Serial number is already in SERIAL_MASTER';
  serialnumber_in_snlabel       VARCHAR2 (100) := 'Serial number is already in SN_LABEL';
  ac_found_not_expected         VARCHAR2 (100) := 'Access Code found where not expected';
  ac_not_found_expected         VARCHAR2 (100) := 'Access Code expected not found';
  ac_assigned_to_another        VARCHAR2 (100) := 'Access Code was assigned to serial number';
  ac_not_found                  VARCHAR2 (100)
                                    := 'Access Code was not present in ASSIGNED_ACCESS_CODES table';
  mac_incorrect_number_of_macs  VARCHAR2 (100) := 'Incorrect number of MAC addresses in file';
  mac_found_when_not_expected   VARCHAR2 (100) := 'Found MAC addresses when none was expected';
  mac_not_found_expected        VARCHAR2 (100) := 'MAC ID was not found when expected';
  mac_array_incorrect_no_of_macs VARCHAR2 (100) := 'Incorrect number of MAC IDs in Array';
  mac_is_not_hex                VARCHAR2 (100) := 'MAC string is not a hex string';
  mac_invalid_length            VARCHAR2 (100) := 'MAC has incorrect number of characters';
  mac_prefix_not_equal          VARCHAR2 (100) := 'OUI change not allowed in MAC ID set';
  mac_not_sequential            VARCHAR2 (100) := 'MAC addresses not sequential';
  assigned_mac_table_invalid    VARCHAR2 (100)
                                      := 'MAC IDs are Missing or assigned to another serial number';
  rev_not_found                 VARCHAR2 (100) := 'Revision was not found for material';
  wep_found_not_expected        VARCHAR2 (100) := 'WEP Key found where not expected';
  wep_length_error              VARCHAR2 (100) := 'Invalid Length For WEP Key';
  wep_not_hex                   VARCHAR2 (100) := 'WEP Key is not in proper hex format';
  wpa_found_not_expected        VARCHAR2 (100) := 'WPA Key found where not expected';
  wpa_not_found_expected        VARCHAR2 (100) := 'WPA Key not found where expected';
  wpa_length_error              VARCHAR2 (100) := 'Invalid Length For WPA Key';
  wpa_not_hex                   VARCHAR2 (100) := 'WPA Key is not in proper hex format';
  wpa_invalid_char              VARCHAR2 (100) := 'Invalid character found in WPA Key';
  wpa_char_duplicate_error      VARCHAR2 (100)
                                        := 'Found same character more than two times in a row. Ch=';
  wpa_minimum_chars             VARCHAR2 (100)
                                 := 'WPA did not have required minumum number of unique characters';
  wpa_assigned_to_another       VARCHAR2 (100) := 'WPA was assigned to serial number ';
  ssid_ptype_mismatch           VARCHAR2 (100) := 'SSID does not match calculated type P ssid';
  -- added in ver 1.0.9
  ssid_found_not_expected       VARCHAR2 (100) := 'SSID found where not expected';
  ssid_not_found_expected       VARCHAR2 (100) := 'SSID not found where expected';
  ssid_invalid_char             VARCHAR2 (100) := 'Invalid character found in SSID String';
  ssid_banned_chars             VARCHAR2 (100) := 'SSID contains blocked words';
  ssid_assigned_to_serialnumber VARCHAR2 (100) := 'SSID is assigned to serial number';
  ssid_build_type_pssid_failure VARCHAR2 (100) := 'Could not build SSID for P ssid';
  wifi_uid_found_not_expected   VARCHAR2 (100) := 'Wifi UID found when not expected';
  wifi_uid_not_found_expected   VARCHAR2 (100) := 'Wifi UID not found when expected';
  wifi_data_assigned_to_another VARCHAR2 (100) := 'Wifi data assigned to another serial number';
  wifi_data_uid_not_found       VARCHAR2 (100) := 'Wifi data U_ID  was not found in table';
-- GP 30-OCT-12
  ssid_invalid                  VARCHAR2 (50) := 'Invalid SSID found';
  ssid1_invalid                 VARCHAR2 (50) := 'Invalid SSID 5G found';
  ssid1_assigned_to_serialnumber VARCHAR2 (50) := 'SSID 5G is assigned to serial number';
  ssid1_found_not_expected      VARCHAR2 (50) := 'SSID 5G found where not expected';
  ssid1_not_found_expected      VARCHAR2 (50) := 'SSID 5G not found where expected';
  ssid1_build_type_pssid_failure VARCHAR2 (50) := 'Could not build SSID 5G for ''P'' ssid';
  ssid1_ptype_mismatch          VARCHAR2 (50)
                                             := 'SSID 5G does not match calculated type ''P'' ssid';
  ssid1_invalid_char            VARCHAR2 (50) := 'Invalid character found in SSID 5G String';
  ssid1_banned_chars            VARCHAR2 (50) := 'SSID 5G contains blocked words';

-- GP END
  PROCEDURE xxntgr_ptm_odm_ship (
    errbuf                      OUT      VARCHAR2
   ,retcode                     OUT      VARCHAR2
   ,p_input_file_id             IN       NUMBER
   ,p_asn_no                    IN       VARCHAR2
   ,p_ignr_dup_sn                        VARCHAR2
   ,p_deactivate_orig_sn                 VARCHAR2
   ,p_match_to_sn                        VARCHAR2
   ,p_delete_asn                         VARCHAR2
   ,p_split_asn                          VARCHAR2
  )
  AS
--To get asn_number (invoice number if asn is null) for mandatory field validation
    CURSOR get_asn_inv
    IS
      SELECT DISTINCT asn_number, invoice_number, UPPER (manufacturer_name) manufacturer_name
                     ,file_name, input_file_id
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE NVL (process_status, 'U') = 'U'
                  AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1))
                  AND NVL (asn_number, '1') = NVL (p_asn_no, NVL (asn_number, '1'))
                  AND UPPER (SUBSTR (file_name, 1, 6)) <> 'PTMRFB'
             -------- condition added for change 5.9 Exclude Refurbished SKU File
      GROUP BY        asn_number
                     ,invoice_number
                     ,UPPER (manufacturer_name)
                     ,file_name
                     ,input_file_id;

--To get input_file_id, item_number for item level validation  01-AUG-2014
    CURSOR get_dist_items_inv (
      p_input_file_id                      NUMBER
    )
    IS
      SELECT DISTINCT asn_number, input_file_id, item_number
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE NVL (process_status, 'U') = 'U' AND input_file_id = p_input_file_id;

--To get dist errors after item level validation  01-AUG-2014
    CURSOR get_dist_errors (
      p_input_file_id                      NUMBER
    )
    IS
      SELECT DISTINCT process_message
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE (process_status = 'E') AND input_file_id = p_input_file_id;

--get ASN_NUMBER to process
    CURSOR get_asn_number
    IS
      SELECT DISTINCT asn_number, UPPER (manufacturer_name), file_name, input_file_id
                     ,odm_name         --,po_number --- added odm_name 4.3  Remove po_number for 5.6
                 FROM xxntgr_ptm_odm_shipment_stg
--                WHERE (process_status = 'U' OR process_status IS NULL)
      WHERE           process_status = 'V'
                  AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1))
                  AND NVL (asn_number, '1') = NVL (p_asn_no, NVL (asn_number, '1'))
                  AND UPPER (SUBSTR (file_name, 1, 6)) <> 'PTMRFB'
             --------  condition added for change 5.9 Exclude Refurbished SKU File
      GROUP BY        asn_number, UPPER (manufacturer_name), file_name, input_file_id, odm_name;

    --,po_number; Remove po_number for 5.6

    -- to validate serial number for duplicacy and set process_mode
    CURSOR set_process_mode (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT asn_number, serial_number_product, xfactory_date, top_serial_number, manufacturer_name
            ,processed_flag                                                   --added by ppatil 3.10
        FROM xxntgr_ptm_odm_shipment_stg
       WHERE asn_number = p_asn_num
         AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
--         AND (process_status = 'U' OR process_status IS NULL);
         AND process_status = 'V';

-- Validate quantity_shipped against shipment and item
    CURSOR validate_shipped_qty (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT DISTINCT invoice_number, item_number
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE 1 = 1
                  AND asn_number = p_asn_num
                  AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
                  AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
--AND top_serial_number IS NULL;
                  AND NVL (top_serial_number, serial_number_product) = serial_number_product;

    -- 6.0 Get count of only the parent SN

    -- for po validation
    CURSOR validate_po (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT DISTINCT po_number
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_num
                  AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
                  AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

--get items to validate
    CURSOR validate_items (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT DISTINCT item_number
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_num
                  AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
                  AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

--AND top_serial_number != NULL;

    --to get any missing item number for asn, from pallet file
    CURSOR get_missed_item (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT DISTINCT msi.segment1
                 FROM rcv_shipment_headers rsh
                     ,rcv_shipment_lines rsl
                     ,mtl_system_items_b msi
                     ,fnd_lookup_values flv
                WHERE rsh.shipment_num = p_asn_num
                  AND rsh.shipment_header_id = rsl.shipment_header_id
                  AND rsl.item_id = msi.inventory_item_id
                  AND rsl.to_organization_id = msi.organization_id
                  AND flv.lookup_code = TO_CHAR (rsh.vendor_id)
                  AND flv.meaning = UPPER (p_manufacturer)
                  AND flv.lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                  AND flv.enabled_flag = 'Y'                    -- GP 23-AUG-12 Added this condition
/*AND msi.item_type IN
                  (SELECT lookup_code
                  FROM fnd_lookup_values
                  WHERE lookup_type = 'XXNTGR_PTM_ODM_ITEM_TYPE'
                  AND enabled_flag='Y')*/
      MINUS
      SELECT DISTINCT item_number
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE 1 = 1
                  AND asn_number = p_asn_num
                  AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
                  AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                  AND NVL (top_serial_number, serial_number_product) = serial_number_product; -- 6.0

--get the duplicate SNs within the ASN_NUMBER
    CURSOR get_duplicate_sn (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT   serial_number_product
          FROM xxntgr_ptm_odm_shipment_stg
         WHERE asn_number = p_asn_num
           AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
           AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
      GROUP BY serial_number_product
        HAVING COUNT (serial_number_product) > 1;

--added by ppatil 3.10 start
--check the duplicate SNs if p_duplicate_sn ='N'
    CURSOR check_duplicate_sn (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT asn_number, serial_number_product, xfactory_date, top_serial_number, manufacturer_name
            ,processed_flag
        FROM xxntgr_ptm_odm_shipment_stg
       WHERE asn_number = p_asn_num
         AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
         AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

--added by ppatil 3.10 end

    --Validate top_serial_number with parent serial_number
    CURSOR validate_top_serial (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT item_number, top_serial_number
        FROM xxntgr_ptm_odm_shipment_stg
       WHERE asn_number = p_asn_num
         AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
         AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
         AND top_serial_number IS NOT NULL
         AND top_serial_number NOT IN (
               SELECT serial_number_product
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_num
                  AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
                  AND NVL (process_status, 'NULL') NOT IN ('P', 'S'));

    --Get distinct items to handle ASN with multiple items
    CURSOR get_dist_items (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT DISTINCT item_number
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_num
                  AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
                  AND process_status = 'P';

---
    CURSOR get_ptm_data (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
     ,p_item                               VARCHAR2
    )
    IS
      SELECT *
        FROM xxntgr_ptm_odm_shipment_stg
       WHERE asn_number = p_asn_num
         AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
         AND item_number = p_item
         AND process_status = 'P';

------
-- cursor to identify duplicates
    CURSOR identify_dup_asn (
      p_asn_num                            VARCHAR2
    )                                                                         -- 6.2 Added parameter
    IS
      SELECT   asn_number, manufacturer_name, MAX (input_file_id) max_batch
              ,COUNT (DISTINCT input_file_id) batch_count
          FROM xxntgr_ptm_odm_shipment_stg
         WHERE (process_status = 'U' OR process_status IS NULL) AND asn_number = p_asn_num
      GROUP BY asn_number, manufacturer_name;

------
--cursor to split the asn
    CURSOR get_asn_to_split (
      p_asn                                VARCHAR2
    )
    IS
      SELECT   pallet_id, master_carton_id, record_row_number, asn_number, invoice_number
          FROM xxntgr_ptm_odm_shipment_stg
         WHERE asn_number = p_asn
      ORDER BY pallet_id, master_carton_id;

--------------------------------
-- GP START 21-MAY-2012
-- get legacy items to validate
    CURSOR validate_items_davinci (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT DISTINCT item_number, item_revision, assembly_number, processed_flag
                     ,COUNT (*)                                               --added by ppatil 3.10
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_num
                  AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
                  AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                  AND processed_flag = 'I'                                    --added by ppatil 3.10
                  AND UPPER (manufacturer_name) IN (
                             SELECT UPPER (meaning)
                               FROM fnd_lookup_values
                              WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                    AND enabled_flag = 'Y')
             GROUP BY processed_flag, item_number, item_revision, assembly_number;

-- get attributes to check for 90 level davinci item ta_number
    CURSOR validate_tanumber_davinci (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
     ,p_item_number                        VARCHAR2
    )
    IS
      SELECT item_number, assembly_number, asn_number, serial_number_product, mac_address_product
            ,item_revision, assembly_revision, access_code, wep_key, wpa_key, wifi_id, primary_ssid
            ,ssid1
        FROM xxntgr_ptm_odm_shipment_stg
       WHERE asn_number = p_asn_num
         AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
         AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
         AND item_number = p_item_number;

--
    CURSOR get_dist_items_legacy (
      p_asn_num                            VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT DISTINCT item_number, processed_flag, COUNT (*)                  --added by ppatil 3.10
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_num
                  AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
                  AND process_status = 'P'
                  AND processed_flag = 'I'                                    --added by ppatil 3.10
                  AND UPPER (manufacturer_name) IN (
                             SELECT UPPER (meaning)
                               FROM fnd_lookup_values
                              WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                    AND enabled_flag = 'Y')
             GROUP BY processed_flag, item_number;

--
    CURSOR bell_item_cur (
      p_asn                                VARCHAR2
     ,p_manufacturer                       VARCHAR2
     ,p_item                               VARCHAR2
    )
    IS
      SELECT   imei_number
          FROM xxntgr_ptm_odm_shipment_stg
         WHERE asn_number = p_asn
           AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
           AND item_number = p_item
           AND UPPER (manufacturer_name) IN (
                             SELECT UPPER (meaning)
                               FROM fnd_lookup_values
                              WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                    AND enabled_flag = 'Y')
           AND item_number IN (SELECT lookup_code
                                 FROM fnd_lookup_values
                                WHERE lookup_type = 'XXNTGR_PTM_BELL_ITEMS' AND enabled_flag = 'Y')
      ORDER BY TO_NUMBER (imei_number);

-- Cursor to find new ranges based on the minimum and maximum imei numbers
    CURSOR v_ranges (
      p_minv                               NUMBER
     ,p_maxv                               NUMBER
    )
    IS
      SELECT   1 AS batch_id
              ,DECODE
                     (CASE
                        WHEN max_imei_number + 1 < p_minv
                          THEN 1
                        WHEN min_imei_number > p_minv
                        AND LAG (min_imei_number) OVER (ORDER BY min_imei_number, max_imei_number) IS NULL
                          THEN 1
                        WHEN max_imei_number + 1 =
                              LEAD (min_imei_number) OVER (ORDER BY min_imei_number
                              ,max_imei_number)
                          THEN 3                                                          -- 160-160
                        WHEN max_imei_number + 1 > p_minv
                          THEN 2
                      -- AND max_imei_number+1 != LEAD(min_imei_number) OVER (ORDER BY min_imei_number,max_imei_number)
                      END
                     ,1, p_minv
                     ,2, max_imei_number + 1
                     ,3, NULL
                     ) min_number
              ,DECODE
                     (CASE
                        WHEN min_imei_number > p_minv
                        AND LAG (min_imei_number) OVER (ORDER BY min_imei_number, max_imei_number) IS NULL
                          THEN 1
                        WHEN max_imei_number =
                              LEAD (min_imei_number - 1) OVER (ORDER BY min_imei_number
                              ,max_imei_number)
                          THEN 2
                      END
                     ,1, min_imei_number - 1
                     ,2, NULL
                     ,NVL (LEAD (min_imei_number - 1) OVER (ORDER BY min_imei_number
                           ,max_imei_number)
                          ,p_maxv
                          )
                     ) AS max_number
              ,SYSDATE, 1 AS created_by
          FROM xxntgr_apps.xxntgr_bellmobility_all_ranges
         WHERE min_imei_number < p_maxv
      ORDER BY min_imei_number, max_imei_number;

-- find duplicate imei numbers within ASNs
    CURSOR imei_dup_stg_cur (
      p_asn_number                         VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT   imei_number
          FROM xxntgr_ptm_odm_shipment_stg
         WHERE asn_number = p_asn_number
           AND UPPER (manufacturer_name) = UPPER (p_manufacturer)
           AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
      GROUP BY imei_number
        HAVING COUNT (imei_number) > 1;

-- GP END

    --ppatil start 3.10
    CURSOR get_product_reposit_data (
      p_serial_number_product              VARCHAR2
     ,p_item                               VARCHAR2
    )
    IS
      SELECT *
        FROM xxntgr_product_id_repository
       WHERE serial_number_product = p_serial_number_product AND item_number = p_item;

--ppatil end 3.10

    -- 4.8 Cursor for SN validation logic
    CURSOR cur_get_item_sn (
      p_asn                                VARCHAR2
     ,p_input_file_id                      NUMBER
     ,p_item_number                        VARCHAR2
    )
    IS
      SELECT *
        FROM xxntgr_ptm_odm_shipment_stg stg
       WHERE stg.asn_number = p_asn
         AND stg.input_file_id = p_input_file_id
         AND stg.item_number = p_item_number;

-- 4.8 End cursor

    -- 6.0 Cursor for SN validation logic at product ID level
    CURSOR cur_get_prod_id_sn (
      p_asn                                VARCHAR2
     ,p_input_file_id                      NUMBER
     ,p_product_id                         VARCHAR2
    )
    IS
      SELECT *
        FROM xxntgr_ptm_odm_shipment_stg stg
       WHERE stg.asn_number = p_asn
         AND stg.input_file_id = p_input_file_id
         AND apps.xxntgr_ptm_common_pkg.get_product_id (stg.assembly_number) = p_product_id;

-- 6.0 End cursor

    --
    v_asn_number                  VARCHAR2 (60);
    v_asn                         VARCHAR2 (60);
    v_asn_num                     VARCHAR2 (60);
    v_user_id                     NUMBER;
    v_errmsg                      VARCHAR2 (9000);
    v_top_serial_no               VARCHAR2 (60);
    v_po_line_ref                 VARCHAR2 (150);
    v_po_header_id                NUMBER;
    v_po_line_id                  NUMBER;
    v_po_line_loc_id              NUMBER;
    v_shipment_header_id          NUMBER;
    v_shipment_line_id            NUMBER;
    v_item_no                     VARCHAR2 (40);
    v_item_num                    VARCHAR2 (40);
    v_check_item                  VARCHAR2 (40);
    v_quantity_shipped            NUMBER;
    v_container_num               VARCHAR2 (35);
    v_instance_id                 NUMBER;
--v_top_instance_id           NUMBER;
    v_inventory_item_id           NUMBER;
--v_child_item_id             NUMBER;
--v_parrent_item_num          VARCHAR2(40);
    v_start_date_active           DATE;
    v_last_trans_type             VARCHAR2 (100);
    v_warranty_begin_date         DATE;
    v_warranty_end_date           DATE;
    v_warranty_period             VARCHAR2 (30);
    v_install_date                DATE;
    v_installed_at_party_id       NUMBER;
    v_sold_to_customer_id         NUMBER;
    v_organization_id             NUMBER;
    v_subinv_code                 VARCHAR2 (100);
    v_trans_type                  VARCHAR2 (100);
    v_trans_source_type           VARCHAR2 (100);
    v_trans_source_header_id      NUMBER;
    v_trans_source_line_id        NUMBER;
    v_count_stg                   NUMBER;
    v_chk_sn                      NUMBER;
    ln_request_id                 NUMBER;
    v_proc_status                 VARCHAR2 (10);
    v_process_msg                 VARCHAR2 (3000);
    v_item_vldtn_error            VARCHAR2 (200);
    v_qty_rcvd_vldtn_error        VARCHAR2 (200);
    v_mailid                      VARCHAR2 (1000);
    v_reqstid                     NUMBER;
    v_mac_add                     NUMBER;
    v_mac_address                 VARCHAR2 (4000);
    v_other_macs                  VARCHAR2 (4000);
    v_ship_to_org_code            VARCHAR2 (60);
    v_duplicate_sn                VARCHAR2 (60);
    v_validate_fields             VARCHAR2 (32000);
    v_validate_fields1            VARCHAR2 (10);                                       --- mmore 3.4
    v_invoice                     VARCHAR2 (60);
    v_manufacturer                VARCHAR2 (60);
    v_error_message               VARCHAR2 (32000);
    v_invalid_asn                 VARCHAR2 (32000);
    v_invalid_mfg                 VARCHAR2 (32000);
    v_chk_duplicate_sn            VARCHAR2 (60);
    v_missed_item                 VARCHAR2 (60);
    v_asn_inv_no                  VARCHAR2 (60);
    v_num                         NUMBER;
    v_dist_item                   VARCHAR2 (40);
    v_process_mode                VARCHAR2 (10);
    v_dup_asn_sn                  VARCHAR2 (60);
    v_inst_id                     NUMBER;
    v_reason                      VARCHAR2 (32000);
    v_get_mail_error              VARCHAR2 (32000);
    v_po_number                   VARCHAR2 (60);
    v_po_num                      VARCHAR2 (60);
    v_mfg                         VARCHAR2 (60);
    v_vendor_id                   NUMBER;                                          -- GP 18-AUG-2014
    v_mfg_code                    NUMBER;                                          -- GP 18-AUG-2014
-- 6.1 commented below variables
--v_po_header_id_ch           NUMBER;
--v_po_line_id_ch             NUMBER;
--v_po_line_loc_id_ch         NUMBER;
--v_shipment_header_id_ch     NUMBER;
--v_shipment_line_id_ch       NUMBER;
--v_organization_id_ch        NUMBER;
--v_subinv_code_ch            VARCHAR2(100);
--v_sold_to_customer_id_ch    NUMBER;
--v_ship_to_org_code_ch       VARCHAR2(60);
--v_file_name                 VARCHAR2(100); -- mmore 3.4
    v_file_name                   xxntgr_ptm_odm_shipment_stg.file_name%TYPE;            --mmore 3.4
    v_aircard_item                VARCHAR2 (100);                                       -- mmore 3.4
    v_err_location                VARCHAR2 (4000);                                      -- mmore 3.4
    v_item_type                   VARCHAR2 (30);
    v_item_type_fg                VARCHAR2 (30);
    v_mac_item                    VARCHAR2 (30);
    v_mac_flg                     VARCHAR2 (10);
    v_comma_count                 NUMBER;
    v_split_asn                   VARCHAR2 (200);
    v_asn_without_split           VARCHAR2 (100);
    v_shipped_qty                 NUMBER;
    v_rec_count                   NUMBER;
    v_asn_count                   NUMBER;
    v_asn_1                       VARCHAR2 (60);
    v_asn_2                       VARCHAR2 (60);
    v_asn_3                       VARCHAR2 (60);
-- GP START
    v_davinci_item                VARCHAR2 (30);
    v_davinci_ta_item             VARCHAR2 (30);
    v_return_code                 VARCHAR2 (1);
    v_item_attributes_rec         serial_control%ROWTYPE;
    v_item_rev_rec                serial_control%ROWTYPE;
    v_ta_err_msg                  VARCHAR2 (32000);
    v_ta_err_msg_tl               VARCHAR2 (32000);
    v_serial_number               VARCHAR2 (30);
    v_imei_dup                    VARCHAR2 (10);
    v_input_file_id               NUMBER;
    v_bell_err_msg                VARCHAR2 (250);
    v_email_ranges                VARCHAR2 (3000);
    dup_imei_found                NUMBER := 0;
    bell_email_err                EXCEPTION;
    davinci_err                   EXCEPTION;
    v_davinci_item_rev            VARCHAR2 (10);
    v_duplicate_imei_stg          VARCHAR2 (60);
    v_duplicate_imei_ptm          VARCHAR2 (60);
    v_range_insert                VARCHAR2 (1);
    v_to_shipment_org             VARCHAR2 (25);
    v_add_input_file_id           NUMBER;
    l_input_file_id               NUMBER;
    l_sn_number                   VARCHAR2 (60);                              --added by ppatil 3.11
    v_shipment_count              NUMBER;                                              -- mmore 3.12
    v_odm_name                    VARCHAR2 (20);                                     ---skonanki 4.3
---lv_po_number      VARCHAR2(200)    ;-- Amit Bhatka 5.5  Commented For Change 5.6
    v_nontrade_po                 VARCHAR2 (1);                                   -- Amit Bhatka 5.5
    v_imei_number                 VARCHAR2 (50);
    i_cnt                         NUMBER := 0;
    j_cnt                         NUMBER := 0;
    count_no                      NUMBER := 0;
    minv                          NUMBER;
    maxv                          NUMBER;

    TYPE imei_num IS TABLE OF NUMBER
      INDEX BY BINARY_INTEGER;

    imei_num_array                imei_num;
-- GP END
    v_last_updated_by             VARCHAR2 (100);
    v_created_by                  VARCHAR2 (100);
    v_sn_exists_instance_id       NUMBER;

    TYPE numarray IS TABLE OF NUMBER
      INDEX BY BINARY_INTEGER;

    multimacs                     numarray;

    TYPE chararray IS TABLE OF VARCHAR2 (100)
      INDEX BY BINARY_INTEGER;

    multimacs_char                chararray;
    m                             NUMBER := 0;
    v_mac                         VARCHAR2 (100);
    n                             NUMBER := 0;
    temp                          NUMBER;
    k                             NUMBER := 0;
    pos                           NUMBER := 0;
    p_delim                       VARCHAR2 (1) := '|';
-- 4.8  Added variables to be used for serial number validation logic
    lv_wrong_format_sn            VARCHAR2 (32000);
    l_product_line                VARCHAR2 (40);
    l_product_category            VARCHAR2 (40);
    l_product_family              VARCHAR2 (40);
    l_sn_type                     VARCHAR2 (10);
    l_sn_err_meg                  VARCHAR2 (4000);

    TYPE g_varchar_tbl IS TABLE OF VARCHAR2 (50)
      INDEX BY VARCHAR2 (50);

    gvt_sn_item                   g_varchar_tbl;
--4.8 End Variables
    v_processed_asn               VARCHAR2 (1);
    v_validate_po                 NUMBER;
    l_bundle_sku                  NUMBER;
    l_bundle_flag                 VARCHAR2 (1);
    l_bundle_item_tbl             g_varchar_tbl;
    v_davinci_validation          VARCHAR (2);                            ----- Added for Change 6.5
    l_check_product_cfg           VARCHAR (1);                            ----- Added for Change 6.7
--------
  BEGIN                                                                                          --1
    v_reqstid := fnd_global.conc_request_id;
    xxntgr_debug_utl.LOG ('CONC REQUEST ID : ' || v_reqstid);
    fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' PTM ODM Start');

    IF p_delete_asn = 'Y'
    THEN
      fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Delete ASN is set');

      IF p_asn_no IS NOT NULL
      THEN
        --Delete records from PTM tables and exit
        BEGIN
          /*added by ppatil 3.10 FOR deleting data from DAVINCI tables start */
             /*  SELECT distinct input_file_id
               INTO  l_input_file_id
               FROM XXNTGR_PTM_PO_SHIPMENT_TRX
               WHERE asn_number = P_ASN_NO ;*/   --3.14 commented query by ppatil not in use
          DELETE FROM imec.sn_label
                WHERE sn IN (SELECT serial_number
                               FROM xxntgr_ptm_po_shipment_trx
                              WHERE asn_number = p_asn_no);

          IF SQL%FOUND
          THEN
            fnd_file.put_line (fnd_file.LOG
                              , 'no of records deleted from IMEC.SN_LABEL ' || SQL%ROWCOUNT
                              );
          END IF;

          DELETE FROM imec.subassbly
                WHERE serialnumber IN (SELECT serial_number
                                         FROM xxntgr_ptm_po_shipment_trx
                                        WHERE asn_number = p_asn_no);

          IF SQL%FOUND
          THEN
            fnd_file.put_line (fnd_file.LOG
                              , 'no of records deleted from IMEC.SUBASSBLY ' || SQL%ROWCOUNT
                              );
          END IF;

          DELETE FROM imec.assigned_wep
                WHERE serialnumber IN (SELECT serial_number
                                         FROM xxntgr_ptm_po_shipment_trx
                                        WHERE asn_number = p_asn_no);

          IF SQL%FOUND
          THEN
            fnd_file.put_line (fnd_file.LOG
                              , 'no of records deleted from IMEC.ASSIGNED_WEP ' || SQL%ROWCOUNT
                              );
          END IF;

          DELETE FROM imec.config_data
                WHERE serialnumber IN (SELECT serial_number
                                         FROM xxntgr_ptm_po_shipment_trx
                                        WHERE asn_number = p_asn_no);

          IF SQL%FOUND
          THEN
            fnd_file.put_line (fnd_file.LOG
                              , 'no of records deleted from IMEC.CONFIG_DATA ' || SQL%ROWCOUNT
                              );
          END IF;

          DELETE FROM imec.test_data
                WHERE serialnumber IN (SELECT serial_number
                                         FROM xxntgr_ptm_po_shipment_trx
                                        WHERE asn_number = p_asn_no);

          IF SQL%FOUND
          THEN
            fnd_file.put_line (fnd_file.LOG
                              , 'no of records deleted from IMEC.TEST_DATA ' || SQL%ROWCOUNT
                              );
          END IF;

          DELETE FROM imec.assigned_ssids
                WHERE sn IN (SELECT serial_number
                               FROM xxntgr_ptm_po_shipment_trx
                              WHERE asn_number = p_asn_no);

          IF SQL%FOUND
          THEN
            fnd_file.put_line (fnd_file.LOG
                              , 'no of records deleted from IMEC.ASSIGNED_SSIDS ' || SQL%ROWCOUNT
                              );
          END IF;

          DELETE FROM imec.serial_master
                WHERE serialnumber IN (SELECT serial_number
                                         FROM xxntgr_ptm_po_shipment_trx
                                        WHERE asn_number = p_asn_no);

          IF SQL%FOUND
          THEN
            fnd_file.put_line (fnd_file.LOG
                              , 'no of records deleted from IMEC.SERIAL_MASTER ' || SQL%ROWCOUNT
                              );
          END IF;

          /*added by ppatil 3.10 FOR deleting data from DAVINCI tables end */
          DELETE FROM xxntgr_ptm_trx_register
                WHERE instance_id IN (
                        SELECT trx.instance_id
                          FROM xxntgr_ptm_item_instances ins
                              ,xxntgr_ptm_po_shipment_trx trx
                              ,xxntgr_ptm_trx_register reg
                         WHERE ins.instance_id = trx.instance_id
                           AND trx.instance_id = reg.instance_id
                           AND trx.asn_number = p_asn_no
                           AND NVL (trx.input_file_id, 1) =
                                                   NVL (p_input_file_id, NVL (trx.input_file_id, 1)));

          DELETE FROM xxntgr_ptm_item_instances
                WHERE instance_id IN (
                        SELECT trx.instance_id
                          FROM xxntgr_ptm_po_shipment_trx trx, xxntgr_ptm_item_instances ins
                         WHERE trx.instance_id = ins.instance_id
                           AND trx.asn_number = p_asn_no
                           AND NVL (trx.input_file_id, 1) =
                                                   NVL (p_input_file_id, NVL (trx.input_file_id, 1)));

          DELETE FROM xxntgr_ptm_item_instances
                WHERE top_serial_number IN (
                        SELECT trx.serial_number
                          FROM xxntgr_ptm_po_shipment_trx trx
                         WHERE 1 = 1
                           AND trx.asn_number = p_asn_no
                           AND NVL (trx.input_file_id, 1) =
                                                   NVL (p_input_file_id, NVL (trx.input_file_id, 1)));

          -- Delete from repository table as well GP 11-DEC-13
          DELETE FROM xxntgr_product_id_repository
                WHERE batch_id IN (
                        SELECT trx.input_file_id
                          FROM xxntgr_ptm_po_shipment_trx trx
                         WHERE trx.asn_number = p_asn_no
                           AND NVL (trx.input_file_id, 1) =
                                                   NVL (p_input_file_id, NVL (trx.input_file_id, 1)));

          DELETE FROM xxntgr_ptm_po_shipment_trx
                WHERE asn_number = p_asn_no
                  AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1));

          DELETE FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_no
                  AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1));

          ------ Added for change 5.2
          UPDATE xxntgr_apps.xxntgr_ptm_odm_file_header
             SET recall_flag = 'Y'
           WHERE asn_number = p_asn_no
             AND process_status NOT IN ('E')
             AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1));

          ------ Added for change 5.2
          xxntgr_debug_utl.LOG (   'All the records for ASN# '
                                || p_asn_no
                                || ' and Batch# '
                                || p_input_file_id
                                || ' are removed from staging and PTM tables.'
                               );
        EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
            xxntgr_debug_utl.LOG (   'Error while getting input_file_id for the ASN#'
                                  || p_asn_no
                                  || ' '
                                  || SQLCODE
                                  || SQLERRM
                                 ,'1'
                                 );
          WHEN OTHERS
          THEN
            retcode := 1;
            xxntgr_debug_utl.LOG (   'Error while deleting the ASN#'
                                  || p_asn_no
                                  || ' '
                                  || SQLCODE
                                  || SQLERRM
                                 ,'1'
                                 );

            -- mmore start 3.4
            BEGIN
              log_error_email
                ('XXNTGR_PTM_ODM_SHIP'
                ,'PTM ODM Shipment File Import ORAERROR'
                ,    'Hi Team,\n\n\tThere is an error in program PTM ODM Shipment File Import.\n\tConcurrent Request ID: '
                  || v_reqstid
                ,    '\n\tASN#: '
                  || p_asn_no
                  || '\n\tError while deleting ASN data. SQLERRM: '
                  || SQLERRM
                  || '\n\nRegards\nNETGEAR Support'
                );
            EXCEPTION
              WHEN OTHERS
              THEN
                xxntgr_debug_utl.LOG (   'WO01 Error in executing log_error_email. SQLERRM => '
                                      || SQLERRM
                                     ,'1'
                                     );
            END;
        -- mmore end 3.4
        END;
      END IF;
    ELSE                                                                      --  P_DELETE_ASN = 'N'
      fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Delete ASN is N');

---Split the ASN
      BEGIN
        IF p_split_asn IS NOT NULL
        THEN
          fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Split ASN is Y');
          v_comma_count :=
                         NVL ((LENGTH (REGEXP_REPLACE (p_split_asn, '[^,]')) / LENGTH (',') + 1)
                             ,0);
          --parse comma separated string to identify ASNs
          v_asn_without_split := p_split_asn;
          v_asn_count := 0;

          FOR i IN 1 .. v_comma_count
          LOOP
            v_asn_count := v_asn_count + 1;
            v_split_asn :=
              NVL (SUBSTR (v_asn_without_split, 1, (INSTR (v_asn_without_split, ',') - 1))
                  ,v_asn_without_split
                  );

            IF v_asn_count = 1
            THEN
              v_asn_1 := v_split_asn;
            ELSE
              IF v_asn_count = 2
              THEN
                v_asn_2 := v_split_asn;
              ELSE
                IF v_asn_count = 3
                THEN
                  v_asn_3 := v_split_asn;
                END IF;
              END IF;
            END IF;

            v_asn_without_split :=
              NVL (SUBSTR (v_asn_without_split, (INSTR (v_asn_without_split, ',') + 1))
                  ,v_asn_without_split
                  );
          END LOOP;

          BEGIN
            SELECT SUM (l.quantity_shipped)
              INTO v_shipped_qty
              FROM rcv_shipment_headers h, rcv_shipment_lines l
             WHERE 1 = 1
               AND h.receipt_source_code = 'VENDOR'
               AND h.asn_type = 'ASN'
               AND l.shipment_header_id = h.shipment_header_id
               AND l.source_document_code = 'PO'
               AND h.vendor_id IN (
                             SELECT lookup_code
                               FROM fnd_lookup_values
                              WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                    AND enabled_flag = 'Y')
               AND h.shipment_num = v_asn_2;
          EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
              v_shipped_qty := 0;
            WHEN OTHERS
            THEN
              v_shipped_qty := 0;
              xxntgr_debug_utl.LOG ('Error while getting shipped_quantity');
          END;

          --Update staging table with split ASNs
          v_rec_count := 0;

          FOR i IN get_asn_to_split (v_asn_1)
          LOOP
            v_rec_count := v_rec_count + 1;

            IF v_rec_count <= v_shipped_qty
            THEN
              UPDATE xxntgr_ptm_odm_shipment_stg
                 SET asn_number = v_asn_2
                    ,invoice_number = v_asn_2
               WHERE asn_number = i.asn_number AND record_row_number = i.record_row_number;
            ELSE
              UPDATE xxntgr_ptm_odm_shipment_stg
                 SET asn_number = v_asn_3
                    ,invoice_number = v_asn_3
               WHERE asn_number = i.asn_number AND record_row_number = i.record_row_number;
            END IF;
          END LOOP;
        END IF;
      END;

      COMMIT;
      fnd_file.put_line (fnd_file.LOG
                        , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Starting ASN Level Validation'
                        );

      --Start field level validation
      BEGIN
        FOR dist_asn IN get_asn_inv
        LOOP
          v_num := 1;
          v_mac_flg := NULL;
          v_validate_fields := NULL;
          v_validate_fields1 := NULL;                                                  -- mmore 3.4
          v_asn_inv_no := dist_asn.asn_number;              -- mmore 3.4 This variable was not set.
          fnd_file.put_line (fnd_file.LOG
                            , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Start ASN Number:' || v_asn_inv_no
                            );
          fnd_file.put_line (fnd_file.LOG
                            , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Check duplicate files for ASN'
                            );

          ---Delete duplicate asn files, if any
          BEGIN
            FOR i IN identify_dup_asn (v_asn_inv_no)
            LOOP
              IF i.batch_count > 1
              THEN
                UPDATE xxntgr_ptm_odm_shipment_stg
                   SET process_status = 'E'
                      ,process_message = 'DUPLICATE_FILE'
                      ,last_update_date = SYSDATE
                 WHERE (process_status = 'U' OR process_status IS NULL)
                   AND asn_number = i.asn_number
                   AND UPPER (manufacturer_name) = UPPER (i.manufacturer_name)
                   AND input_file_id < i.max_batch;

                --Insert duplicate data into archive table
                INSERT INTO xxntgr_ptm_odm_shipment_arch
                  SELECT *
                    FROM xxntgr_ptm_odm_shipment_stg
                   WHERE asn_number = i.asn_number
                     AND UPPER (manufacturer_name) = UPPER (i.manufacturer_name)
                     AND input_file_id < i.max_batch;

                --Delete duplicate data from staging table
                DELETE FROM xxntgr_ptm_odm_shipment_stg
                      WHERE asn_number = i.asn_number
                        AND UPPER (manufacturer_name) = UPPER (i.manufacturer_name)
                        AND input_file_id < i.max_batch;
              END IF;
            END LOOP;
          EXCEPTION
            WHEN OTHERS
            THEN
              xxntgr_debug_utl.LOG ('Error while deleting duplicate ASN file!!');

              -- mmore start 3.4
              BEGIN
                log_error_email
                  ('XXNTGR_PTM_ODM_SHIP'
                  ,'PTM ODM Shipment File Import ORAERROR'
                  ,    'Hi Team,\n\n\tThere is an error in program PTM ODM Shipment File Import.\n\tConcurrent Request ID: '
                    || v_reqstid
                  ,    '\n\tError while deleting duplicate ASN file. SQLERRM: '
                    || SQLERRM
                    || '\n\nRegards\nNETGEAR Support'
                  );
              EXCEPTION
                WHEN OTHERS
                THEN
                  xxntgr_debug_utl.LOG (   'WO02 Error in executing log_error_email. SQLERRM => '
                                        || SQLERRM
                                       ,'1'
                                       );
              END;
          -- mmore end 3.4
          END;

          fnd_file.put_line (fnd_file.LOG
                            , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Check duplicate files for ASN Done'
                            );

          --START Change 5.2
            ----Validate Processed ASN
          BEGIN
            v_processed_asn := 'N';

            SELECT DISTINCT 'Y'
                       INTO v_processed_asn
                       FROM xxntgr_apps.xxntgr_ptm_odm_file_header hdr
                      WHERE 1 = 1
                        AND hdr.asn_number = v_asn_inv_no
                        AND hdr.input_file_id <> dist_asn.input_file_id
                        AND hdr.process_status IN ('P', 'S')
                        AND NVL (hdr.recall_flag, 'N') = 'N';

            IF v_processed_asn = 'Y'
            THEN
              v_errmsg := 'ASN Is already Processed ';
              fnd_file.put_line (fnd_file.LOG
                                , TO_CHAR (SYSDATE, 'HH:MI:SS') || 'ASN already processed'
                                );

              UPDATE xxntgr_ptm_odm_shipment_stg
                 SET process_status = 'E'
                    ,process_message = process_message || '++178##' || v_errmsg
               WHERE asn_number = v_asn_inv_no
                 AND input_file_id = dist_asn.input_file_id
                 AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

              xxntgr_debug_utl.LOG ('ASN:' || v_asn_inv_no || '-' || v_errmsg, '1');
              v_validate_fields1 := 'E';
            END IF;
          EXCEPTION
            WHEN OTHERS
            THEN
              NULL;
          END;

          ----Validate Processed ASN
          --END Change 5.2
          xxntgr_debug_utl.LOG (   'ASN Loop Starts : '
                                || dist_asn.asn_number
                                || '-'
                                || dist_asn.manufacturer_name
                               );
          fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || 'Validate PO Number');

          -------- Start change 5.7    Checking po in Base Table
          FOR r IN (SELECT DISTINCT po_number
                               FROM xxntgr_apps.xxntgr_ptm_odm_shipment_stg
                              WHERE asn_number = dist_asn.asn_number
                                AND input_file_id = dist_asn.input_file_id)
          LOOP
            v_validate_po := 0;
            v_error_message := NULL;

            SELECT COUNT (po_header_id)
              INTO v_validate_po
              FROM apps.po_headers_all
             WHERE segment1 = r.po_number;

            IF v_validate_po = 0
            THEN
              v_error_message :=
                          ' Invalid PO Number,PO DATA NOT AVALIABLE - PO_NUMBER :- ' || r.po_number;
              v_validate_fields1 := 'E';

              UPDATE xxntgr_apps.xxntgr_ptm_odm_shipment_stg
                 SET process_status = 'E'
                    ,process_message = process_message || v_error_message
               WHERE input_file_id = dist_asn.input_file_id AND po_number = r.po_number;

              EXIT;               ----------- Any one of PO is Invalid then We are Exiting from Loop
            END IF;
          END LOOP;

          -------- End change 5.7    Checking po in Base Table
          fnd_file.put_line (fnd_file.LOG
                            , TO_CHAR (SYSDATE, 'HH:MI:SS') || 'Validate Pallet and Carton ID'
                            );

          -- Start Amit Bhatka changes 5.4
          FOR c_pallet IN (SELECT DISTINCT pallet_id
                                      FROM xxntgr_apps.xxntgr_ptm_odm_shipment_stg stg
                                     WHERE 1 = 1
                                       AND po_number IS NOT NULL
                                       AND input_file_id = dist_asn.input_file_id)
          LOOP
            v_error_message := NULL;
            --fnd_file.put_line( fnd_file.log,'Validating Pallet ID:'||c_pallet.pallet_id);
            xxntgr_ptm_common_pkg.validate_pallet_id (c_pallet.pallet_id, v_error_message);

            IF v_error_message IS NOT NULL
            THEN
              fnd_file.put_line (fnd_file.LOG, 'Validation Fail Updating STG Table');
              v_validate_fields1 := 'E';

              UPDATE xxntgr_apps.xxntgr_ptm_odm_shipment_stg
                 SET process_status = 'E'
                    ,process_message = process_message || v_error_message
               WHERE pallet_id = c_pallet.pallet_id AND input_file_id = dist_asn.input_file_id;
            END IF;

            FOR c_carton IN (SELECT DISTINCT master_carton_id
                                        FROM xxntgr_apps.xxntgr_ptm_odm_shipment_stg stg
                                       WHERE 1 = 1
                                         AND pallet_id = c_pallet.pallet_id
                                         AND input_file_id = dist_asn.input_file_id)
            LOOP
              v_error_message := NULL;
              --fnd_file.put_line( fnd_file.log,'Validating Carton ID:'||c_carton.master_carton_id);
              xxntgr_ptm_common_pkg.validate_carton_id (dist_asn.input_file_id
                                                       ,c_carton.master_carton_id
                                                       ,v_error_message
                                                       );

              IF v_error_message IS NOT NULL
              THEN
                v_validate_fields1 := 'E';
                fnd_file.put_line (fnd_file.LOG, 'Validation Fail Updating STG Table');

                UPDATE xxntgr_apps.xxntgr_ptm_odm_shipment_stg
                   SET process_status = 'E'
                      ,process_message = process_message || v_error_message
                 WHERE pallet_id = c_pallet.pallet_id
                   AND master_carton_id = c_carton.master_carton_id
                   AND input_file_id = dist_asn.input_file_id;
              END IF;
            END LOOP;
          END LOOP;

-- End Amit Bhatka changes 5.4

          -- call item level validations
          FOR dist_items IN get_dist_items_inv (dist_asn.input_file_id)
          LOOP
            v_aircard_item := NULL;                                                    -- mmore 3.4
            -- 4.8 initializing variables for each item
            l_product_line := NULL;
            l_product_category := NULL;
            l_product_family := NULL;
            l_sn_type := NULL;
            l_bundle_sku := 0;
            l_bundle_flag := 'N';
-- 4.8 end
            xxntgr_debug_utl.LOG (' Call to validate record from import program -', '3');
            --- Subbareddy 4.0
            fnd_file.put_line (fnd_file.LOG
                              ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                || ' Serialize Item:'
                                || dist_items.item_number
                              );
            xxntgr_ptm_common_pkg.xxntgr_serialize_item_proc (dist_items.item_number);

            --End 4.0
            IF dist_items.item_number IS NULL
            THEN
              UPDATE xxntgr_ptm_odm_shipment_stg
                 SET process_status = 'E'
                    ,process_message = 'SKU must be provided in the file'
               WHERE NVL (asn_number, '1') = NVL (dist_asn.asn_number, '1')
                 AND NVL (item_number, '1') = NVL (dist_items.item_number, '1')
                 AND input_file_id = dist_items.input_file_id
                 AND NVL (UPPER (manufacturer_name), '1') =
                                                       NVL (UPPER (dist_asn.manufacturer_name), '1')
                 AND NVL (process_status, 'NULL') NOT IN ('P', 'S');
            ELSE
              -- 6.0 determine if the SKU is a bundle SKU
              SELECT COUNT (1)
                INTO l_bundle_sku
                FROM xxntgr_apps.xxntgr_ptm_bundle_bom bom
               WHERE bom.item_number = dist_items.item_number AND NVL (bom.fg_enabled, 'N') = 'Y';

              IF l_bundle_sku > 0
              THEN
                fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Bundle SKU');
                l_bundle_flag := 'Y';
                l_bundle_item_tbl (dist_asn.asn_number || '#' || dist_items.item_number) := 'Y';

				----- Added for Change 6.7 Validate Product Id Configuried Into PTM
                l_check_product_cfg := 'N';

                FOR c_rec IN (SELECT fa_product_id
                                FROM xxntgr_apps.xxntgr_ptm_bundle_bom bom
                               WHERE bom.item_number = dist_items.item_number
                                 AND NVL (bom.active_flag, 'N') = 'Y')
                LOOP
                  BEGIN
                    SELECT 'Y'
                      INTO l_check_product_cfg
                      FROM xxntgr_apps.xxntgr_ptm_item_cfg
                     WHERE product_id = c_rec.fa_product_id;
                  EXCEPTION
                    WHEN OTHERS
                    THEN
                      l_check_product_cfg := 'N';

                      UPDATE xxntgr_ptm_odm_shipment_stg
                         SET process_status = 'E'
                            ,process_message =
                                       c_rec.fa_product_id || ': Product Id Is Not Configured into PTM'
                       WHERE NVL (asn_number, '1') = NVL (dist_asn.asn_number, '1')
                         AND NVL (item_number, '1') = NVL (dist_items.item_number, '1')
                         AND input_file_id = dist_items.input_file_id
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

						BEGIN
							log_error_email
							  ('XXNTGR_PTM_ODM_SHIP'
							  ,'PTM ODM Shipment File Import ORAERROR'
							  ,    'Hi Team,\n\n\tThere is an error in program PTM ODM Shipment File Import.\n\tConcurrent Request ID: '
								|| v_reqstid
							  ,    '\n\tASN# '
								|| v_asn_inv_no
								||' - '
								|| c_rec.fa_product_id ||' - Product Id Is Not Configured into PTM: '
								|| SQLERRM
								|| '\n\nRegards\nNETGEAR Support'
							  );
						  EXCEPTION
							WHEN OTHERS
							THEN
							  xxntgr_debug_utl.LOG ('WO03 Error in executing log_error_email. SQLERRM => '
													|| SQLERRM
												   ,'1'
												   );
						END;


                      fnd_file.put_line (fnd_file.LOG
                                        ,    'Product Id Is Not Configured into PTM  -'
                                          || c_rec.fa_product_id
                                        );
                      EXIT;
                  END;
                END LOOP;

                IF l_check_product_cfg = 'Y'
                THEN
                  FOR fa_rec IN
                    (SELECT DISTINCT bom.fa_product_id
                                    ,NVL (itmcfg.serial_number_type, 'A') serial_number_type
                                FROM xxntgr_apps.xxntgr_ptm_bundle_bom bom
                                    ,xxntgr_apps.xxntgr_ptm_item_cfg itmcfg
                               WHERE bom.item_number = dist_items.item_number
                                 AND itmcfg.product_id = bom.fa_product_id
                                 AND NVL (bom.active_flag, 'N') = 'Y')
                  LOOP
                    --- Storing SN type for the product IDs in table type variable
                    gvt_sn_item (fa_rec.fa_product_id) := fa_rec.serial_number_type;
                  END LOOP;
                END IF;
              -- Not a bundle SKU
              ELSE
                fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Non Bundle SKU');
                l_bundle_item_tbl (dist_asn.asn_number || '#' || dist_items.item_number) := 'N';

                -- mmore start 3.4

				BEGIN
                  -- 4.8 modified the query to fetch the product line, family and category values
                  fnd_file.put_line (fnd_file.LOG
                                    , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Deriving Product Hierarchy'
                                    );

                  SELECT NVL (msi.attribute19, 'N'), mc.segment1, mc.segment2, mc.segment3
                    INTO v_aircard_item, l_product_line, l_product_category, l_product_family
                    FROM mtl_system_items_b msi
                        ,mtl_item_categories mic
                        ,mtl_categories mc
                        ,mtl_category_sets mcs
                   WHERE msi.organization_id = mic.organization_id
                     AND msi.inventory_item_id = mic.inventory_item_id
                     AND mic.category_id = mc.category_id
                     AND mic.category_set_id = mcs.category_set_id
                     AND msi.organization_id = 488
                     AND msi.segment1 = dist_items.item_number
                     AND mcs.description = 'NG Line | Category | Family';

                  IF v_aircard_item = 'AIRCARD'
                  THEN
                    fnd_file.put_line (fnd_file.LOG
                                      , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Aircard Item'
                                      );

                    UPDATE xxntgr_ptm_odm_shipment_stg
                       SET process_status = 'E'
                          ,process_message =
                             'AIRCARD SKU cannot be processed throught ODM shipment. Please process it in AIRCARD file format'
                     WHERE NVL (asn_number, '1') = NVL (dist_asn.asn_number, '1')
                       AND NVL (item_number, '1') = NVL (dist_items.item_number, '1')
                       AND input_file_id = dist_items.input_file_id
                       AND NVL (UPPER (manufacturer_name), '1') =
                                                       NVL (UPPER (dist_asn.manufacturer_name), '1')
                       AND NVL (process_status, 'NULL') NOT IN ('P', 'S');
                  END IF;
                EXCEPTION
                  WHEN OTHERS
                  THEN
                    NULL;
                END;

                -- mmore end 3.4

                -- 4.8 Derive the Serial Number format set for the item
                BEGIN
                  SELECT NVL (COALESCE ((SELECT serial_number_type
                                           FROM xxntgr_apps.xxntgr_ptm_item_cfg
                                          WHERE 1 = 1 AND item_number = dist_items.item_number)
                                       , (SELECT serial_number_type
                                            FROM xxntgr_apps.xxntgr_ptm_item_cfg
                                           WHERE 1 = 1 AND product_family = l_product_family)
                                       , (SELECT serial_number_type
                                            FROM xxntgr_apps.xxntgr_ptm_item_cfg
                                           WHERE 1 = 1 AND product_category = l_product_category)
                                       , (SELECT serial_number_type
                                            FROM xxntgr_apps.xxntgr_ptm_item_cfg
                                           WHERE 1 = 1 AND product_line = l_product_line)
                                       )
                             ,'A'
                             )
                    INTO l_sn_type
                    FROM DUAL;
                EXCEPTION
                  WHEN OTHERS
                  THEN
                    fnd_file.put_line (fnd_file.LOG, 'Error While getting the Serial Number Type');
                    l_sn_type := 'X';
                -- RAISE exp_main_raise_error;
                END;

                --- Storing SN type for the items in table type variable
                gvt_sn_item (dist_items.item_number) := l_sn_type;
              -- 4.8 End
              END IF;
            -- 6.0 end
            END IF;

            IF NVL (v_aircard_item, 'N') = 'AIRCARD'
            THEN                                                     -- mmore 3.4 added if condition
              v_validate_fields1 := 'E';                                               -- mmore 3.4
            ELSE                                                                        -- mmore 3.4
              fnd_file.put_line (fnd_file.LOG
                                , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Call Validate Record'
                                );
              xxntgr_ptm_common_pkg.validate_record (dist_items.input_file_id
                                                    ,dist_items.item_number
                                                    ,NULL
                                                    ,'NONAC'
                                                    ,v_error_message
                                                    );
              fnd_file.put_line (fnd_file.LOG
                                , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Validate Record Done'
                                );
              fnd_file.put_line (fnd_file.LOG
                                , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Serial Number Validation'
                                );

              IF NVL (l_bundle_flag, 'N') <> 'Y'
              THEN
                -- 4.8 loop cursor to validate the serial numbers for this item
                l_sn_type := gvt_sn_item (dist_items.item_number);
                fnd_file.put_line (fnd_file.LOG, 'SN Type:' || l_sn_type);

                IF l_sn_type NOT IN ('X')
                THEN
                  FOR item_sn_rec IN cur_get_item_sn (dist_asn.asn_number
                                                     ,dist_items.input_file_id
                                                     ,dist_items.item_number
                                                     )
                  LOOP
                    l_sn_err_meg := NULL;
                    -- getting Serial Number type from table type record
                    xxntgr_ptm_common_pkg.validate_serial_number
                                                                (item_sn_rec.serial_number_product
                                                                ,l_sn_type
                                                                ,'ODM'
                                                                ,l_sn_err_meg
                                                                );

                    ---    fnd_file.put_line( fnd_file.log,'Serial Number Validated:'||l_sn_err_meg);
                    IF l_sn_err_meg IS NOT NULL
                    THEN
                      UPDATE xxntgr_ptm_odm_shipment_stg
                         SET process_message = process_message || '++178##' || l_sn_err_meg
                       WHERE asn_number = dist_asn.asn_number
                         AND input_file_id = dist_items.input_file_id
                         AND serial_number_product = item_sn_rec.serial_number_product;

                      v_error_message := v_error_message || '++178##' || l_sn_err_meg;
                    END IF;
                  END LOOP;
                END IF;
              ELSE
                FOR fa_rec IN (SELECT DISTINCT bom.fa_product_id
                                          FROM xxntgr_apps.xxntgr_ptm_bundle_bom bom
                                         WHERE bom.item_number = dist_items.item_number
                                           AND NVL (bom.active_flag, 'N') = 'Y')
                LOOP
                  l_sn_type := gvt_sn_item (fa_rec.fa_product_id);
                  fnd_file.put_line (fnd_file.LOG, 'Product ID:' || fa_rec.fa_product_id);
                  fnd_file.put_line (fnd_file.LOG, 'SN Type:' || l_sn_type);

                  IF l_sn_type NOT IN ('X')
                  THEN
                    FOR item_sn_rec IN cur_get_prod_id_sn (dist_asn.asn_number
                                                          ,dist_items.input_file_id
                                                          ,fa_rec.fa_product_id
                                                          )
                    LOOP
                      l_sn_err_meg := NULL;
                      -- getting Serial Number type from table type record
                      xxntgr_ptm_common_pkg.validate_serial_number
                                                                (item_sn_rec.serial_number_product
                                                                ,l_sn_type
                                                                ,'ODM'
                                                                ,l_sn_err_meg
                                                                );

                      IF l_sn_err_meg IS NOT NULL
                      THEN
                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_message = process_message || '++178##' || l_sn_err_meg
                         WHERE asn_number = dist_asn.asn_number
                           AND input_file_id = dist_items.input_file_id
                           AND serial_number_product = item_sn_rec.serial_number_product;

                        v_error_message := v_error_message || '++178##' || l_sn_err_meg;
                      END IF;
                    END LOOP;
                  END IF;
                END LOOP;
              END IF;

              fnd_file.put_line (fnd_file.LOG
                                , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Serial Number Validation Done'
                                );
              -- 4.8 end
              v_validate_fields := v_error_message;
              xxntgr_debug_utl.LOG ('Validate Fields - ' || v_validate_fields, '3');
              fnd_file.put_line (fnd_file.LOG
                                ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                  || ' v_validate_fields:'
                                  || v_validate_fields
                                );

              IF v_validate_fields IS NOT NULL
              THEN
                v_validate_fields1 := 'E';                                             -- mmore 3.4

                UPDATE xxntgr_ptm_odm_shipment_stg
                   SET process_status = 'E'
                 WHERE NVL (item_number, '1') = NVL (dist_items.item_number, '1')
                   AND input_file_id = dist_items.input_file_id
                   AND NVL (process_status, 'NULL') NOT IN ('P', 'S');
              END IF;
            END IF;                                                                     -- mmore 3.4
          END LOOP;

          IF v_validate_fields1 IS NULL
          THEN
            -- Start Changes for bundle enhancement 6.0
            v_error_message := NULL;
            fnd_file.put_line (fnd_file.LOG
                              , TO_CHAR (SYSDATE, 'HH:MI:SS') || 'Call validate_bundle');
            xxntgr_ptm_common_pkg.validate_bundle (dist_asn.input_file_id, 'NAC', v_error_message);
            fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || 'End validate_bundle');

            IF v_error_message IS NOT NULL
            THEN
              v_validate_fields1 := 'E';
              fnd_file.put_line (fnd_file.LOG
                                , TO_CHAR (SYSDATE, 'HH:MI:SS') || 'Validate Bundle Failure'
                                );

              UPDATE xxntgr_apps.xxntgr_ptm_odm_shipment_stg
                 SET process_status = 'E'
               WHERE input_file_id = dist_asn.input_file_id AND asn_number = dist_asn.asn_number;
            END IF;
          -- End changes for bundle enh 6.0
          END IF;

          --IF v_validate_fields IS NOT NULL THEN -- mmore 3.4
          IF v_validate_fields1 IS NOT NULL
          THEN                               -- mmore 3.4 checking v_validate_fields1 for any errors
            v_error_message := NULL;

            FOR dist_errors IN get_dist_errors (dist_asn.input_file_id)
            LOOP
              --mmore start 3.5
              IF LENGTH (v_error_message) > 31000
              THEN
                fnd_file.put_line (fnd_file.LOG
                                  , 'Exit loop get_dist_errors ' || LENGTH (v_error_message)
                                  );
                EXIT;
              END IF;

              --mmore end 3.5
              v_error_message := v_error_message || '<BR>' || dist_errors.process_message;
            END LOOP;

            fnd_file.put_line (fnd_file.LOG, 'Outside loop get_dist_errors...' || v_error_message);
            --mmore 3.5
            v_error_message := REPLACE (v_error_message, '++010##', '|');
            v_error_message := REPLACE (v_error_message, '++020##', '|');
            v_error_message := REPLACE (v_error_message, '++132##', '|');
            v_error_message := REPLACE (v_error_message, '++138##', '|');
            v_error_message := REPLACE (v_error_message, '++178##', '|');
            v_error_message := REPLACE (v_error_message, '++995##', '|');
            v_error_message := REPLACE (v_error_message, '++996##', '|');
            xxntgr_debug_utl.LOG ('Error v_error_message : ' || v_error_message);

            --Call the mail program to send notification for validation failure
            BEGIN
              --XXNTGR_PTM_ODM_PKG.PTM_ODM_SHIP_MAIL(v_asn_inv_no,upper(dist_asn.manufacturer_name),dist_asn.file_name,SUBSTR(v_validate_fields,1,1500),v_get_mail_error);
              xxntgr_ptm_odm_pkg.ptm_odm_ship_mail (v_asn_inv_no
                                                   ,UPPER (dist_asn.manufacturer_name)
                                                   ,dist_asn.file_name
                                                   ,SUBSTR (v_error_message, 1, 1500)
                                                   ,v_get_mail_error
                                                   );
              xxntgr_debug_utl.LOG (v_get_mail_error, '1');
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                NULL;
              WHEN OTHERS
              THEN
                NULL;
            END;

            xxntgr_debug_utl.LOG (   'Error while doing validation for mandatory fields for ASN : '
                                  || v_asn_inv_no
                                  || '-'
                                  || v_validate_fields
                                 ,'1'
                                 );

            --Insert error data into archive table
            INSERT INTO xxntgr_ptm_odm_shipment_arch
              SELECT *
                FROM xxntgr_ptm_odm_shipment_stg
               WHERE NVL (asn_number, '1') = NVL (dist_asn.asn_number, '1')
                 --ASN_NUMBER = v_asn_inv_no
                 AND input_file_id = dist_asn.input_file_id;

            --Delete error data from staging table
            DELETE FROM xxntgr_ptm_odm_shipment_stg
                  WHERE NVL (asn_number, '1') = NVL (dist_asn.asn_number, '1')
                    --nvl(asn_number,invoice_number) = v_asn_inv_no
                    AND input_file_id = dist_asn.input_file_id;

            COMMIT;
            retcode := 1;
            -- call to insert header record -- GP added 07-JUL-2014
            xxntgr_ptm_common_pkg.xxntgr_insert_odm_file_header (dist_asn.input_file_id);
          ELSE
            -- 6.2 Set process status to V to indicate that the file has passed all validations
            UPDATE xxntgr_apps.xxntgr_ptm_odm_shipment_stg
               SET process_status = 'V'
             WHERE NVL (asn_number, '1') = NVL (dist_asn.asn_number, '1')
               AND input_file_id = dist_asn.input_file_id;
          END IF;
        END LOOP;                                                         -- END of dist ASN numbers
      EXCEPTION
        WHEN OTHERS
        THEN
          xxntgr_debug_utl.LOG
                           (   'Error while checking for mandatory columns in the file for ASN : '
                            || v_asn_inv_no
                            || '-'
                            || SQLCODE
                            || SQLERRM
                           ,'1'
                           );

          -- mmore start 3.4
          BEGIN
            log_error_email
              ('XXNTGR_PTM_ODM_SHIP'
              ,'PTM ODM Shipment File Import ORAERROR'
              ,    'Hi Team,\n\n\tThere is an error in program PTM ODM Shipment File Import.\n\tConcurrent Request ID: '
                || v_reqstid
              ,    '\n\tASN# '
                || v_asn_inv_no
                || ' Error while checking for mandatory columns in the file. SQLERRM: '
                || SQLERRM
                || '\n\nRegards\nNETGEAR Support'
              );
          EXCEPTION
            WHEN OTHERS
            THEN
              xxntgr_debug_utl.LOG ('WO03 Error in executing log_error_email. SQLERRM => '
                                    || SQLERRM
                                   ,'1'
                                   );
          END;
      -- mmore end 3.4
      END;

      -- Start processig if all the mandatory columns are provided
      BEGIN
        fnd_file.put_line (fnd_file.LOG
                          , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' All mandatory columns provided'
                          );

        OPEN get_asn_number;

        LOOP
          FETCH get_asn_number
           INTO v_asn_num, v_manufacturer, v_file_name, v_add_input_file_id, v_odm_name;

          --added v_odm_name  4.3 --- Remove v_po_number for 5.6
          EXIT WHEN get_asn_number%NOTFOUND;
          v_invalid_mfg := NULL;
          v_reason := NULL;
          v_proc_status := NULL;
          v_errmsg := NULL;                                                            -- mmore 3.4
          v_err_location := NULL;                                                      -- mmore 3.4
          fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' ASN-' || v_asn_num);
          fnd_file.put_line (fnd_file.LOG
                            , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Validate Manufacturer');

          --Validate Manufacturer
          BEGIN
            SELECT meaning, lookup_code
              INTO v_mfg, v_mfg_code
              FROM fnd_lookup_values
             WHERE meaning = UPPER (v_manufacturer)
               AND lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
               AND enabled_flag = 'Y';
          EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
              v_invalid_mfg := 'Manufacturer Name ' || v_manufacturer || ' is invalid.';
            WHEN OTHERS
            THEN
              xxntgr_debug_utl.LOG (   'ASN:'
                                    || v_asn_num
                                    || '-'
                                    || 'Error while validating for valid ODM name: '
                                    || SQLCODE
                                    || SQLERRM
                                   ,'1'
                                   );
          END;

          --Validate Manufacturer on the ASN
          BEGIN
            --4.3 Start
            IF v_odm_name <> UPPER (v_manufacturer)
            THEN
              v_invalid_mfg := 'Manufacturer Name ' || v_manufacturer || ' is invalid.';
            END IF;

            --4.3 End
            --mmore 3.12 code added start
            SELECT COUNT (1)
              INTO v_shipment_count
              FROM apps.rcv_shipment_headers rsh, apps.fnd_lookup_values flv
             WHERE rsh.vendor_id = flv.lookup_code
               AND flv.lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
               AND flv.enabled_flag = 'Y'
               AND TRIM (rsh.shipment_num) = v_asn_num
               AND rsh.vendor_id = v_mfg_code;

            IF v_shipment_count > 1
            THEN
              v_invalid_mfg :=
                   'There are more than one ASN# '
                || v_asn_num
                || ' exists in system for Manufacturer '
                || v_manufacturer;
            END IF;
          --mmore 3.12 code added end
          EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
              NULL;
            WHEN OTHERS
            THEN
              xxntgr_debug_utl.LOG (   'ASN:'
                                    || v_asn_num
                                    || '-'
                                    || 'Error while validating for valid ODM name: '
                                    || SQLCODE
                                    || SQLERRM
                                   ,'1'
                                   );
          END;

          IF v_invalid_mfg IS NOT NULL
          THEN
            UPDATE xxntgr_ptm_odm_shipment_stg
               SET process_status = 'E'
                  ,
                   --process_message = SUBSTR(process_message||' '||'INVALID ASN',1,4000) -- Commented for 4.3
                   process_message = SUBSTR (process_message || ' ' || v_invalid_mfg, 1, 4000)
             WHERE asn_number = v_asn_num
               AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
--               AND (process_status = 'U' OR process_status IS NULL);
               AND process_status = 'V';

            --Insert error data into archive table
            INSERT INTO xxntgr_ptm_odm_shipment_arch
              SELECT *
                FROM xxntgr_ptm_odm_shipment_stg
               WHERE asn_number = v_asn_num
                 AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                 AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

            --Delete error data from staging table
            DELETE FROM xxntgr_ptm_odm_shipment_stg
                  WHERE NVL (process_status, 'NULL') NOT IN ('P', 'S')
                    AND asn_number = v_asn_num
                    AND UPPER (manufacturer_name) = UPPER (v_manufacturer);

            BEGIN
              --   XXNTGR_PTM_ODM_PKG.PTM_ODM_SHIP_MAIL(v_asn_num,upper(v_manufacturer),v_file_name,SUBSTR(v_invalid_mfg,1,1500),v_get_mail_error);---Commented for 4.3
              xxntgr_ptm_odm_pkg.ptm_odm_ship_mail (v_asn_num
                                                   ,v_odm_name
                                                   ,v_file_name
                                                   ,SUBSTR (v_invalid_mfg, 1, 1500)
                                                   ,v_get_mail_error
                                                   );                               -- Added for 4.3
              xxntgr_debug_utl.LOG ('ASN:' || v_asn_num || '-' || v_invalid_mfg || v_get_mail_error
                                   ,'1'
                                   );
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                NULL;
              WHEN OTHERS
              THEN
                NULL;
            END;

            retcode := 1;
          END IF;

          IF v_invalid_mfg IS NULL
          THEN
            -------- Start change 5.5
            ------ Checking For PO is Non Trade If Non Trade then not validating ASN

            -------- Start change 5.6    Checking po type in Loop
            FOR i IN (SELECT DISTINCT po_number
                                 FROM xxntgr_apps.xxntgr_ptm_odm_shipment_stg
                                WHERE asn_number = v_asn_num
                                      AND input_file_id = v_add_input_file_id)
            LOOP
              v_nontrade_po := 'N';
              v_nontrade_po := xxntgr_ptm_common_pkg.check_nontrade_po_type (i.po_number);
              fnd_file.put_line (fnd_file.LOG
                                ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                  || ' PO Number-'
                                  || i.po_number
                                  || ';Non Trade PO-'
                                  || v_nontrade_po
                                );

              IF v_nontrade_po = 'Y'
              THEN              ----------- Any one of PO is Non Trade then We are Exiting from Loop
                EXIT;
              END IF;
            END LOOP;

            -------- End change 5.6    Checking po type in Loop
            IF NVL (v_nontrade_po, 'N') = 'N'
            THEN
              --Validated ASN_NUMBER
              BEGIN
                fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Validate ASN');

                SELECT TRIM (shipment_num)
                  INTO v_asn
                  FROM rcv_shipment_headers
                 WHERE TRIM (shipment_num) = v_asn_num
                   AND vendor_id =
                         (SELECT lookup_code
                            FROM fnd_lookup_values
                           WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                             AND meaning = UPPER (v_manufacturer)
                             AND enabled_flag = 'Y');
              EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                  v_invalid_asn := 'ASN Number ' || v_asn_num || ' is Invalid.';
                  xxntgr_debug_utl.LOG (v_invalid_asn, '2');
                  v_asn := NULL;
                WHEN OTHERS
                THEN
                  v_asn := NULL;
              END;
            ELSE
              v_asn := v_asn_num;
            END IF;
          -------- End change 5.5
          ELSE
            v_asn := NULL;
          END IF;

          IF v_asn IS NOT NULL
          THEN
            v_asn_number := v_asn_num;

            BEGIN
              IF v_asn_number IS NOT NULL
              THEN
                xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || 'VALIDATION STARTS - ', '3');
                 /* VALIDATION starts here */
                -- set process_mode, and validate serial_number for duplicacy
                fnd_file.put_line (fnd_file.LOG
                                  , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Set Process Mode'
                                  );

                FOR i IN set_process_mode (v_asn_number, v_manufacturer)
                LOOP
                  BEGIN
                    SELECT trx.asn_number
                      INTO v_dup_asn_sn
                      FROM xxntgr_ptm_po_shipment_trx trx
                     WHERE trx.asn_number = i.asn_number
                       AND trx.serial_number = i.serial_number_product
                       AND trx.manufacturer_name = UPPER (i.manufacturer_name);

                    v_process_mode := 'UPDATE';

                    /* start ppatil 3.10 */
                    UPDATE xxntgr_ptm_odm_shipment_stg
                       SET processed_flag = 'U'
                     WHERE asn_number = i.asn_number
                       AND UPPER (manufacturer_name) = UPPER (i.manufacturer_name)
                       AND serial_number_product = i.serial_number_product;

                    /* end ppatil 3.10 */
                    --mmore start 2.9
                    BEGIN
                      UPDATE xxntgr_ptm_odm_ship_notif
                         SET end_date_active = NULL
                            ,last_updated_by = fnd_global.user_id
                            ,last_update_date = SYSDATE
                       WHERE end_date_active IS NOT NULL
                         AND asn_number = i.asn_number
                         AND vendor_id =
                               (SELECT lookup_code
                                  FROM fnd_lookup_values
                                 WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                   AND meaning = UPPER (i.manufacturer_name)
                                   AND enabled_flag = 'Y');
                    EXCEPTION
                      WHEN OTHERS
                      THEN
                        xxntgr_debug_utl.LOG
                          (   'ERROR ASN: '
                           || v_asn_number
                           || ' MANUFACTURER: '
                           || i.manufacturer_name
                           || ' UPDATE MODE error in updating end_date_active to null in table XXNTGR_PTM_ODM_SHIP_NOTIF'
                          ,'3'
                          );
                    END;
                  --mmore end 2.9
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      v_process_mode := 'INSERT';

                      /* start ppatil 3.10 */
                      UPDATE xxntgr_ptm_odm_shipment_stg
                         SET processed_flag = 'I'
                       WHERE asn_number = i.asn_number
                         AND UPPER (manufacturer_name) = UPPER (i.manufacturer_name)
                         AND serial_number_product = i.serial_number_product;
                    /* end ppatil 3.10 */
                    WHEN OTHERS
                    THEN
                      v_err_location := v_err_location || 'A';                         -- mmore 3.4
                      v_errmsg := 'Error while setting process mode: ' || SQLCODE || SQLERRM;
                      fnd_file.put_line (fnd_file.LOG
                                        ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                          || 'Update xxntgr_ptm_odm_shipment_stg 6'
                                        );

                      UPDATE xxntgr_ptm_odm_shipment_stg
                         SET process_status = 'E'
                            ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                       WHERE asn_number = v_asn_number
                         AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                         AND serial_number_product = i.serial_number_product;

                      fnd_file.put_line (fnd_file.LOG
                                        ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                          || 'Update xxntgr_ptm_odm_shipment_stg 6'
                                        );
                      xxntgr_debug_utl.LOG ('After Seting v_process_mode 2');
                      xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');
                  END;
                END LOOP;            -- --set process_mode, and validate serial_number for duplicacy

                fnd_file.put_line (fnd_file.LOG
                                  , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Set Process Mode Done'
                                  );
                xxntgr_debug_utl.LOG (   'ASN:'
                                      || v_asn_number
                                      || '-'
                                      || 'Process Mode = '
                                      || v_process_mode
                                     ,'3'
                                     );
-- --added by ppatil 3.10 start
                      --v_reason := null;
                fnd_file.put_line (fnd_file.LOG
                                  , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Check Duplicate SN'
                                  );

                FOR j IN check_duplicate_sn (v_asn_number, v_manufacturer)
                LOOP
                  --IF (P_IGNR_DUP_SN = 'N' OR P_ASN_NO IS NULL) THEN
                  IF (p_ignr_dup_sn = 'N')
                  THEN
                    IF j.processed_flag = 'I'
                    THEN                                                     --added by ppatil 3.10
                      BEGIN
                        SELECT serial_number
                          INTO v_chk_duplicate_sn
                          FROM xxntgr_ptm_item_instances
                         WHERE serial_number = j.serial_number_product AND end_date_active IS NULL;

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                         SUBSTR (process_message || ' ' || 'DUPLICATE SN2', 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                           AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                           AND serial_number_product = j.serial_number_product;

                        v_reason :=
                          SUBSTR (   v_reason
                                  || ' Serial Number '
                                  || j.serial_number_product
                                  || ' has previously been used.'
                                 ,1
                                 ,4000
                                 );
                      EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                          NULL;
                        WHEN OTHERS
                        THEN
                          v_err_location := v_err_location || 'B';                     -- mmore 3.4
                          v_errmsg :=
                            'Error while finding duplicate SN in PTM table: ' || SQLCODE || SQLERRM;

                          UPDATE xxntgr_ptm_odm_shipment_stg
                             SET process_status = 'E'
                                ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                           WHERE asn_number = v_asn_number
                             AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                             AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                             AND serial_number_product = j.serial_number_product;

                          xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');
                      END;
                    END IF;
                  END IF;

                  IF p_ignr_dup_sn = 'Y'
                  THEN
                    IF p_deactivate_orig_sn = 'Y'
                    THEN
                      UPDATE xxntgr_ptm_item_instances
                         SET end_date_active = j.xfactory_date - 1
                            ,last_update_date = SYSDATE                              -- GP 22-AUG-12
                       WHERE serial_number = j.serial_number_product AND end_date_active IS NULL;
                    END IF;
                  END IF;
                END LOOP;

                fnd_file.put_line (fnd_file.LOG
                                  , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Check Duplicate SN Done'
                                  );
                --added by ppatil 3.10 end chk_duplicate_sn
                fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Validate Items');

                -- Validation for Item Number
                FOR i IN validate_items (v_asn_number, v_manufacturer)
                LOOP
                  fnd_file.put_line (fnd_file.LOG
                                    ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                      || ' Item Number-'
                                      || i.item_number
                                    );

                  BEGIN
                    SELECT segment1
                      INTO v_check_item
                      FROM mtl_system_items_b
                     WHERE segment1 = i.item_number AND organization_id = 488;          --master org
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      UPDATE xxntgr_ptm_odm_shipment_stg
                         SET process_status = 'E'
                            ,process_message =
                                          SUBSTR (process_message || ' ' || ' INVALID SKU', 1, 4000)
                       WHERE asn_number = v_asn_number
                         AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                         AND item_number = i.item_number;

                      v_reason :=
                        SUBSTR (v_reason || '<br>' || 'SKU ' || i.item_number || ' is invalid.'
                               ,1
                               ,4000
                               );
                    WHEN OTHERS
                    THEN
                      v_err_location := v_err_location || 'C';                         -- mmore 3.4
                      v_errmsg := 'Error in Item number validation. ' || SQLCODE || '-' || SQLERRM;
                      fnd_file.put_line (fnd_file.LOG
                                        ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                          || 'Update xxntgr_ptm_odm_shipment_stg 8'
                                        );

                      UPDATE xxntgr_ptm_odm_shipment_stg
                         SET process_status = 'E'
                            ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                       WHERE asn_number = v_asn_number
                         AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                         AND item_number = i.item_number;

                      xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');
                  END;
                END LOOP;

                --validation for any item number missing from file in comparision with the ASN, only if P_MATCH_TO_SN parameter is set to 'Y'
                IF p_match_to_sn = 'Y'
                THEN
                  BEGIN
                    fnd_file.put_line (fnd_file.LOG
                                      ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                        || ' Check for missing item in ASN'
                                      );

                    OPEN get_missed_item (v_asn_number, v_manufacturer);

                    LOOP
                      FETCH get_missed_item
                       INTO v_missed_item;

                      EXIT WHEN get_missed_item%NOTFOUND;

                      IF v_missed_item IS NOT NULL
                      THEN
                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                          SUBSTR (process_message || ' ' || 'ITEM_MISSING', 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                           AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                        v_reason :=
                          SUBSTR (   v_reason
                                  || '<br>'
                                  || 'SKU '
                                  || v_missed_item
                                  || ' is missing from file (but exists on ASN).'
                                 ,1
                                 ,4000
                                 );
                      ELSE
                        NULL;
                      END IF;
                    END LOOP;

                    CLOSE get_missed_item;
                  END;
                ELSE
                  NULL;
                END IF;

                IF NVL (v_nontrade_po, 'N') = 'N'
                THEN   ------- change 5.5 Added if Condition Id NONTRADE po Then SKIP ASN Validation
                  --validation for quantity_shipped, item number and duplicate SN
                  fnd_file.put_line (fnd_file.LOG
                                    ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                      || ' Validate Quantity for a Trade PO'
                                    );

                  OPEN validate_shipped_qty (v_asn_number, v_manufacturer);

                  LOOP
                    FETCH validate_shipped_qty
                     INTO v_invoice, v_item_num;

                    EXIT WHEN validate_shipped_qty%NOTFOUND;

                    /*Validation for quantity_shipped*/
                    BEGIN
                      SELECT NVL (SUM (l.quantity_shipped), 0)
                        INTO v_quantity_shipped
                        FROM rcv_shipment_headers h
                            ,rcv_shipment_lines l
                            ,po_line_locations_all pll
                            ,mtl_system_items_b msi
                       WHERE 1 = 1
                         AND TRIM (h.shipment_num) = v_asn_number   --<ASN_NUMBER>    --'5700410631'
                         --AND pll.attribute2 = v_po_line_ref
                         AND h.vendor_id =
                               (SELECT lookup_code
                                  FROM fnd_lookup_values
                                 WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                   AND meaning = UPPER (v_manufacturer)
                                   AND enabled_flag = 'Y')
                         AND h.receipt_source_code = 'VENDOR'
                         AND h.asn_type = 'ASN'
                         AND l.shipment_header_id = h.shipment_header_id
                         AND l.source_document_code = 'PO'
                         AND pll.line_location_id = l.po_line_location_id
                         AND msi.inventory_item_id = l.item_id
                         AND msi.organization_id = l.to_organization_id           --488 --master org
                         AND msi.segment1 = v_item_num;
                    EXCEPTION
                      WHEN NO_DATA_FOUND
                      THEN
                        v_quantity_shipped := 0;
                        xxntgr_debug_utl.LOG (   'Error while getting Shipped_Quantity for ASN: '
                                              || v_asn_number
                                              || SQLCODE
                                              || SQLERRM
                                             ,'2'
                                             );
                      WHEN OTHERS
                      THEN
                        v_quantity_shipped := 0;
                        v_err_location := v_err_location || 'D';                       -- mmore 3.4
                        v_errmsg :=
                             'Error while getting Shipped_Quantity for ASN: '
                          || v_asn_number
                          || SQLCODE
                          || SQLERRM;

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                           AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                           AND item_number = v_item_num;

                        xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');
                    END;

                    fnd_file.put_line (fnd_file.LOG
                                      ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                        || ' Quantity Shipped-'
                                        || v_quantity_shipped
                                      );

                    BEGIN
                      --count the total line of staging table for asn_number to get the total quantity recieved
                      SELECT COUNT (*)
                        INTO v_count_stg
                        FROM xxntgr_ptm_odm_shipment_stg
                       WHERE asn_number = v_asn_number
                         AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                         AND item_number = v_item_num
--                      AND top_serial_number is null;
                         AND NVL (top_serial_number, serial_number_product) = serial_number_product;

                      fnd_file.put_line (fnd_file.LOG
                                        ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                          || ' Quantity in PTM file-'
                                          || v_count_stg
                                        );

                      -- 6.0
                      IF v_count_stg <> 0
                      THEN
                        IF v_count_stg <> v_quantity_shipped
                        THEN
                          --update staging table to error, and send mail to the contacts
                          fnd_file.put_line (fnd_file.LOG
                                            ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                              || 'Update xxntgr_ptm_odm_shipment_stg 10'
                                            );

                          UPDATE xxntgr_ptm_odm_shipment_stg
                             SET process_status = 'E'
                                ,process_message =
                                       SUBSTR (process_message || ' ' || 'QTY_SN_MISMATCH', 1, 4000)
                           WHERE asn_number = v_asn_number
                             AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                             AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                             AND item_number = v_item_num;

                          fnd_file.put_line (fnd_file.LOG
                                            ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                              || 'Update xxntgr_ptm_odm_shipment_stg 10'
                                            );
                          v_reason :=
                            SUBSTR
                              (   v_reason
                               || '<br>'
                               || 'The quantity shipped on ASN does not match the total serial numbers in file for SKU '
                               || v_item_num
                              ,1
                              ,4000
                              );
                        END IF;
                      END IF;
                    EXCEPTION
                      WHEN OTHERS
                      THEN
                        v_err_location := v_err_location || 'E';                       -- mmore 3.4
                        v_errmsg := 'Error in Quantity_Received validation. ' || SQLCODE || SQLERRM;
                        fnd_file.put_line (fnd_file.LOG
                                          ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                            || 'Update xxntgr_ptm_odm_shipment_stg 11'
                                          );

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                           AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                           AND item_number = v_item_num;

                        fnd_file.put_line (fnd_file.LOG
                                          ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                            || 'Update xxntgr_ptm_odm_shipment_stg 11'
                                          );
                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 9');
                        xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');
                    END;
                  END LOOP;

                  CLOSE validate_shipped_qty;

                  fnd_file.put_line (fnd_file.LOG
                                    ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                      || ' Validate Quantity for a Trade PO Done'
                                    );
                  fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Validate PO');

                  ---Validation for PO Number
                  FOR i IN validate_po (v_asn_number, v_manufacturer)
                  LOOP
                    BEGIN
                      SELECT DISTINCT p.segment1
                                 INTO v_po_num
                                 FROM rcv_shipment_headers h, rcv_shipment_lines l
                                     ,po_headers_all p
                                WHERE 1 = 1
                                  AND l.shipment_header_id = h.shipment_header_id
                                  AND l.po_header_id = p.po_header_id
                                  AND p.segment1 = i.po_number
                                  AND TRIM (h.shipment_num) = v_asn_number
                                  AND h.vendor_id =
                                        (SELECT lookup_code
                                           FROM fnd_lookup_values
                                          WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                            AND meaning = UPPER (v_manufacturer)
                                            AND enabled_flag = 'Y');
                    EXCEPTION
                      WHEN NO_DATA_FOUND
                      THEN
                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                     SUBSTR (process_message || ' ' || 'INVALID_PO_NUMBER', 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND po_number = i.po_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                           AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                        v_reason :=
                          SUBSTR (v_reason || '<br>' || 'PO Number ' || i.po_number
                                  || ' is invalid.'
                                 ,1
                                 ,4000
                                 );
                      WHEN OTHERS
                      THEN
                        v_err_location := v_err_location || 'F';                       -- mmore 3.4
                        v_errmsg := 'Error while validating PO Number; ' || SQLCODE || SQLERRM;

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND po_number = i.po_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                           AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                        xxntgr_debug_utl.LOG (   'ASN:'
                                              || v_asn_number
                                              || '-'
                                              || 'Error while validating PO Number; '
                                              || SQLCODE
                                              || SQLERRM
                                             ,'1'
                                             );
                    END;
                  END LOOP;
                END IF;

                ------- change 5.5 Added if Condition Id NONTRADE po Then SKIP ASN Validation
                fnd_file.put_line (fnd_file.LOG
                                  , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Validate Top Serial Number'
                                  );

                --Validation for INVALID_TOP_SN
                FOR i IN validate_top_serial (v_asn_number, v_manufacturer)
                LOOP
                  BEGIN
                    fnd_file.put_line (fnd_file.LOG
                                      ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                        || ' Invalid Top Serial Number-'
                                        || i.top_serial_number
                                      );

                    UPDATE xxntgr_ptm_odm_shipment_stg
                       SET process_status = 'E'
                          ,process_message =
                                        SUBSTR (process_message || ' ' || 'INVALID TOP SN', 1, 4000)
                     WHERE asn_number = v_asn_number
                       AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                       AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                       AND top_serial_number = i.top_serial_number;

                    v_reason :=
                      SUBSTR (   v_reason
                              || '<br>'
                              || 'Top Serial Number '
                              || i.top_serial_number
                              || ' referenced by child item '
                              || i.item_number
                              || ' is non-existent.'
                             ,1
                             ,4000
                             );
                  EXCEPTION
                    WHEN OTHERS
                    THEN
                      v_err_location := v_err_location || 'G';                         -- mmore 3.4
                      v_errmsg := 'Error while getting INVALID_TOP_SN ' || SQLCODE || SQLERRM;

                      UPDATE xxntgr_ptm_odm_shipment_stg
                         SET process_status = 'E'
                            ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                       WHERE asn_number = v_asn_number
                         AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                         AND top_serial_number = i.top_serial_number;

                      xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');
                  END;

                  EXIT;
                END LOOP;

                fnd_file.put_line (fnd_file.LOG
                                  ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                    || ' Validate Top Serial Number Done'
                                  );
                fnd_file.put_line (fnd_file.LOG
                                  , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Validate Davinci Items'
                                  );
                --- Change 6.5 Added If condition IF Profile XXNTGR_PTM_DAVINCI_VALIDATION value is Y then only Validate
                v_davinci_validation := NULL;

                SELECT MAX (profile_option_value)
                  INTO v_davinci_validation
                  FROM fnd_profile_option_values
                 WHERE profile_option_id =
                                      (SELECT profile_option_id
                                         FROM fnd_profile_options
                                        WHERE profile_option_name = 'XXNTGR_PTM_DAVINCI_VALIDATION');

                IF v_davinci_validation = 'Y'
                THEN
                  -- Davinci Validation
                  FOR i IN validate_items_davinci (v_asn_number, v_manufacturer)
                  LOOP
                    BEGIN
                      fnd_file.put_line (fnd_file.LOG
                                        ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                          || ' Item Number-'
                                          || i.item_number
                                        );

                      -- FA Number
                      SELECT material, rev
                        INTO v_davinci_item, v_davinci_item_rev
                        FROM serial_control
                       WHERE material = i.item_number;

                      fnd_file.put_line (fnd_file.LOG
                                        ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                          || ' v_davinci_item-'
                                          || v_davinci_item
                                        );

                      IF v_davinci_item IS NOT NULL
                      THEN
                        -- Check if 90 level item(ta_number) is also a Davinci item
                        BEGIN                                                                   --2
                          SELECT material
                            INTO v_davinci_ta_item
                            FROM serial_control
                           WHERE material = i.assembly_number;

                          fnd_file.put_line (fnd_file.LOG
                                            ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                              || ' v_davinci_ta_item-'
                                              || v_davinci_ta_item
                                            );

                          IF v_davinci_ta_item IS NOT NULL
                          THEN
                            fnd_file.put_line (fnd_file.LOG
                                              ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                                || ' Validate TA Number attributes'
                                              );

                            -- validate ta number attributes
                            FOR j IN validate_tanumber_davinci (v_asn_number
                                                               ,v_manufacturer
                                                               ,i.item_number
                                                               )
                            LOOP
                              v_ta_err_msg_tl := NULL;

                              -- Validate if the both the items(item and TA) are same then revs should be same for Davinci items
                              IF i.item_number = j.assembly_number
                              THEN
                                IF i.item_revision <> j.assembly_revision
                                THEN
                                  v_reason :=
                                    SUBSTR
                                      (   v_reason
                                       || '<br>'
                                       || ' SKU -'
                                       || i.item_number
                                       || ' ITEM Number and Assembly Number are same in the file. But, Item rev is not same as Assembly rev.'
                                      ,1
                                      ,4000
                                      );
                                END IF;
                              END IF;

                              xxntgr_debug_utl.LOG ('After Seting v_process_mode 16');

                              SELECT *
                                INTO v_item_attributes_rec
                                FROM serial_control
                               WHERE material = j.assembly_number;

                              xxntgr_debug_utl.LOG ('After Seting v_process_mode 16');
                              fnd_file.put_line (fnd_file.LOG
                                                , 'check SN for ' || j.serial_number_product
                                                );
                              check_serial_number (j.serial_number_product
                                                  ,v_item_attributes_rec
                                                  ,v_ta_err_msg
                                                  );

                              IF v_ta_err_msg IS NOT NULL
                              THEN
                                v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg || '; ';
                              END IF;

                              fnd_file.put_line (fnd_file.LOG
                                                , 'check revision for ' || j.assembly_revision
                                                );
                              -- CheckRev
                              check_revision (j.assembly_number
                                             ,j.assembly_revision
                                             ,v_item_attributes_rec
                                             ,v_ta_err_msg
                                             );

                              IF v_ta_err_msg IS NOT NULL
                              THEN
                                v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg || '; ';
                              END IF;

                              check_access_code (j.access_code
                                                ,j.assembly_number
                                                ,j.serial_number_product
                                                ,v_item_attributes_rec
                                                ,v_ta_err_msg
                                                );

                              IF v_ta_err_msg IS NOT NULL
                              THEN
                                v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg || '; ';
                              END IF;

                              --CheckMAC
                              check_mac (j.serial_number_product
                                        ,j.mac_address_product
                                        ,v_item_attributes_rec
                                        ,v_ta_err_msg
                                        );

                              IF v_ta_err_msg IS NOT NULL
                              THEN
                                v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg || '; ';
                              END IF;

                              --CheckSSID(primary_ssid)
                              check_ssid (j.serial_number_product
                                         ,j.mac_address_product
                                         ,j.primary_ssid
                                         ,v_manufacturer
                                         ,v_item_attributes_rec
                                         ,v_ta_err_msg
                                         );

                              IF v_ta_err_msg IS NOT NULL
                              THEN
                                v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg || '; ';
                              END IF;

                              --CheckSSID1 -- GP 29-0ct-12
                              IF j.ssid1 IS NOT NULL
                              THEN
                                check_ssid1 (j.serial_number_product
                                            ,j.mac_address_product
                                            ,j.ssid1
                                            ,v_manufacturer
                                            ,v_item_attributes_rec
                                            ,v_ta_err_msg
                                            );

                                IF v_ta_err_msg IS NOT NULL
                                THEN
                                  v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg || '; ';
                                END IF;
                              END IF;

                              --CheckWEP
                              check_wep (j.wep_key
                                        ,j.serial_number_product
                                        ,v_item_attributes_rec
                                        ,v_ta_err_msg
                                        );

                              IF v_ta_err_msg IS NOT NULL
                              THEN
                                v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg || '; ';
                              END IF;

                              --CheckWPA
                              check_wpa (j.wpa_key
                                        ,j.serial_number_product
                                        ,v_item_attributes_rec
                                        ,v_ta_err_msg
                                        );

                              IF v_ta_err_msg IS NOT NULL
                              THEN
                                v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg || '; ';
                              END IF;

                              --CheckWIFI
                              check_wifi (j.wifi_id
                                         ,j.serial_number_product
                                         ,v_item_attributes_rec
                                         ,v_ta_err_msg
                                         );

                              IF v_ta_err_msg IS NOT NULL
                              THEN
                                v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg;
                              END IF;

                              IF v_ta_err_msg_tl IS NOT NULL
                              THEN
                                xxntgr_debug_utl.LOG ('After Seting v_process_mode 17');
                                fnd_file.put_line (fnd_file.LOG
                                                  ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                                    || 'Update xxntgr_ptm_odm_shipment_stg 16'
                                                  );

                                UPDATE xxntgr_ptm_odm_shipment_stg
                                   SET process_status = 'E'
                                      ,process_message =
                                         SUBSTR (process_message || ' ' || 'TA NUMBER VALIDATION'
                                                ,1
                                                ,4000
                                                )
                                 WHERE asn_number = v_asn_number
                                   AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                                   AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                                   AND item_number = i.item_number
                                   AND serial_number_product = j.serial_number_product;

                                fnd_file.put_line (fnd_file.LOG
                                                  ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                                    || 'Update xxntgr_ptm_odm_shipment_stg 16'
                                                  );
                                xxntgr_debug_utl.LOG ('After Seting v_process_mode 17');
                                v_reason :=
                                  SUBSTR (   v_reason
                                          || '<br>'
                                          || 'SKU '
                                          || i.assembly_number
                                          || ' - '
                                          || v_ta_err_msg_tl
                                         ,1
                                         ,4000
                                         );
                                xxntgr_debug_utl.LOG (   ' ****** TA NUMBER VALIDATION FAILED -'
                                                      || v_reason
                                                     ,'3'
                                                     );
                              END IF;
                            END LOOP;                           -- end validate ta number attributes

                            fnd_file.put_line (fnd_file.LOG
                                              ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                                || ' Validate TA Number attributes Done'
                                              );
                          END IF;                           -- IF v_davinci_ta_item is NOT NULL THEN
                        EXCEPTION
                          WHEN NO_DATA_FOUND
                          THEN
                            UPDATE xxntgr_ptm_odm_shipment_stg
                               SET process_status = 'E'
                                  ,process_message =
                                     SUBSTR
                                       (   process_message
                                        || ' '
                                        || ' TA NUMBER INVALID - Assembly Number does not exists in serial_control'
                                       ,1
                                       ,4000
                                       )
                             WHERE asn_number = v_asn_number
                               AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                               AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                               AND item_number = i.item_number;

                            v_reason :=
                              SUBSTR (   v_reason
                                      || '<br>'
                                      || 'ASEEMBLY NUMBER '
                                      || i.assembly_number
                                      || ' does NOT exist in SERIAL_CONTROL table.'
                                     ,1
                                     ,4000
                                     );
                        END;
                      ELSE                                -- means a legacy item. not a davinci item
                        NULL;
                      END IF;                                          -- v_davinci_item is NOT NULL
                    EXCEPTION
                      WHEN NO_DATA_FOUND
                      THEN
                        --XXNTGR_DEBUG_UTL.LOG('-- not a davinci item  - else legacy item'||v_asn_number);
                        v_davinci_item_rev := NULL;

                        BEGIN
                          SELECT material
                            INTO v_davinci_ta_item
                            FROM serial_control
                           WHERE material = i.assembly_number;
                        EXCEPTION
                          WHEN NO_DATA_FOUND
                          THEN
                            v_davinci_ta_item := NULL;
                        END;

                        IF v_davinci_ta_item IS NOT NULL
                        THEN
                          UPDATE xxntgr_ptm_odm_shipment_stg
                             SET process_status = 'E'
                                ,process_message =
                                    SUBSTR (process_message || ' ' || ' TA NUMBER INVALID', 1, 4000)
                           WHERE asn_number = v_asn_number
                             AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                             AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                             AND item_number = i.item_number;

                          v_reason :=
                            SUBSTR
                              (   v_reason
                               || '<br>'
                               || 'Assembly Number '
                               || i.assembly_number
                               || ' should not be a Davinci item as the item number is not a Davinci item'
                              ,1
                              ,4000
                              );
                        END IF;
                      WHEN OTHERS
                      THEN
                        v_err_location := v_err_location || 'I';                       -- mmore 3.4
                        v_errmsg :=
                           'Error in Davinci Item number validation. ' || SQLCODE || '-' || SQLERRM;
                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 20');

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                           AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                           AND item_number = i.item_number;

                        xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');
                    END;

                    -- Validate item number rev
                    IF v_davinci_item_rev IS NOT NULL
                    THEN
                      v_item_rev_rec.rev := v_davinci_item_rev;
                      check_revision (i.item_number, i.item_revision, v_item_rev_rec, v_ta_err_msg);

                      IF v_ta_err_msg IS NOT NULL
                      THEN
                        v_ta_err_msg_tl := v_ta_err_msg_tl || v_ta_err_msg;
                      END IF;

                      xxntgr_debug_utl.LOG (' *** v_ta_err_msg -' || v_ta_err_msg);

                      IF v_ta_err_msg IS NOT NULL
                      THEN
                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 21');
                        fnd_file.put_line (fnd_file.LOG
                                          ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                            || 'Update xxntgr_ptm_odm_shipment_stg 20'
                                          );

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                 SUBSTR (process_message || ' ' || 'ITEM NUMBER REV INVALID'
                                        ,1
                                        ,4000
                                        )
                         WHERE asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                           AND NVL (process_status, 'NULL') NOT IN ('P', 'S')
                           AND item_number = i.item_number;

                        fnd_file.put_line (fnd_file.LOG
                                          ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                            || 'Update xxntgr_ptm_odm_shipment_stg 21'
                                          );
                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 21');
                        v_reason :=
                          SUBSTR (   v_reason
                                  || '<br>'
                                  || 'SKU '
                                  || i.item_number
                                  || ' Davinci Item Number Rev Validation failed - '
                                  || i.item_revision
                                  || '-'
                                  || v_ta_err_msg
                                 ,1
                                 ,4000
                                 );
                      END IF;
                    END IF;
                  END LOOP;                                   --  End Loop of validate_items_davinci

                  fnd_file.put_line (fnd_file.LOG
                                    ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                      || ' Validate Davinci Items Done - '
                                      || v_reason
                                    );
                END IF;

                --- Change 6.5 Added Above If condition IF Profile XXNTGR_PTM_DAVINCI_VALIDATION value is Y then only Validate

                -- END IF; --removed by ppatil 3.10
                 -- GP END
                IF v_reason IS NOT NULL
                THEN
                  --Call the send mail program to send mail to contacts
                  BEGIN
                    xxntgr_ptm_odm_pkg.ptm_odm_ship_mail (v_asn_number
                                                         ,UPPER (v_manufacturer)
                                                         ,v_file_name
                                                         ,SUBSTR (v_reason, 1, 1500)
                                                         ,v_get_mail_error
                                                         );
                    xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_get_mail_error, '1');
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      NULL;
                    WHEN OTHERS
                    THEN
                      NULL;
                  END;

                  xxntgr_debug_utl.LOG ('Program failed for following reasons : ' || v_reason, '1');
                END IF;

                /*VALIDATION ends here*/

                /*====Record processing starts here===*/
                BEGIN                                                                            --3
                  xxntgr_debug_utl.LOG (' *** Record processing STARTS -' || v_ta_err_msg, '3');
                  fnd_file.put_line (fnd_file.LOG
                                    , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Record Processing Starts'
                                    );

                  BEGIN
                    SELECT DISTINCT process_status
                               INTO v_proc_status
                               FROM xxntgr_ptm_odm_shipment_stg
                              WHERE asn_number = v_asn_number
                                AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                                AND process_status = 'E';
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      NULL;
                    WHEN OTHERS
                    THEN
                      NULL;
                  END;

                  fnd_file.put_line (fnd_file.LOG
                                    ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                      || ' v_proc_status - '
                                      || v_proc_status
                                    );

                  IF v_proc_status = 'E'
                  THEN
                    --Insert error data into archive table
                    INSERT INTO xxntgr_ptm_odm_shipment_arch
                      SELECT *
                        FROM xxntgr_ptm_odm_shipment_stg
                       WHERE asn_number = v_asn_number
                         AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                    --Delete error data from staging table
                    DELETE FROM xxntgr_ptm_odm_shipment_stg
                          WHERE asn_number = v_asn_number
                            AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                            AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                    retcode := 1;
                  ELSE                                                         -- process_status = P
                    BEGIN                                                                       --5
                      -- XXNTGR_DEBUG_UTL.LOG(' **** process_status <> E - ' ||v_ta_err_msg);
                      BEGIN                                                                     --6
                        --Set the process_status to "P" (Processed) for validated records(ASN)
                        fnd_file.put_line (fnd_file.LOG
                                          ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                            || ' Update Process Status to P'
                                          );

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'P'
--                         WHERE (process_status = 'U' OR process_status IS NULL)
                        WHERE  process_status = 'V'
                           AND asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer);

                        fnd_file.put_line (fnd_file.LOG
                                          , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Update Instance ID'
                                          );

                        --get the instance_id value from the sequence and update staging table
                        FOR upd_instanceid IN (SELECT serial_number_product
                                                 FROM xxntgr_ptm_odm_shipment_stg
                                                WHERE asn_number = v_asn_number
                                                  AND UPPER (manufacturer_name) =
                                                                              UPPER (v_manufacturer)
                                                  AND process_status = 'P')
                        LOOP
                          SELECT xxntgr_ptm_item_instances_s.NEXTVAL
                            INTO v_instance_id
                            FROM DUAL;

                          UPDATE xxntgr_ptm_odm_shipment_stg
                             SET instance_id = v_instance_id
                           WHERE serial_number_product = upd_instanceid.serial_number_product
                             AND asn_number = v_asn_number
                             AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                             AND process_status = 'P';
                        END LOOP;

                        fnd_file.put_line (fnd_file.LOG
                                          ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                            || ' Update Instance ID Done'
                                          );
                      EXCEPTION
                        WHEN OTHERS
                        THEN
                          xxntgr_debug_utl.LOG (   'ASN:'
                                                || v_asn_number
                                                || '-'
                                                || 'Error while updating instance id. '
                                                || SQLCODE
                                                || SQLERRM
                                               ,'1'
                                               );
                      END;                                                                       --6

                      FOR dist_items IN get_dist_items (v_asn_number, v_manufacturer)
                      LOOP
                        --Get aditional column values, which are not coming from staging table, to populate XXNTGR_PTM_ITEM_INSTANCES,XXNTGR_PTM_TRX_REGISTER and XXNTGR_PTM_PO_SHIPMENT_TRX tables
                        BEGIN                                                                   --7
                          fnd_file.put_line (fnd_file.LOG
                                            ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                              || ' Getting additional ASN details for '
                                              || dist_items.item_number
                                            );

                          SELECT DISTINCT
                                          --pll.attribute2 --SUPPLIER_SALES_ORDER
                                          NULL                                      --l.PO_HEADER_ID
                                              , NULL                                  --l.PO_LINE_ID
                                         ,NULL                               --l.PO_LINE_LOCATION_ID
                                              , h.shipment_header_id
                                         ,NULL                                  --l.SHIPMENT_LINE_ID
                                              , msi.inventory_item_id
                                         --,msi.segment1
                          ,               msi.organization_id, NULL                --l.CONTAINER_NUM
                                         --,mtt.transaction_type_name
                                         --,mtst.transaction_source_type_name
                                         --,mmt.subinventory_code
                          ,               l.to_subinventory
                                         ,ooha.sold_to_org_id                  --sold_to_customer_id
                                         ,mp.organization_code
                                     INTO
                                          --v_supplier_so,
                                          v_po_header_id, v_po_line_id
                                         ,v_po_line_loc_id, v_shipment_header_id
                                         ,v_shipment_line_id, v_inventory_item_id
                                         ,
                                          --v_item_no,
                                          v_organization_id, v_container_num
                                         ,
                                          --v_trans_type,
                                          --v_trans_source_type,
                                          v_subinv_code
                                         ,v_sold_to_customer_id
                                         ,v_ship_to_org_code
                                     FROM rcv_shipment_headers h
                                         ,rcv_shipment_lines l
                                         ,
                                          --rcv_transactions rt,
                                          --mtl_material_transactions mmt,
                                          po_line_locations_all pll
                                         ,mtl_system_items_b msi
                                         ,
                                          --mtl_transaction_types mtt,
                                          --MTL_TXN_SOURCE_TYPES mtst,
                                          oe_order_headers_all ooha
                                         ,mtl_parameters mp
                                    WHERE 1 = 1
                                      AND TRIM (h.shipment_num) = v_asn_number
                                      --and pll.attribute2 = v_po_line_ref
                                      AND msi.segment1 = dist_items.item_number
                                      AND h.vendor_id =
                                            (SELECT lookup_code
                                               FROM fnd_lookup_values
                                              WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                                AND meaning = UPPER (v_manufacturer)
                                                AND enabled_flag = 'Y')
                                      AND h.receipt_source_code = 'VENDOR'
                                      AND h.asn_type = 'ASN'
                                      AND l.shipment_header_id = h.shipment_header_id
                                      --and h.shipment_header_id = rt.shipment_header_id
                                      --and l.shipment_line_id = rt.shipment_line_id
                                      --and rt.po_line_id = l.po_line_id
                                      --and rt.po_line_id = pll.po_line_id
                                      --and rt.transaction_type = 'DELIVER'
                                      --and rt.transaction_id = mmt.rcv_transaction_id
                                      AND l.source_document_code = 'PO'
                                      AND pll.line_location_id = l.po_line_location_id
                                      AND msi.inventory_item_id = l.item_id
                                      AND msi.organization_id = l.to_organization_id
                                                                                  --488 --master org
                                      --and mmt.transaction_type_id = mtt.transaction_type_id
                                      --and mmt.transaction_source_type_id = mtst.transaction_source_type_id
                                      AND l.oe_order_header_id = ooha.header_id(+)
                                      AND l.to_organization_id = mp.organization_id;
                        EXCEPTION
                          WHEN NO_DATA_FOUND
                          THEN
                            NULL;
                          WHEN OTHERS
                          THEN
                            v_err_location := v_err_location || 'J';                   -- mmore 3.4
                            v_errmsg :=
                                 'Error while getting additional column values. '
                              || SQLCODE
                              || '-'
                              || SQLERRM;
                            xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');

                            UPDATE xxntgr_ptm_odm_shipment_stg
                               SET process_status = 'E'
                                  ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                             WHERE asn_number = v_asn_number
                               AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                               AND process_status = 'P';

                            --Insert error data into archive table
                            INSERT INTO xxntgr_ptm_odm_shipment_arch
                              SELECT *
                                FROM xxntgr_ptm_odm_shipment_stg
                               WHERE asn_number = v_asn_num
                                 AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                                 AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                            --Delete error data from staging table
                            DELETE FROM xxntgr_ptm_odm_shipment_stg
                                  WHERE process_status = 'E'
                                    AND asn_number = v_asn_num
                                    AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                                    AND NVL (process_status, 'NULL') NOT IN ('P', 'S');
                        END;                                                                     --7

                        BEGIN
                          SELECT DISTINCT process_status
                                     INTO v_proc_status
                                     FROM xxntgr_ptm_odm_shipment_stg
                                    WHERE asn_number = v_asn_number
                                      AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                                      AND process_status = 'P';
                        EXCEPTION
                          WHEN NO_DATA_FOUND
                          THEN
                            NULL;
                          WHEN OTHERS
                          THEN
                            NULL;
                        END;

                        fnd_file.put_line (fnd_file.LOG
                                          ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                            || ' Getting Ship to Org ID'
                                          );

                        -- Check if the shipment is going to APL(N10)  GP 11-DEC-13
                        BEGIN
                          SELECT DISTINCT DECODE (l.to_organization_id
                                                 ,506, 'KERRY'
                                                 ,490, 'APL'
                                                 ,497, 'DSV'
                                                 ,l.to_organization_id
                                                 )
                                     INTO v_to_shipment_org
                                     FROM rcv_shipment_headers h, rcv_shipment_lines l
                                    WHERE 1 = 1
                                      AND h.receipt_source_code = 'VENDOR'
                                      AND h.asn_type = 'ASN'
                                      AND l.shipment_header_id = h.shipment_header_id
                                      AND l.source_document_code = 'PO'
                                      AND h.vendor_id IN (
                                            SELECT lookup_code
                                              FROM fnd_lookup_values
                                             WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                               AND enabled_flag = 'Y')
                                      AND TRIM (h.shipment_num) = v_asn_number;

                          xxntgr_debug_utl.LOG ('After Seting v_process_mode 31');
                        EXCEPTION
                          WHEN NO_DATA_FOUND
                          THEN
                            xxntgr_debug_utl.LOG ('TO SHIP ORG NOT FOUND');
                          WHEN OTHERS
                          THEN
                            xxntgr_debug_utl.LOG ('Error while getting TO SHIPMENT ORG');
                        END;

                        -- END GP
                        IF v_proc_status = 'P'
                        THEN
                          fnd_file.put_line (fnd_file.LOG
                                            ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                              || ' Start Inserting Data'
                                            );

                          FOR i IN get_ptm_data (v_asn_number
                                                ,v_manufacturer
                                                ,dist_items.item_number
                                                )
                          LOOP
                            BEGIN
                              --get value for mac_address/other_macs
                              BEGIN
                                SELECT INSTR (i.mac_address_product, '|')
                                  INTO v_mac_add
                                  FROM DUAL;

                                IF v_mac_add = 0
                                THEN
                                  v_mac_address := i.mac_address_product;
                                  v_other_macs := NULL;
                                ELSE
                                  v_mac_address :=
                                    SUBSTR (i.mac_address_product
                                           ,1
                                           , (INSTR (i.mac_address_product, '|') - 1)
                                           );
                                END IF;
                              END;

                              -- convert hex to number/sort/concatinate mac_addresses and store in other_macs column
                              IF v_mac_add <> 0
                              THEN
                                BEGIN
                                  pos := INSTR (i.mac_address_product, p_delim, 1, 1);
                                  -- determine first chunk of string
                                  k := 0;

                                  WHILE (pos != 0)
                                  LOOP                         -- while there are chunks left, loop
                                    multimacs_char (k) :=
                                                         SUBSTR (i.mac_address_product, 1, pos - 1);
                                    multimacs (k) :=
                                      TO_NUMBER (NVL (SUBSTR (i.mac_address_product, 1, pos - 1), 0)
                                                ,'XXXXXXXXXXXX'
                                                );       -- create array element for chuck of string
                                    i.mac_address_product :=
                                      SUBSTR (i.mac_address_product
                                             , pos + 1
                                             ,LENGTH (i.mac_address_product)
                                             );                          -- remove chunk from string
                                    pos := INSTR (i.mac_address_product, p_delim, 1, 1);

                                    -- determine next chunk
                                    IF pos = 0
                                    THEN                              -- no last chunk, add to array
                                      multimacs (k + 1) :=
                                         TO_NUMBER (NVL (i.mac_address_product, 0), 'XXXXXXXXXXXX');
                                      multimacs_char (k + 1) := i.mac_address_product;
                                    END IF;

                                    k := k + 1;
                                  END LOOP;

                                  --get the array size
                                  n := multimacs.COUNT ();

                                  -- bubble sort - using two loops, compare each value with the one after it
                                  -- if the one after the current one is smaller , then switch their locations in the array
                                  FOR i IN 0 .. n - 1
                                  LOOP
                                    FOR j IN 0 .. n - (i + 1) - 1
                                    LOOP
                                      IF (multimacs (j) > multimacs (j + 1))
                                      THEN
                                        temp := multimacs (j);
                                        multimacs (j) := multimacs (j + 1);
                                        multimacs (j + 1) := temp;
                                      END IF;
                                    END LOOP;
                                  END LOOP;

                                  v_other_macs := NULL;

                                  FOR i IN multimacs.FIRST .. multimacs.LAST
                                  LOOP
                                    v_mac := TO_CHAR (multimacs (i), 'FMXXXXXXXXXXXX');

                                    FOR m IN multimacs_char.FIRST .. multimacs_char.LAST
                                    LOOP
                                      IF v_mac =
                                           SUBSTR (multimacs_char (m)
                                                  ,-LENGTH (v_mac)
                                                  ,LENGTH (v_mac)
                                                  )
                                      THEN
                                        v_mac := multimacs_char (m);
                                        EXIT;
                                      END IF;
                                    END LOOP;

                                    IF multimacs (i) IS NOT NULL AND multimacs (i) <> 0
                                    THEN
                                      IF v_other_macs IS NULL
                                      THEN
                                        v_other_macs := v_mac;
                                      ELSE
                                        v_other_macs := v_other_macs || ':' || v_mac;
                                      END IF;
                                    END IF;
                                  END LOOP;
                                END;
                              END IF;
                            -- End convert hex to number/sort/concatinate
                            END;

                            xxntgr_debug_utl.LOG (   TO_CHAR (SYSDATE, 'HH:MI:SS')
                                                  || ' MAC Address Done for:'
                                                  || i.serial_number_product
                                                 );

                            -- 4.4 Added SERIAL_NUMBER_01 - SERIAL_NUMBER_09 columns in the below insert script
                            IF i.processed_flag = 'I'
                            THEN                                              --added by ppatil 3.10
                              INSERT INTO xxntgr_ptm_item_instances
                                          (instance_id, serial_number, item_number
                                          ,inventory_item_id, item_revision, mac_address
                                          ,imei_number, masterlock_number
                                          ,networklock_number, servicelock_number
                                          ,assembly_revision, assembly_number, wep_key
                                          ,wifi_id, access_code, primary_ssid, wpa_key
                                          ,mac_id_cable, mac_id_emta, hardware_version
                                          ,firmware_version, ean_code, software_version
                                          ,srm_password, mac_id_rf, mac_id_mta
                                          ,mac_id_mta_man_router, mac_id_mta_data
                                          ,mac_id_ethernet, mac_id_usb
                                          ,primaryssid_passphrase, mac_id_cmci, mac_id_lan
                                          ,mac_id_wan, mac_id_device, mac_id_wireless
                                          ,mac_id_wifi_ssid1, ssid1, ssid1_passphrase
                                          ,wpa_passphrase, wps_pin_code, pppoa_username
                                          ,pppoa_passphrase, tr069_unique_key_64bit
                                          ,fon_key, ta_number, other_macs,
                                                                          --CREATED_BY,
                                                                          creation_date
                                          ,
                                           --LAST_UPDATED_BY,
                                           last_update_date, start_date_active, end_date_active
                                          ,last_transaction_type, warranty_begin_date
                                          ,warranty_end_date, warranty_period, install_date
                                          ,installed_at_party_site_id, sold_to_customer_id
                                          ,attribute1, attribute2, attribute3, attribute4
                                          ,attribute5, attribute6, attribute7, attribute8
                                          ,attribute9, attribute10, attribute11
                                          ,attribute12, attribute13, attribute14
                                          ,attribute15, serial_number_01, serial_number_02
                                          ,serial_number_03, serial_number_04
                                          ,serial_number_05, serial_number_06
                                          ,serial_number_07, serial_number_08
                                          ,serial_number_09, top_serial_number
                                          ,bt_mac_address, cert_id, battery_sn
                                          ,bt_secure_key, apple_home_key, lights_auth_code
                                          ,lights_uuid
                                          ,product_id
                                          ,bundle_flag
                                          ,coupon_code          -------------   Added for Change 6.4
                                          ,speaker_box_sn                    -- Added for change 6.6
                                          ,license_key                       -- Added for change 6.6
										  ,meural_prod_key                   -- Added for change 6.8
										  ,rfid								 --Added for change 6.9	
                                          )
                                   VALUES (i.instance_id, i.serial_number_product, i.item_number
                                          ,v_inventory_item_id, i.item_revision, v_mac_address
                                          ,i.imei_number, i.masterlock_number
                                          ,i.networklock_number, i.servicelock_number
                                          ,i.assembly_revision, i.assembly_number, i.wep_key
                                          ,i.wifi_id, i.access_code, i.primary_ssid, i.wpa_key
                                          ,i.mac_id_cable, i.mac_id_emta, i.hardware_version
                                          ,i.firmware_version, i.ean_code, i.software_version
                                          ,i.srm_password, i.mac_id_rf, i.mac_id_mta
                                          ,i.mac_id_mta_man_router, i.mac_id_mta_data
                                          ,i.mac_id_ethernet, i.mac_id_usb
                                          ,i.primaryssid_passphrase, i.mac_id_cmci, i.mac_id_lan
                                          ,i.mac_id_wan, i.mac_id_device, i.mac_id_wireless
                                          ,i.mac_id_wifi_ssid1, i.ssid1, i.ssid1_passphrase
                                          ,i.wpa_passphrase, i.wps_pin_code, i.pppoa_username
                                          ,i.pppoa_passphrase, i.tr069_unique_key_64bit
                                          ,i.fon_key, i.ta_number, v_other_macs,
                                                                                --i.created_by,
                                                                                i.creation_date
                                          ,
                                           --i.last_updated_by,
                                           i.last_update_date, i.xfactory_date,  --start_date_active
                                                                               NULL
                                          ,                                        --end_date_active
                                           '01',                            --LAST_TRANSACTION_TYPE,
                                                NULL
                                          ,                                   --WARRANTY_BEGIN_DATE,
                                           NULL,                                --WARRANTY_END_DATE,
                                                NULL,                             --WARRANTY_PERIOD,
                                                     NULL
                                          ,                                          --INSTALL_DATE,
                                           NULL,                       --INSTALLED_AT_PARTY_SITE_ID,
                                                v_sold_to_customer_id
                                          ,                                   --SOLD_TO_CUSTOMER_ID,
                                           i.attribute1, i.attribute2, i.attribute3, i.attribute4
                                          ,                                             -- 5g_mac_id
                                           i.attribute5,                             -- acs_password
                                                        i.attribute6, i.attribute7, i.attribute8
                                          ,i.attribute9, i.attribute10, i.attribute11
                                          ,i.attribute12, i.attribute13, i.attribute14
                                          ,i.attribute15, i.serial_number_01, i.serial_number_02
                                          ,i.serial_number_03, i.serial_number_04
                                          ,i.serial_number_05, i.serial_number_06
                                          ,i.serial_number_07, i.serial_number_08
                                          ,i.serial_number_09, i.top_serial_number
                                          ,i.bt_mac_address, i.cert_id, i.battery_sn
                                          ,i.bt_secure_key, i.apple_home_key, i.lights_auth_code
                                          ,i.lights_uuid
                                          ,apps.xxntgr_ptm_common_pkg.get_product_id
                                                                                  (i.assembly_number)
                                          ,DECODE (l_bundle_item_tbl (   i.asn_number
                                                                      || '#'
                                                                      || dist_items.item_number
                                                                     )
                                                  ,'Y', DECODE (i.serial_number_product
                                                               ,i.top_serial_number, 'P'
                                                               ,'C'
                                                               )
                                                  ,NULL
                                                  )
                                          ,i.coupon_code        -------------   Added for Change 6.4
                                          ,i.speaker_box_sn                  -- Added for change 6.6
                                          ,i.license_key                     -- Added for change 6.6
										  ,i.meural_prod_key                 -- Added for change 6.8
										  ,i.rfid                            -- Added for change 6.9
                                          );

                              IF    i.top_serial_number IS NULL
                                 OR i.serial_number_product = i.top_serial_number
                              THEN
                                --Insert into XXNTGR_PTM_TRX_REGISTER table
                                INSERT INTO xxntgr_ptm_trx_register
                                            (transaction_id, instance_id
                                            ,item_number, inventory_item_id, item_revision
                                            ,organization_id, subinventory_code
                                            ,serial_number, transaction_type
                                            ,transaction_source_type
                                            ,transaction_source_header_id
                                            ,transaction_source_line_id, creation_date
                                            ,last_update_date
                                            )
                                     VALUES (xxntgr_ptm_trx_register_s.NEXTVAL, i.instance_id
                                            ,i.item_number, v_inventory_item_id, i.item_revision
                                            ,v_organization_id, v_subinv_code
                                            ,i.serial_number_product, '01'
                                            ,'PO'
                                            ,v_shipment_header_id
                                            ,v_shipment_line_id, i.creation_date
                                            ,i.last_update_date
                                            );

                                --Insert into XXNTGR_PTM_PO_SHIPMENT_TRX  table
                                INSERT INTO xxntgr_ptm_po_shipment_trx
                                            (instance_id, pallet_id, master_carton_id
                                            ,item_number, inventory_item_id, item_revision
                                            ,organization_id, serial_number
                                            ,asn_number, invoice_number, packing_slip_number
                                            ,container_number, po_number, po_line_reference
                                            ,xfactory_date, manufacturer_name
                                            ,date_of_manufacture, country_of_origin
                                            ,ship_to_org_code, creation_date
                                            ,last_update_date, po_header_id, po_line_id
                                            ,po_line_location_id, shipment_header_id
                                            ,shipment_line_id, warranty_period, input_file_id
                                            ,process_status, request_id
                                            ,attribute1
                                            )
                                     VALUES (i.instance_id, i.pallet_id, i.master_carton_id
                                            ,i.item_number, v_inventory_item_id, i.item_revision
                                            ,v_organization_id, i.serial_number_product
                                            ,i.asn_number, i.invoice_number, i.packing_slip_number
                                            ,i.container_number, i.po_number, i.po_line_reference
                                            ,i.xfactory_date, UPPER (i.manufacturer_name)
                                            ,i.date_of_manufacture, UPPER (i.country_of_origin)
                                            ,v_ship_to_org_code, i.creation_date
                                            ,i.last_update_date, v_po_header_id, v_po_line_id
                                            ,v_po_line_loc_id, v_shipment_header_id
                                            ,v_shipment_line_id, NULL, i.input_file_id
                                            ,i.process_status, v_reqstid
                                            ,i.attribute3                       -- optus SAP_NO (PO)
                                            );
                              END IF;

                              /* Removed by ppatil 3.11*/
                              xxntgr_debug_utl.LOG (   TO_CHAR (SYSDATE, 'HH:MI:SS')
                                                    || ' Inserted Data for:'
                                                    || i.serial_number_product
                                                   );
                            --added by ppatil 3.10
                            ELSIF i.processed_flag = 'U'
                            THEN
                              --(Process_mode = UPDATE) - UPDATE the PTM tables with the new data, based on old_instance_id added by ppatil 3.10
                              SELECT trx.instance_id, reg.transaction_type
                                INTO v_inst_id, v_trans_type
                                FROM xxntgr_ptm_po_shipment_trx trx
                                    ,xxntgr_ptm_item_instances inst
                                    ,xxntgr_ptm_trx_register reg
                               WHERE trx.instance_id = inst.instance_id
                                 AND inst.instance_id = reg.instance_id
                                 AND trx.asn_number = i.asn_number
                                 AND trx.manufacturer_name = UPPER (i.manufacturer_name)
                                 AND trx.serial_number = i.serial_number_product
                                 AND reg.transaction_type = '01';             --added by ppatil 3.10

                              UPDATE xxntgr_ptm_item_instances
                                 SET instance_id = i.instance_id
                                    ,serial_number = i.serial_number_product
                                    ,item_number = i.item_number
                                    ,inventory_item_id = v_inventory_item_id
                                    ,item_revision = i.item_revision
                                    ,mac_address = v_mac_address
                                    ,imei_number = i.imei_number
                                    ,masterlock_number = i.masterlock_number
                                    ,networklock_number = i.networklock_number
                                    ,servicelock_number = i.servicelock_number
                                    ,assembly_revision = i.assembly_revision
                                    ,assembly_number = i.assembly_number
                                    ,wep_key = i.wep_key
                                    ,wifi_id = i.wifi_id
                                    ,access_code = i.access_code
                                    ,primary_ssid = i.primary_ssid
                                    ,wpa_key = i.wpa_key
                                    ,mac_id_cable = i.mac_id_cable
                                    ,mac_id_emta = i.mac_id_emta
                                    ,hardware_version = i.hardware_version
                                    ,firmware_version = i.firmware_version
                                    ,ean_code = i.ean_code
                                    ,software_version = i.software_version
                                    ,srm_password = i.srm_password
                                    ,mac_id_rf = i.mac_id_rf
                                    ,mac_id_mta = i.mac_id_mta
                                    ,mac_id_mta_man_router = i.mac_id_mta_man_router
                                    ,mac_id_mta_data = i.mac_id_mta_data
                                    ,mac_id_ethernet = i.mac_id_ethernet
                                    ,mac_id_usb = i.mac_id_usb
                                    ,primaryssid_passphrase = i.primaryssid_passphrase
                                    ,mac_id_cmci = i.mac_id_cmci
                                    ,mac_id_lan = i.mac_id_lan
                                    ,mac_id_wan = i.mac_id_wan
                                    ,mac_id_device = i.mac_id_device
                                    ,mac_id_wireless = i.mac_id_wireless
                                    ,mac_id_wifi_ssid1 = mac_id_wifi_ssid1
                                    ,ssid1 = i.ssid1
                                    ,ssid1_passphrase = i.ssid1_passphrase
                                    ,wpa_passphrase = i.wpa_passphrase
                                    ,wps_pin_code = i.wps_pin_code
                                    ,pppoa_username = i.pppoa_username
                                    ,pppoa_passphrase = i.pppoa_passphrase
                                    ,tr069_unique_key_64bit = i.tr069_unique_key_64bit
                                    ,fon_key = i.fon_key
                                    ,ta_number = i.ta_number
                                    ,other_macs = v_other_macs
                                    ,
                                     --CREATED_BY = i.created_by,
                                     --CREATION_DATE = i.creation_date,
                                     --LAST_UPDATED_BY = i.last_updated_by,
                                     last_update_date = SYSDATE
                                    ,                                         --added by ppatil 3.10
                                     start_date_active = i.xfactory_date
                                    ,end_date_active = NULL
                                    ,last_transaction_type = '01'
                                    ,warranty_begin_date = NULL
                                    ,warranty_end_date = NULL
                                    ,warranty_period = NULL
                                    ,install_date = NULL
                                    ,installed_at_party_site_id = NULL
                                    ,sold_to_customer_id = v_sold_to_customer_id
                                    ,attribute1 = i.attribute1
                                    ,                                              -- added GP 07-12
                                     attribute2 = i.attribute2
                                    ,                                              -- added GP 07-12
                                     attribute3 = i.attribute3
                                    ,attribute4 = i.attribute4
                                    ,                                                   -- 5g_mac_id
                                     attribute5 = i.attribute5
                                    ,                                                -- acs_password
                                     attribute6 = i.attribute6
                                    ,attribute7 = i.attribute7
                                    ,attribute8 = i.attribute8
                                    ,attribute9 = i.attribute9
                                    ,attribute10 = i.attribute10
                                    ,attribute11 = i.attribute11
                                    ,attribute12 = i.attribute12
                                    ,attribute13 = i.attribute13
                                    ,attribute14 = i.attribute14
                                    ,attribute15 = i.attribute15
                                    ,serial_number_01 = i.serial_number_01
                                    ,serial_number_02 = i.serial_number_02
                                    ,serial_number_03 = i.serial_number_03
                                    ,serial_number_04 = i.serial_number_04
                                    ,serial_number_05 = i.serial_number_05
                                    ,serial_number_06 = i.serial_number_06
                                    ,serial_number_07 = i.serial_number_07
                                    ,serial_number_08 = i.serial_number_08
                                    ,serial_number_09 = i.serial_number_09
                                    ,top_serial_number = i.top_serial_number
                                    ,bt_mac_address = i.bt_mac_address
                                    ,cert_id = i.cert_id
                                    ,battery_sn = i.battery_sn
                                    ,bt_secure_key = i.bt_secure_key
                                    ,apple_home_key = i.apple_home_key
                                    ,lights_auth_code = i.lights_auth_code
                                    ,lights_uuid = i.lights_uuid
                                    ,bundle_flag =
                                       DECODE (l_bundle_item_tbl (   i.asn_number
                                                                  || '#'
                                                                  || dist_items.item_number
                                                                 )
                                              ,'Y', DECODE (i.serial_number_product
                                                           ,i.top_serial_number, 'P'
                                                           ,'C'
                                                           )
                                              ,NULL
                                              )
                                    ,coupon_code = i.coupon_code
                                                                -------------   Added for Change 6.4
                                    ,speaker_box_sn = i.speaker_box_sn       -- Added for change 6.6
                                    ,license_key = i.license_key             -- Added for change 6.6
									,meural_prod_key = i.meural_prod_key     -- Added for change 6.8
									,rfid = i.rfid                           -- Added for change 6.9
                               WHERE instance_id = v_inst_id;

                              IF    i.top_serial_number IS NULL
                                 OR i.serial_number_product = i.top_serial_number
                              THEN
                                --Update XXNTGR_PTM_TRX_REGISTER table
                                UPDATE xxntgr_ptm_trx_register
                                   SET transaction_id = xxntgr_ptm_trx_register_s.NEXTVAL
                                      ,instance_id = i.instance_id
                                      ,item_number = i.item_number
                                      ,inventory_item_id = v_inventory_item_id
                                      ,item_revision = i.item_revision
                                      ,organization_id = v_organization_id
                                      ,subinventory_code = v_subinv_code
                                      ,serial_number = i.serial_number_product
                                      ,transaction_type = '01'
                                      ,transaction_source_type = 'PO'
                                      ,transaction_source_header_id = v_shipment_header_id
                                      ,transaction_source_line_id = v_shipment_line_id
                                      ,last_update_date = SYSDATE             --added by ppatil 3.10
                                 WHERE instance_id = v_inst_id AND transaction_type = v_trans_type;

                                UPDATE xxntgr_ptm_po_shipment_trx
                                   SET instance_id = i.instance_id
                                      ,pallet_id = i.pallet_id
                                      ,master_carton_id = i.master_carton_id
                                      ,item_number = i.item_number
                                      ,inventory_item_id = v_inventory_item_id
                                      ,item_revision = i.item_revision
                                      ,organization_id = v_organization_id
                                      ,serial_number = i.serial_number_product
                                      ,asn_number = i.asn_number
                                      ,invoice_number = i.invoice_number
                                      ,packing_slip_number = i.packing_slip_number
                                      ,container_number = i.container_number
                                      ,po_number = i.po_number
                                      ,po_line_reference = i.po_line_reference
                                      ,xfactory_date = i.xfactory_date
                                      ,manufacturer_name = UPPER (i.manufacturer_name)
                                      ,date_of_manufacture = i.date_of_manufacture
                                      ,country_of_origin = UPPER (i.country_of_origin)
                                      ,ship_to_org_code = v_ship_to_org_code
                                      ,last_update_date = SYSDATE
                                      ,po_header_id = v_po_header_id
                                      ,po_line_id = v_po_line_id
                                      ,po_line_location_id = v_po_line_loc_id
                                      ,shipment_header_id = v_shipment_header_id
                                      ,shipment_line_id = v_shipment_line_id
                                      ,warranty_period = NULL
                                      ,input_file_id = i.input_file_id
                                      ,process_status = i.process_status
                                      ,request_id = v_reqstid
                                      ,attribute1 = i.attribute3
                                 WHERE instance_id = v_inst_id;
                              END IF;

                              xxntgr_debug_utl.LOG (   TO_CHAR (SYSDATE, 'HH:MI:SS')
                                                    || ' Updated data for:'
                                                    || i.serial_number_product
                                                   );
                            END IF;                          --else part of IF process_mode = INSERT

                            /* added by ppatil 3.11 start*/
                            IF     v_to_shipment_org = 'APL'
                               AND (   i.top_serial_number IS NULL
                                    OR i.serial_number_product = i.top_serial_number
                                   )
                            THEN
                              BEGIN
                                SELECT serial_number_product
                                  INTO l_sn_number
                                  FROM xxntgr_product_id_repository
                                 WHERE item_number = i.item_number
                                   AND serial_number_product = i.serial_number_product;

                                IF SQL%FOUND
                                THEN
                                  FOR j IN get_product_reposit_data (i.serial_number_product
                                                                    ,i.item_number
                                                                    )
                                  LOOP
                                    UPDATE xxntgr_product_id_repository
                                       SET batch_id = NVL (i.input_file_id, j.batch_id)
                                          ,interface_id = NVL (i.instance_id, j.interface_id)
                                          ,pallet_id = NVL (i.pallet_id, j.pallet_id)
                                          ,pallet_id_original =
                                                             NVL (i.pallet_id, j.pallet_id_original)
                                          ,master_carton_id =
                                                        NVL (i.master_carton_id, j.master_carton_id)
                                          ,master_carton_id_original =
                                               NVL (i.master_carton_id, j.master_carton_id_original)
                                          ,item_number = NVL (i.item_number, j.item_number)
                                          ,mac_address_product =
                                                  NVL (i.mac_address_product, j.mac_address_product)
                                          ,invoice_number = NVL (i.invoice_number, j.invoice_number)
                                          ,packslip_number =
                                                      NVL (i.packing_slip_number, j.packslip_number)
                                          ,xfactory_date = NVL (i.xfactory_date, j.xfactory_date)
                                          ,manufacture_date =
                                                           NVL (i.xfactory_date, j.manufacture_date)
                                          ,imei_number = NVL (i.imei_number, j.imei_number)
                                          ,master_lock_number =
                                                     NVL (i.masterlock_number, j.master_lock_number)
                                          ,network_lock_number =
                                                   NVL (i.networklock_number, j.network_lock_number)
                                          ,service_lock_number =
                                                   NVL (i.servicelock_number, j.service_lock_number)
                                          ,ta_number_level_rev =
                                                    NVL (i.assembly_revision, j.ta_number_level_rev)
                                          ,item_number_level_rev =
                                                      NVL (i.item_revision, j.item_number_level_rev)
                                          ,wep_key = NVL (i.wep_key, j.wep_key)
                                          ,wifi_id = NVL (i.wifi_id, j.wifi_id)
                                          ,access_code = NVL (i.access_code, j.access_code)
                                          ,ssid = NVL (i.primary_ssid, j.ssid)
                                          ,wpa_key = NVL (i.wpa_key, j.wpa_key)
                                          ,ta_number = NVL (i.assembly_number, j.ta_number)
                                          ,serial_number_pwr_adpt =
                                                        NVL (i.attribute1, j.serial_number_pwr_adpt)
                                          ,mac_address_cable =
                                                           NVL (i.mac_id_cable, j.mac_address_cable)
                                          ,mac_address_emta =
                                                             NVL (i.mac_id_emta, j.mac_address_emta)
                                          ,manufacture_name =
                                                       NVL (i.manufacturer_name, j.manufacture_name)
                                          ,hardware_version =
                                                        NVL (i.hardware_version, j.hardware_version)
                                          ,firmware_version =
                                                        NVL (i.firmware_version, j.firmware_version)
                                          ,ean_code = NVL (i.ean_code, j.ean_code)
                                          ,filename = NVL (i.file_name, j.filename)
                                          ,last_update_date = SYSDATE
                                          ,last_updated_by = 2500
                                          ,attribute1 = NVL (i.attribute1, j.attribute1)
                                     WHERE serial_number_product = i.serial_number_product;
                                  END LOOP;                       -- end of get_product_reposit_data
                                END IF;
                              EXCEPTION
                                WHEN NO_DATA_FOUND
                                THEN
                                  INSERT INTO xxntgr_product_id_repository
                                              (batch_id, interface_id, pallet_id
                                              ,pallet_id_original, master_carton_id
                                              ,master_carton_id_original, item_number
                                              ,serial_number_product, mac_address_product
                                              ,invoice_number, packslip_number
                                              ,xfactory_date, manufacture_date, imei_number
                                              ,master_lock_number, network_lock_number
                                              ,service_lock_number, ta_number_level_rev
                                              ,item_number_level_rev, multi_mac_addresses
                                              ,number_mac_address, wep_key, wifi_id, access_code
                                              ,ssid, wpa_key, warranty_date, ta_number
                                              ,serial_number_pwr_adpt, mac_address_cable
                                              ,mac_address_emta, supplier_number, supplier_name
                                              ,manufacture_name, product_supplier_ref
                                              ,commercial_ref, hardware_version, firmware_version
                                              ,ean_code, type_of_equipment, MEMORY
                                              ,organization_code, sent_to_dc_flag, status_flag
                                              ,status_msg, filename, creation_date, created_by
                                              ,last_update_date, last_updated_by, ssid2
                                              ,acs_password, security_pin_code
                                              ,attribute6, attribute7, attribute8
                                              ,attribute9, attribute10, attribute11
                                              ,attribute12, attribute13, attribute14
                                              ,attribute15
                                              )
                                       VALUES (i.input_file_id, i.instance_id, i.pallet_id
                                              ,i.pallet_id, i.master_carton_id
                                              ,i.master_carton_id, i.item_number
                                              ,i.serial_number_product, v_mac_address
                                              ,i.invoice_number, i.packing_slip_number
                                              ,i.xfactory_date, i.xfactory_date, i.imei_number
                                              ,i.masterlock_number, i.networklock_number
                                              ,i.servicelock_number, i.assembly_revision
                                              ,i.item_revision, v_other_macs
                                              ,NULL, i.wep_key, i.wifi_id, i.access_code
                                              ,i.primary_ssid, i.wpa_key, NULL, i.assembly_number
                                              ,i.attribute1,              -- SERIAL_NUMBER_PWR_ADPT,
                                                            i.mac_id_cable
                                              ,i.mac_id_emta, NULL, NULL
                                              ,NULL, NULL
                                              ,NULL, i.hardware_version, i.firmware_version
                                              ,i.ean_code, NULL, NULL
                                              ,NULL,
                                                    -- disk columns
                                               NULL, NULL
                                              ,NULL, i.file_name, i.creation_date, 2500
                                              ,i.last_update_date, 2500, i.ssid1
                                              ,i.attribute5,                         -- ACS_PASSWORD
                                                            i.primaryssid_passphrase
                                              ,i.attribute6, i.attribute7, i.attribute8
                                              ,i.attribute9, i.attribute10, i.attribute11
                                              ,i.attribute12, i.attribute13, i.attribute14
                                              ,i.attribute15
                                              );
                                WHEN OTHERS
                                THEN
                                  xxntgr_debug_utl.LOG
                                    (   'Error Occured while retrieving Item_Number+Serial_Number:'
                                     || SQLCODE
                                     || SQLERRM
                                    ,'1'
                                    );
                              END;
                            /*added by ppatil 3.11 end */
                            END IF;
                          END LOOP;                                                  -- GET_PTM_DATA
                        ELSE
                          NULL;
                        END IF;                                                 -- proc_status = 'P'
                      END LOOP;    --  FOR dist_items IN get_dist_items(v_asn_number,v_manufacturer)

                      -- GP START
                      -- check imei number against the existing bell mobility ranges
                      -- retrieve all the imei numbers that are not in the ranges
                      --
                      fnd_file.put_line (fnd_file.LOG
                                        ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                          || ' Validate Bell Mobility Ranges'
                                        );

                      FOR dist_items IN get_dist_items_legacy (v_asn_number, v_manufacturer)
                      LOOP
                        -- XXNTGR_DEBUG_UTL.LOG('Legacy - get_dist_items_legacy ' ||v_asn_number,'3');
                        i_cnt := 0;
                        fnd_file.put_line (fnd_file.LOG
                                          ,    'Input_file_id for processing Davinci '
                                            || v_add_input_file_id
                                          );

                        FOR bell_item_rec IN bell_item_cur (v_asn_number
                                                           ,v_manufacturer
                                                           ,dist_items.item_number
                                                           )
                        LOOP
                          -- Loop through imei numbers to find imei numbers that are not in the existing bell mobility ranges
                          BEGIN
                            SELECT 'Y'
                              INTO v_imei_number
                              FROM xxntgr_apps.xxntgr_bellmobility_all_ranges
                             WHERE bell_item_rec.imei_number BETWEEN min_imei_number AND max_imei_number;
                          EXCEPTION
                            WHEN NO_DATA_FOUND
                            THEN
                              i_cnt := i_cnt + 1;
                              imei_num_array (i_cnt) := bell_item_rec.imei_number;
                          END;
                        END LOOP;

-- end Loop through imei numbers to find imei numbers that are not in the existing bell mobility ranges
                        IF i_cnt > 0
                        THEN
                          j_cnt := imei_num_array.FIRST;
                          minv := imei_num_array (imei_num_array.FIRST);
                          maxv := imei_num_array (imei_num_array.LAST);
                        END IF;

                        --XXNTGR_DEBUG_UTL.LOG(' -- imei_num_array max - ' || maxv,'3');
                        --XXNTGR_DEBUG_UTL.LOG('-- imei_num_array min - ' || minv,'3');
                        FOR v_ranges_rec IN v_ranges (minv, maxv)
                        LOOP                     -- Loop through the new ranges to find valid ranges
                          count_no := 0;
                          j_cnt := imei_num_array.FIRST;

                          WHILE j_cnt IS NOT NULL
                          LOOP
                            IF     imei_num_array (j_cnt) >= v_ranges_rec.min_number
                               AND imei_num_array (j_cnt) <= v_ranges_rec.max_number
                            THEN
                              xxntgr_debug_utl.LOG
                                      (   'Atleast one or more imei_numbers exist in this range - '
                                       || v_ranges_rec.min_number
                                       || ' - '
                                       || v_ranges_rec.max_number
                                       || '-'
                                       || imei_num_array (j_cnt)
                                      );
                              count_no := count_no + 1;
                            ELSE
                              --XXNTGR_DEBUG_UTL.LOG('No imei_numbers exist in this range - ' ||'-'|| imei_num_array(j_cnt)|| ','|| v_ranges_rec.min_number || ' - '||v_ranges_rec.max_number);
                              NULL;
                            END IF;

                            j_cnt := imei_num_array.NEXT (j_cnt);
                          END LOOP;

                          IF count_no >= 1
                          THEN
                            -- insert new range/s into the bell mobility range
                            xxntgr_debug_utl.LOG (   'New ranges - 1'
                                                  || v_ranges_rec.min_number
                                                  || ' - '
                                                  || v_ranges_rec.max_number
                                                 );

                            INSERT INTO xxntgr_apps.xxntgr_bellmobility_all_ranges
                                 VALUES (v_add_input_file_id, v_ranges_rec.min_number
                                        ,v_ranges_rec.max_number, SYSDATE, 2500);

                            v_email_ranges :=
                                 v_email_ranges
                              || '<br>['
                              || dist_items.item_number
                              || ']-['
                              || v_ranges_rec.min_number
                              || ']-['
                              || v_ranges_rec.max_number
                              || ']';
                            v_range_insert := 'Y';
                          END IF;
                        END LOOP;            -- End Loop through the new ranges to find valid ranges

                        IF i_cnt > 0 AND v_range_insert <> 'Y'
                        THEN
                          xxntgr_debug_utl.LOG ('Bell Mobility Range - new range - 2');
                          v_email_ranges :=
                               v_email_ranges
                            || '<br>['
                            || dist_items.item_number
                            || ']-['
                            || minv
                            || ']-['
                            || maxv
                            || ']';

                          INSERT INTO xxntgr_apps.xxntgr_bellmobility_all_ranges
                               VALUES (v_add_input_file_id, minv, maxv, SYSDATE, 2500);
                        END IF;
                      END LOOP;                                                    -- Distinct items

                      fnd_file.put_line (fnd_file.LOG
                                        ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                          || ' Validating Bell Mobility Ranges Done'
                                        );
                      -- END inserting bell mobility ranges

                      -- call to handle davinci related table inserts and updates
                      --- Change 6.5 Added If condition IF Profile XXNTGR_PTM_DAVINCI_VALIDATION value is Y then only Validate
                      v_davinci_validation := NULL;

                      SELECT MAX (profile_option_value)
                        INTO v_davinci_validation
                        FROM fnd_profile_option_values
                       WHERE profile_option_id =
                                      (SELECT profile_option_id
                                         FROM fnd_profile_options
                                        WHERE profile_option_name = 'XXNTGR_PTM_DAVINCI_VALIDATION');

                      IF v_davinci_validation = 'Y'
                      THEN
                        IF v_add_input_file_id IS NOT NULL
                        THEN
                          fnd_file.put_line (fnd_file.LOG
                                            ,'Calling davinci function for insert and update'
                                            );
                          -- XXNTGR_DEBUG_UTL.LOG(' +++ Calling Davinci pkg - xxdvnc_util_pkg.update_dvnc_si_tables:' ||v_asn_number,'3');
                          v_return_code :=
                            xxdvnc_util_pkg.update_dvnc_si_tables (v_add_input_file_id
                                                                  ,v_asn_number
                                                                  ,v_manufacturer
                                                                  );          --added by ppatil 3.10

                          IF v_return_code = 'S'
                          THEN
                            --XXNTGR_DEBUG_UTL.LOG('Call to handle Davinci related table inserts was successful - ASN Number -'||v_asn_number||' - batch_id - '||v_add_input_file_id,'1');
                            fnd_file.put_line
                              (fnd_file.LOG
                              ,    'Call to handle Davinci related table inserts and updates was successful - ASN Number -'
                                || v_asn_number
                                || ' - batch_id - '
                                || v_add_input_file_id
                              );
                            NULL;
                          END IF;

                          IF v_return_code = 'E'
                          THEN
                            v_err_location := v_err_location || 'K';                   -- mmore 3.4
                            v_errmsg := 'Error while calling davinci related inserts and updates. ';
                            xxntgr_debug_utl.LOG (   'ASN:'
                                                  || v_asn_number
                                                  || '-'
                                                  || 'BATCH ID:'
                                                  || v_add_input_file_id
                                                  || '-'
                                                  || v_errmsg
                                                 ,'1'
                                                 );
                            RAISE davinci_err;                                 -- raise an exception
                          END IF;
                        END IF;
                      END IF;
                    -- END IF; -- IF process_mode = INSERT  --commented by ppatil 3.10
                        -- GP END
                    EXCEPTION
                      WHEN davinci_err
                      THEN
                        ROLLBACK;                                               -- Added on July 18
                        v_err_location := v_err_location || 'L';                       -- mmore 3.4
                        v_errmsg := 'Error while calling davinci related inserts and updates. ';
                        xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');
                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 41');

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer);

                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 41');
                        --AND nvl(process_status,'NULL') NOT IN ('P','S');
                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 42');
                        --Insert error data into archive table
                        fnd_file.put_line (fnd_file.LOG
                                          ,    TO_CHAR (SYSDATE, 'HH:MI:SS')
                                            || 'Inserting Into ARCH TABLE '
                                          );

                        INSERT INTO xxntgr_ptm_odm_shipment_arch
                          SELECT *
                            FROM xxntgr_ptm_odm_shipment_stg
                           WHERE asn_number = v_asn_num
                             AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                             AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 42');
                        --Delete error data from staging table
                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 43');

                        DELETE FROM xxntgr_ptm_odm_shipment_stg
                              WHERE process_status = 'E'
                                AND asn_number = v_asn_num
                                AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                                AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 43');
                        retcode := 1;
                      WHEN OTHERS
                      THEN
                        ROLLBACK;
                        xxntgr_debug_utl.LOG ('After Seting v_process_mode 44');
                        v_err_location := v_err_location || 'M';                       -- mmore 3.4
                        v_errmsg :=
                             'Error while inserting data into PTM tables. Process Mode: '
                          || v_process_mode
                          || ' Error: '
                          || SQLERRM;
                        xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');

                        UPDATE xxntgr_ptm_odm_shipment_stg
                           SET process_status = 'E'
                              ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                         WHERE asn_number = v_asn_number
                           AND UPPER (manufacturer_name) = UPPER (v_manufacturer);

                        --AND nvl(process_status,'NULL') NOT IN ('P','S');

                        --Insert error data into archive table
                        INSERT INTO xxntgr_ptm_odm_shipment_arch
                          SELECT *
                            FROM xxntgr_ptm_odm_shipment_stg
                           WHERE asn_number = v_asn_num
                             AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                             AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                        --Delete error data from staging table
                        DELETE FROM xxntgr_ptm_odm_shipment_stg
                              WHERE process_status = 'E'
                                AND asn_number = v_asn_num
                                AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                                AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                        retcode := 1;
                    END;                                                                        -- 5

                    -- GP START
                    -- Send mail to Bell with new ranges
                    IF     v_process_mode = 'INSERT'
                       AND v_email_ranges IS NOT NULL
                       AND v_return_code <> 'E'
                    THEN
                      xxntgr_debug_utl.LOG (' 7 -- Davinci Validation bell email- ' || v_asn_number
                                           ,'3'
                                           );
                      xxntgr_bell_email (v_email_ranges, v_bell_err_msg);

                      IF v_bell_err_msg IS NOT NULL
                      THEN
                        xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_bell_err_msg, '1');
                        RAISE bell_email_err;                                 -- raise an exception
                      END IF;
                    END IF;

                    v_email_ranges := NULL;
                  -- GP END
                  END IF;                                     -- else part of IF v_proc_status = 'E'
                EXCEPTION
                  WHEN bell_email_err
                  THEN
                    v_err_location := v_err_location || 'N';                           -- mmore 3.4
                    v_errmsg :=
                      'Error while sending Bell email with imei ranges. ' || SQLCODE || '-'
                      || SQLERRM;

                    UPDATE xxntgr_ptm_odm_shipment_stg
                       SET process_status = 'E'
                          ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                     WHERE asn_number = v_asn_number
                       AND UPPER (manufacturer_name) = UPPER (v_manufacturer);

                    xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');

                    --Insert error data into archive table
                    INSERT INTO xxntgr_ptm_odm_shipment_arch
                      SELECT *
                        FROM xxntgr_ptm_odm_shipment_stg
                       WHERE asn_number = v_asn_num
                         AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                    --Delete error data from staging table
                    DELETE FROM xxntgr_ptm_odm_shipment_stg
                          WHERE process_status = 'E'
                            AND asn_number = v_asn_num
                            AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                            AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                    retcode := 1;
                  WHEN OTHERS
                  THEN
                    v_err_location := v_err_location || 'O';                           -- mmore 3.4
                    v_errmsg := 'Error in the main program. ' || SQLCODE || '-' || SQLERRM;

                    UPDATE xxntgr_ptm_odm_shipment_stg
                       SET process_status = 'E'
                          ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                     WHERE asn_number = v_asn_number
                       AND UPPER (manufacturer_name) = UPPER (v_manufacturer);

                    xxntgr_debug_utl.LOG ('ASN:' || v_asn_number || '-' || v_errmsg, '1');

                    --Insert error data into archive table
                    INSERT INTO xxntgr_ptm_odm_shipment_arch
                      SELECT *
                        FROM xxntgr_ptm_odm_shipment_stg
                       WHERE asn_number = v_asn_num
                         AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                         AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                    --Delete error data from staging table
                    DELETE FROM xxntgr_ptm_odm_shipment_stg
                          WHERE process_status = 'E'
                            AND asn_number = v_asn_num
                            AND UPPER (manufacturer_name) = UPPER (v_manufacturer)
                            AND NVL (process_status, 'NULL') NOT IN ('P', 'S');

                    retcode := 1;
                END;                                                                             --3

                COMMIT;
                /*====Record processing ends here===*/

                -- GP added 07-JUL-2014
                xxntgr_ptm_common_pkg.xxntgr_insert_odm_file_header (v_add_input_file_id);
              END IF;                                                 -- IF v_asn_number is not null
            END;
          ELSE
            v_asn_number := NULL;
            --6.3 Added code to reset the status to U if the ASN is not present in the system
            fnd_file.put_line (fnd_file.LOG
                              , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Reset Staging table Status'
                              );

            UPDATE xxntgr_apps.xxntgr_ptm_odm_shipment_stg
               SET process_status = 'U'
             WHERE input_file_id = v_add_input_file_id
               AND asn_number = v_asn_num
               AND process_status = 'V';
          -- End 6.3
          END IF;                                                           --  IF v_asn IS NOT NULL

          -- IF manufacturer is Invalid insert header record
          IF v_invalid_mfg IS NOT NULL
          THEN
            xxntgr_ptm_common_pkg.xxntgr_insert_odm_file_header (v_add_input_file_id);
            v_errmsg := v_errmsg || ' ' || v_invalid_mfg;                   -- Added for 4.3 change
          END IF;

          COMMIT;
          v_to_shipment_org := NULL;
          -- mmore 3.4 start
          xxntgr_debug_utl.LOG ('V_ASN_NUM: ' || v_asn_num || ' V_ERRMSG : ' || v_errmsg);

          IF v_errmsg IS NOT NULL
          THEN
            fnd_file.put_line (fnd_file.LOG
                              , TO_CHAR (SYSDATE, 'HH:MI:SS') || ' Error Message-' || v_errmsg
                              );

            BEGIN
              log_error_email
                ('XXNTGR_PTM_ODM_SHIP'
                ,'PTM ODM Shipment File Import ORAERROR'
                ,    'Hi Team,\n\n\tThere is an error in program PTM ODM Shipment File Import.\n\tConcurrent Request ID: '
                  || v_reqstid
                ,    '\n\tASN#: '
                  || v_asn_num
                  || ' Input File ID: '
                  || v_add_input_file_id
                  || ' Error locations: '
                  || v_err_location
                  || '\n\tError: '
                  || v_errmsg
                  || '\n\nRegards\nNETGEAR Support'
                );
              v_errmsg := NULL;
              v_err_location := NULL;
            EXCEPTION
              WHEN OTHERS
              THEN
                v_errmsg := NULL;
                v_err_location := NULL;
                xxntgr_debug_utl.LOG (   'WO04 Error in executing log_error_email. SQLERRM => '
                                      || SQLERRM
                                     ,'1'
                                     );
            END;
          ELSE
            --mmore 3.4 end
               -- Add logic to send successful notification emails to ODMs  -- GP 13-MAY-14
            BEGIN
              SELECT 'SUCCESS'
                INTO v_reason
                FROM xxntgr_ptm_po_shipment_trx
               WHERE asn_number = v_asn_num AND ROWNUM = 1;

              fnd_file.put_line (fnd_file.LOG, 'Data Successfully Inserted into PTM Tables');
              fnd_file.put_line (fnd_file.LOG, 'Sending Successful email notification ');
              xxntgr_ptm_odm_pkg.ptm_odm_success_mail_notif (v_asn_num
                                                            ,UPPER (v_manufacturer)
                                                            ,v_file_name
                                                            ,v_reason
                                                            ,v_get_mail_error
                                                            );
              xxntgr_debug_utl.LOG (v_get_mail_error, '1');
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                NULL;
              --XXNTGR_DEBUG_UTL.LOG(' Sending Successful email notification - ASN not found in xxntgr_ptm_po_shipment_trx ' ||v_asn_num,'3');
              WHEN OTHERS
              THEN
                --V_ERRMSG := 'Error while Sending PTM ODM Successful email notification . '||SQLCODE||'-'||SQLERRM; -- mmore 3.4
                  -- mmore start 3.4
                BEGIN
                  log_error_email
                    ('XXNTGR_PTM_ODM_SHIP'
                    ,'PTM ODM Shipment File Import ORAERROR'
                    ,    'Hi Team,\n\n\tThere is an error in program PTM ODM Shipment File Import.\n\tConcurrent Request ID: '
                      || v_reqstid
                    ,    '\n\tASN#: '
                      || v_asn_num
                      || '\n\tError while Sending PTM ODM Successful email notification. SQLERRM: '
                      || SQLERRM
                      || '\n\nRegards\nNETGEAR Support'
                    );
                EXCEPTION
                  WHEN OTHERS
                  THEN
                    xxntgr_debug_utl.LOG
                                        (   'WO05 Error in executing log_error_email. SQLERRM => '
                                         || SQLERRM
                                        ,'1'
                                        );
                END;
            -- mmore end 3.4
            END;
          END IF;                                                                       -- mmore 3.4
        END LOOP;

        CLOSE get_asn_number;
      END;
    END IF;                                                    -- else part of IF P_DELETE_ASN = 'Y'

    fnd_file.put_line (fnd_file.LOG, 'PTM ODM End');
  END xxntgr_ptm_odm_ship;

-- GP START
--------------------------------------------------------------------
-- PROCEDURE
-- NAME:CHECK_SERIAL_NUMBER
--------------------------------------------------------------------
  PROCEDURE check_serial_number (
    p_serial_number             IN       VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    v_sn                          VARCHAR2 (30);
    v_serial_number               VARCHAR2 (100);
  BEGIN
    SELECT serialnumber
      INTO v_serial_number
      FROM imec.serial_master
     WHERE serialnumber = p_serial_number;

    IF v_serial_number IS NOT NULL
    THEN
      v_err_msg := serialnumber_in_master;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      BEGIN
        SELECT sn
          INTO v_sn
          FROM imec.sn_label
         WHERE sn = p_serial_number;

        IF v_sn IS NOT NULL
        THEN
          v_err_msg := serialnumber_in_snlabel;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
          v_err_msg := NULL;
      END;
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_SERIAL_NUMBER PROC: ' || SQLCODE
                              || SQLERRM
                             );
      v_err_msg := 'Error while executing CHECK_SERIAL_NUMBER PROC: ' || SQLCODE || SQLERRM;
  END check_serial_number;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME:CHECK_REVISION
--------------------------------------------------------------------
  PROCEDURE check_revision (
    p_item_number               IN       VARCHAR2
   ,p_item_revision             IN       VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    v_rev                         VARCHAR2 (30);
    v_nom                         VARCHAR2 (30);
  BEGIN
    IF p_item_attributes_rec.rev = p_item_revision
    THEN
      v_err_msg := NULL;
      RETURN;
    END IF;

    SELECT rev, nom
      INTO v_rev, v_nom
      FROM imec.serial_control_rev
     WHERE material = p_item_number AND rev = p_item_revision;

    --IF p_item_attributes_rec.rev <> v_rev THEN
    IF p_item_revision <> v_rev
    THEN
      v_err_msg := rev_not_found;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      v_err_msg := rev_not_found;
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_REVISION PROC: ' || SQLCODE || SQLERRM);
      v_err_msg := 'Error while executing CHECK_REVISION PROC: ' || SQLCODE || SQLERRM;
  END check_revision;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME:CHECK_ACCESS_CODE
--------------------------------------------------------------------
  PROCEDURE check_access_code (
    p_access_code               IN       VARCHAR2
   ,p_ta_number                 IN       VARCHAR2
   ,p_serial_number             IN       VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    found_ac                      VARCHAR2 (30);
    found_sn                      VARCHAR2 (30);
  BEGIN
    IF NVL (p_item_attributes_rec.ac_length, 0) = 0
    THEN
      IF p_access_code IS NULL
      THEN
        v_err_msg := '';
        RETURN;
      END IF;

      IF p_access_code IS NOT NULL
      THEN
        v_err_msg := ac_found_not_expected;
        RETURN;
      END IF;
    ELSE
      IF p_access_code IS NULL
      THEN
        v_err_msg := ac_not_found_expected;
        RETURN;
      END IF;
    END IF;

    SELECT accesscode, sn
      INTO found_ac, found_sn
      FROM imec.assigned_access_codes
     WHERE accesscode = p_access_code AND model = p_ta_number;

    IF found_sn IS NOT NULL
    THEN
      IF found_sn <> p_serial_number
      THEN
        v_err_msg := ac_assigned_to_another || '-' || found_sn;
        RETURN;
      END IF;
    END IF;

    v_err_msg := NULL;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      v_err_msg := ac_not_found;
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_ACCESS_CODE PROC: ' || SQLCODE || SQLERRM
                             );
      v_err_msg := 'Error while executing CHECK_ACCESS_CODE PROC: ' || SQLCODE || SQLERRM;
  END check_access_code;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME: CHECK_MAC
--------------------------------------------------------------------
  PROCEDURE check_mac (
    p_serial_number             IN       VARCHAR2
   ,p_mac_address_product       IN       VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    v_foundmaccount               NUMBER;
    v_mac_add_l                   VARCHAR2 (10);
    v_mac_address_l               VARCHAR2 (250);
    v_other_macs_l                VARCHAR2 (250);
    v_nos_mac                     NUMBER;
    v_hex                         VARCHAR2 (250);
    v_mac_string                  VARCHAR2 (250);
    v_mac_address                 VARCHAR2 (250);
    v_sql                         VARCHAR2 (2000);

    TYPE t_array IS TABLE OF VARCHAR2 (50)
      INDEX BY BINARY_INTEGER;

    strings                       t_array;
    i                             NUMBER := 0;
    pos                           NUMBER := 0;
    p_delim                       VARCHAR2 (1) := '|';
    prefix                        VARCHAR2 (50);
    suffix                        VARCHAR2 (100);
    prevprefix                    VARCHAR2 (50);
    previousvalue                 NUMBER := 0;
    macvalue                      NUMBER;
  BEGIN
    v_mac_string := p_mac_address_product;

    BEGIN
      SELECT INSTR (p_mac_address_product, '|')
        INTO v_mac_add_l
        FROM DUAL;

      SELECT NVL (LENGTH (REGEXP_REPLACE (p_mac_address_product, '[^|]')) / LENGTH ('|'), 0) cnt
        INTO v_nos_mac
        FROM DUAL;

      IF v_mac_add_l = 0
      THEN
        v_mac_address_l := p_mac_address_product;
        v_other_macs_l := NULL;
      ELSE
        v_mac_address_l :=
                         SUBSTR (p_mac_address_product, 1
                                , (INSTR (p_mac_address_product, '|') - 1));
        v_other_macs_l := SUBSTR (p_mac_address_product, (INSTR (p_mac_address_product, '|') + 1));
      END IF;
    END;

    IF p_item_attributes_rec.nom = 0
    THEN
      IF v_mac_address_l IS NOT NULL
      THEN
        v_err_msg := mac_found_when_not_expected;
        RETURN;
      ELSE
        v_err_msg := NULL;
        RETURN;
      END IF;
    ELSE
      IF v_mac_address_l IS NULL
      THEN
        v_err_msg := mac_not_found_expected;
        RETURN;
      END IF;
    END IF;

    IF (v_nos_mac + 1) <> p_item_attributes_rec.nom
    THEN
      v_err_msg := mac_array_incorrect_no_of_macs;
      RETURN;
    END IF;

    BEGIN
      IF v_mac_add_l = 0
      THEN
        SELECT mac_address_product
          INTO v_hex
          FROM xxntgr_ptm_odm_shipment_stg a
         WHERE mac_address_product = v_mac_address_l
           AND REGEXP_LIKE (UPPER (SUBSTR (v_mac_address_l, 7)), '[^0-9 A-F]');

        IF v_hex IS NOT NULL
        THEN
          v_err_msg := mac_is_not_hex;
          RETURN;
        END IF;
      END IF;

      -- Iterate through mac address if there are multiple macids
      pos := INSTR (v_mac_string, p_delim, 1, 1);                 -- determine first chunk of string

      WHILE (pos != 0)
      LOOP                                                      -- while there are chunks left, loop
        --XXNTGR_DEBUG_UTL.LOG('Inside Check Mac 8 - inside pos ='||pos);
        i := i + 1;                                                            -- increment counter
        strings (i) := SUBSTR (v_mac_string, 1, pos - 1);
        -- create array element for chuck of string
        v_mac_string := SUBSTR (v_mac_string, pos + 1, LENGTH (v_mac_string));
        -- remove chunk from string
        pos := INSTR (v_mac_string, p_delim, 1, 1);                         -- determine next chunk

        IF pos = 0
        THEN                                                         -- no last chunk, add to array
          strings (i + 1) := v_mac_string;
        END IF;
      END LOOP;

      FOR i IN 1 .. strings.COUNT
      LOOP
        IF LENGTH (strings (i)) <> 12
        THEN
          v_err_msg := mac_invalid_length;
          RETURN;
        END IF;

        prefix := SUBSTR (strings (i), 0, 6);
        suffix := SUBSTR (strings (i), 7);

        SELECT MAX ('Y') AS v_hex
          INTO v_hex
          FROM DUAL
         WHERE REGEXP_LIKE (UPPER (suffix), '[^0-9 A-F]');

        IF v_hex IS NOT NULL
        THEN
          v_err_msg := mac_is_not_hex;
          RETURN;
        END IF;

        SELECT TO_NUMBER (suffix, 'XXXXXX')
          INTO macvalue
          FROM DUAL;

        IF (i > 1)
        THEN
          IF (prevprefix <> prefix)
          THEN
            v_err_msg := mac_prefix_not_equal;
            RETURN;
            xxntgr_debug_utl.LOG ('v_err_msg - ' || v_err_msg);
          END IF;
        -- GP 06-aug-13 no need to check sequence as we are sorting the macs and passing to product rep table
        /*IF (macvalue != previousValue + 1) THEN
            v_err_msg := MAC_NOT_SEQUENTIAL;
            RETURN;
        END IF; */
        END IF;

        prevprefix := prefix;
        previousvalue := macvalue;
      END LOOP;

      -- END  Iterate through mac address if there are multiple macids
      v_mac_address := REPLACE (p_mac_address_product, '|', ''',''');
      v_sql :=
           'SELECT  COUNT(*) as MACCOUNT FROM IMEC.ASSIGNED_MACS WHERE MAC IN ('''
        || v_mac_address
        || ''') AND (SN IS NULL OR  SN ='''
        || p_serial_number
        || ''')';

      /*SELECT  COUNT(MAC) as MACCOUNT
      INTO v_foundMacCount
      FROM IMEC.ASSIGNED_MACS
      WHERE MAC IN (REPLACE(p_mac_address_product,'|',',') )
      AND (SN IS NULL OR  SN =  p_serial_number);*/
      EXECUTE IMMEDIATE v_sql
                   INTO v_foundmaccount;

      IF v_foundmaccount != p_item_attributes_rec.nom
      THEN
        v_err_msg := assigned_mac_table_invalid;
        RETURN;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        NULL;
    END;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      NULL;
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_MAC PROC: ' || SQLCODE || SQLERRM);
      v_err_msg := 'Error while executing CHECK_MAC PROC: ' || SQLCODE || SQLERRM;
  END check_mac;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME: CHECK_SSID
--------------------------------------------------------------------
  PROCEDURE check_ssid (
    p_serial_number             IN       VARCHAR2
   ,p_mac_address_product       IN       VARCHAR2
   ,p_primary_ssid              IN       VARCHAR2
   ,p_manufacturer              IN       VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    v_sn_ssid                     VARCHAR2 (30);
    v_key_char_set                VARCHAR2 (250);
    vowels                        VARCHAR2 (15) := 'AEIOUXaeioux';
    foundvowel                    BOOLEAN := FALSE;
    v_ssidcharset                 VARCHAR2 (250) := '';
    ch                            VARCHAR2 (1);
    msg                           VARCHAR2 (500)
      := 'This unit has been flagged for invalid SSID. Please put it aside and contact your Netgear representative for further processing instructions. ';
    v_fixed_text                  VARCHAR2 (25);
    v_ssid_prefix                 VARCHAR2 (25);
    v_ssid_from_mac               VARCHAR2 (25);
    v_mac_add_l                   VARCHAR2 (10);
    v_mac_address_l               VARCHAR2 (250);
    v_nos_mac                     NUMBER;

    CURSOR banned_words_cur
    IS
      SELECT word
        FROM imec.banned_words;
  BEGIN
    IF p_item_attributes_rec.ssid_type = 'N'
    THEN
      IF p_primary_ssid IS NULL
      THEN
        v_err_msg := NULL;
        RETURN;
      ELSE
        v_err_msg := ssid_found_not_expected;
        RETURN;
      END IF;
    END IF;

    IF p_primary_ssid IS NULL
    THEN
      v_err_msg := ssid_not_found_expected;
      RETURN;
    END IF;

    --type P ssids are just made from serial number
    IF p_item_attributes_rec.ssid_type = 'P'
    THEN
      IF     (LENGTH (p_serial_number) >= p_item_attributes_rec.ssid_start + 1)
         AND (LENGTH (p_serial_number) >=
                                p_item_attributes_rec.ssid_start + p_item_attributes_rec.ssid_length
             )
      THEN
        v_sn_ssid :=
          SUBSTR (p_serial_number
                 , p_item_attributes_rec.ssid_start + 1
                 ,p_item_attributes_rec.ssid_length
                 );
        v_sn_ssid :=
                 p_item_attributes_rec.ssid_prefix || v_sn_ssid || p_item_attributes_rec.ssid_suffix;
      ELSE
        v_sn_ssid := '-1';
      END IF;

      --XXNTGR_DEBUG_UTL.LOG('Inside Check SSID type P - ' || p_primary_ssid,3);
      --XXNTGR_DEBUG_UTL.LOG('Inside Check SSID type P - v_sn_ssid = ' || v_sn_ssid,3);
      IF v_sn_ssid = '-1'
      THEN
        v_err_msg := ssid_build_type_pssid_failure;
        RETURN;
      ELSE
        IF v_sn_ssid <> p_primary_ssid
        THEN
          v_err_msg := ssid_ptype_mismatch;
          RETURN;
        END IF;
      END IF;
    --type F - Fixed text
    ELSIF p_item_attributes_rec.ssid_type = 'F'
    THEN
      v_fixed_text := p_item_attributes_rec.ssid_fixed_text;

      IF v_fixed_text IS NOT NULL
      THEN
        v_ssid_prefix := SUBSTR (p_primary_ssid, 1, LENGTH (p_item_attributes_rec.ssid_fixed_text));

        -- Add logic here substr(p_primary_ssid,i,1);
        IF v_fixed_text <> v_ssid_prefix
        THEN
          v_err_msg := ssid_invalid || '-' || p_primary_ssid;
          RETURN;
        END IF;
      END IF;
    ELSIF p_item_attributes_rec.ssid_type = 'M'
    THEN
      IF p_item_attributes_rec.ssid_5g_flag IS NULL
      THEN
        --XXNTGR_DEBUG_UTL.LOG('-- Inside Check SSID type ''M'' - ' || p_primary_ssid,'3');
        -- Take the last 6 characters of the primary MAC address
        -- extract last 6 chars of first mac from if there are multimacs
        SELECT INSTR (p_mac_address_product, '|')
          INTO v_mac_add_l
          FROM DUAL;

        SELECT NVL (LENGTH (REGEXP_REPLACE (p_mac_address_product, '[^|]')) / LENGTH ('|'), 0) cnt
          INTO v_nos_mac
          FROM DUAL;

        IF v_mac_add_l = 0
        THEN
          v_mac_address_l := p_mac_address_product;
        ELSE
          v_mac_address_l :=
                         SUBSTR (p_mac_address_product, 1
                                , (INSTR (p_mac_address_product, '|') - 1));
        END IF;

        v_ssid_from_mac := SUBSTR (v_mac_address_l, -6);

        IF v_ssid_from_mac <> p_primary_ssid
        THEN
          v_err_msg := ssid_invalid || '-' || p_primary_ssid;
          RETURN;
        END IF;
      END IF;
    ELSE                                                      -- when ssid type is not 'P' or F or M
      IF v_ssidcharset IS NULL
      THEN
        SELECT alpha || NUMERIC || symbol
          INTO v_ssidcharset
          FROM imec.key_charsets
         WHERE key_type = 'SSID' AND sub_type = 'LIMITED';
      END IF;

      FOR i IN 1 .. LENGTH (p_primary_ssid)
      LOOP
        ch := SUBSTR (p_primary_ssid, i, 1);

        IF INSTR (v_ssidcharset, ch) = 0
        THEN
          v_err_msg := ssid_invalid_char;
          RETURN;
        END IF;

        IF NOT foundvowel
        THEN
          IF INSTR (vowels, ch) != 0
          THEN
            foundvowel := TRUE;
          END IF;
        END IF;
      END LOOP;

      IF (foundvowel)
      THEN
        -- CheckBannedWords(ssid))  --InsertBlockedSerialNumber(serialnumber, ssid);
        FOR banned_words_rec IN banned_words_cur
        LOOP
          IF INSTR (p_primary_ssid, banned_words_rec.word) != 0
          THEN
            INSERT INTO imec.blocked_sn
                 VALUES (p_serial_number, msg || banned_words_rec.word);

            v_err_msg := ssid_banned_chars;
            RETURN;
          END IF;
        END LOOP;
      END IF;

      SELECT sn
        INTO v_sn_ssid
        FROM imec.assigned_ssids
       WHERE mfg =
               DECODE (UPPER (p_manufacturer)
                      ,'ASKEY', 'AK'
                      ,'FOXCONN', 'FX'
                      ,'WESTELL', 'B4'
                      ,p_manufacturer
                      )
         AND ssid = p_primary_ssid
         AND ROWNUM = 1;

      IF v_sn_ssid IS NOT NULL AND v_sn_ssid <> p_serial_number
      THEN
        v_err_msg := ssid_assigned_to_serialnumber || '-' || v_sn_ssid;
        RETURN;
      END IF;
    END IF;

    v_err_msg := NULL;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      NULL;
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_SSID PROC: ' || SQLCODE || SQLERRM);
      v_err_msg := 'Error while executing CHECK_SSID PROC: ' || SQLCODE || SQLERRM;
  END check_ssid;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME: CHECK_SSID1
--------------------------------------------------------------------
  PROCEDURE check_ssid1 (
    p_serial_number             IN       VARCHAR2
   ,p_mac_address_product       IN       VARCHAR2
   ,p_ssid1                     IN       VARCHAR2
   ,p_manufacturer              IN       VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    v_sn_ssid1                    VARCHAR2 (30);
    v_key_char_set                VARCHAR2 (250);
    vowels                        VARCHAR2 (15) := 'AEIOUXaeioux';
    foundvowel                    BOOLEAN := FALSE;
    v_ssidcharset                 VARCHAR2 (250) := '';
    ch                            VARCHAR2 (1);
    msg                           VARCHAR2 (500)
      := 'This unit has been flagged for invalid SSID1. Please put it aside and contact your Netgear representative for further processing instructions. ';
    v_fixed_text                  VARCHAR2 (25);
    v_ssid1_suffix                VARCHAR2 (25);
    v_ssid1_prefix                VARCHAR2 (25);
    v_ssid1_from_mac              VARCHAR2 (25);
    v_mac_add_l                   VARCHAR2 (10);
    v_mac_address_l               VARCHAR2 (250);
    v_nos_mac                     NUMBER;

    CURSOR banned_words_cur
    IS
      SELECT word
        FROM imec.banned_words;
  BEGIN
    --XXNTGR_DEBUG_UTL.LOG('Inside Check SSID1 - ' || p_ssid1,3);
    IF p_item_attributes_rec.ssid_type = 'N'
    THEN
      IF p_ssid1 IS NULL
      THEN
        v_err_msg := NULL;
        RETURN;
      ELSE
        v_err_msg := ssid1_found_not_expected;
        RETURN;
      END IF;
    END IF;

    IF p_ssid1 IS NULL
    THEN
      v_err_msg := ssid1_not_found_expected;
      RETURN;
    END IF;

    --type P ssids are just made from serial number
    IF p_item_attributes_rec.ssid_type = 'P'
    THEN
      IF     (LENGTH (p_serial_number) >= p_item_attributes_rec.ssid_start + 1)
         AND (LENGTH (p_serial_number) >=
                                p_item_attributes_rec.ssid_start + p_item_attributes_rec.ssid_length
             )
      THEN
        v_sn_ssid1 :=
          SUBSTR (p_serial_number
                 ,p_item_attributes_rec.ssid_start
                 ,p_item_attributes_rec.ssid_length
                 );
        v_sn_ssid1 :=
                p_item_attributes_rec.ssid_prefix || v_sn_ssid1 || p_item_attributes_rec.ssid_suffix;
      ELSE
        v_sn_ssid1 := '-1';
      END IF;

      --XXNTGR_DEBUG_UTL.LOG('Inside Check SSID1 type P - ' || p_ssid1);
      --XXNTGR_DEBUG_UTL.LOG('Inside Check SSID1 type P - v_sn_ssid = ' || v_sn_ssid1);
      IF v_sn_ssid1 = '-1'
      THEN
        v_err_msg := ssid1_build_type_pssid_failure;
        RETURN;
      ELSE
        IF v_sn_ssid1 <> p_ssid1
        THEN
          v_err_msg := ssid1_ptype_mismatch;
          RETURN;
        END IF;
      END IF;
    --type F - Fixed text
    ELSIF p_item_attributes_rec.ssid_type = 'F'
    THEN
      v_fixed_text := p_item_attributes_rec.ssid_fixed_text;

      IF p_item_attributes_rec.ssid_5g_flag = 'Y' AND v_fixed_text IS NOT NULL
      THEN
        v_ssid1_suffix := SUBSTR (p_ssid1, -2);
        v_ssid1_prefix := SUBSTR (p_ssid1, 1, LENGTH (p_item_attributes_rec.ssid_fixed_text));

        IF v_fixed_text <> v_ssid1_prefix OR v_ssid1_suffix <> '5G'
        THEN
          v_err_msg := ssid1_invalid || '-' || p_ssid1;
          RETURN;
        END IF;
      END IF;
    ELSIF p_item_attributes_rec.ssid_type = 'M'
    THEN
      IF p_item_attributes_rec.ssid_5g_flag = 'Y'
      THEN
        xxntgr_debug_utl.LOG ('-- Inside Check SSID1 type ''M'' - ' || p_ssid1);

        --Take the last 6 characters of the primary MAC address
        -- extract last 6 chars of first mac if there are multimacs
        SELECT INSTR (p_mac_address_product, '|')
          INTO v_mac_add_l
          FROM DUAL;

        SELECT NVL (LENGTH (REGEXP_REPLACE (p_mac_address_product, '[^|]')) / LENGTH ('|'), 0) cnt
          INTO v_nos_mac
          FROM DUAL;

        IF v_mac_add_l = 0
        THEN
          v_mac_address_l := p_mac_address_product;
        ELSE
          v_mac_address_l :=
                         SUBSTR (p_mac_address_product, 1
                                , (INSTR (p_mac_address_product, '|') - 1));
        END IF;

        v_ssid1_from_mac := SUBSTR (v_mac_address_l, -6);

        IF LENGTH (v_ssid1_from_mac) = 6
        THEN
          v_ssid1_from_mac := v_ssid1_from_mac || ' 5G';
        END IF;

        IF v_ssid1_from_mac <> p_ssid1
        THEN
          v_err_msg := ssid1_invalid || '-' || p_ssid1;
          RETURN;
        END IF;
      END IF;
    ELSE                                                      -- when ssid type is not 'P' or F or M
      IF v_ssidcharset IS NULL
      THEN
        SELECT alpha || NUMERIC || symbol
          INTO v_ssidcharset
          FROM imec.key_charsets
         WHERE key_type = 'SSID' AND sub_type = 'LIMITED';
      END IF;

      FOR i IN 1 .. LENGTH (p_ssid1)
      LOOP
        ch := SUBSTR (p_ssid1, i, 1);

        IF INSTR (v_ssidcharset, ch) = 0
        THEN
          v_err_msg := ssid1_invalid_char;
          RETURN;
        END IF;

        IF NOT foundvowel
        THEN
          IF INSTR (vowels, ch) != 0
          THEN
            foundvowel := TRUE;
          END IF;
        END IF;
      END LOOP;

      IF (foundvowel)
      THEN
        -- CheckBannedWords(ssid1))  --InsertBlockedSerialNumber(serialnumber, ssid);
        FOR banned_words_rec IN banned_words_cur
        LOOP
          IF INSTR (p_ssid1, banned_words_rec.word) != 0
          THEN
            INSERT INTO imec.blocked_sn
                 VALUES (p_serial_number, msg || banned_words_rec.word);

            v_err_msg := ssid1_banned_chars;
            RETURN;
          END IF;
        END LOOP;
      END IF;

      --XXNTGR_DEBUG_UTL.LOG('Inside Check SSID1 before assigned_ssids -');
      SELECT sn
        INTO v_sn_ssid1
        FROM imec.assigned_ssids
       WHERE mfg =
               DECODE (UPPER (p_manufacturer)
                      ,'ASKEY', 'AK'
                      ,'FOXCONN', 'FX'
                      ,'WESTELL', 'B4'
                      ,p_manufacturer
                      )
         AND ssid = p_ssid1
         AND ROWNUM = 1;

      IF v_sn_ssid1 IS NOT NULL AND v_sn_ssid1 <> p_serial_number
      THEN
        v_err_msg := ssid1_assigned_to_serialnumber || '-' || v_sn_ssid1;
        RETURN;
      END IF;
    END IF;

    v_err_msg := NULL;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      NULL;
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_SSID1 PROC: ' || SQLCODE || SQLERRM);
      v_err_msg := 'Error while executing CHECK_SSID1 PROC: ' || SQLCODE || SQLERRM;
  END check_ssid1;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME: CHECK_WEP
--------------------------------------------------------------------
  PROCEDURE check_wep (
    p_wep_key                   IN       VARCHAR2
   ,p_serial_number                      VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    weplen                        NUMBER;
    wepabs                        NUMBER;
    v_hex                         VARCHAR2 (50);
  BEGIN
    IF p_item_attributes_rec.wep_length = 0
    THEN
      IF p_wep_key IS NULL
      THEN
        v_err_msg := NULL;
        RETURN;
      ELSE
        v_err_msg := wep_found_not_expected;
        RETURN;
      END IF;
    END IF;

    weplen := LENGTH (p_wep_key);

    IF p_item_attributes_rec.wep_type = 'H'
    THEN
      SELECT MAX ('Y') AS v_hex
        INTO v_hex
        FROM DUAL
       WHERE REGEXP_LIKE (UPPER (p_wep_key), '[^0-9 A-F]');

      IF v_hex IS NOT NULL
      THEN
        v_err_msg := wep_not_hex;
        RETURN;
      END IF;

      weplen := weplen / 2;
    END IF;

    wepabs := ABS (p_item_attributes_rec.wep_length);

    IF (weplen != wepabs)
    THEN
      v_err_msg := wep_length_error;
      RETURN;
    END IF;

    v_err_msg := NULL;
  EXCEPTION
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_WEP PROC: ' || SQLCODE || SQLERRM);
      v_err_msg := 'Error while executing CHECK_WEP PROC: ' || SQLCODE || SQLERRM;
  END check_wep;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME: CHECK_WPA
--------------------------------------------------------------------
  PROCEDURE check_wpa (
    p_wpa_key                   IN       VARCHAR2
   ,p_serial_number                      VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    v_serial_number               VARCHAR2 (30);
    wpalen                        NUMBER;
    v_hex                         VARCHAR2 (50);
    wpaabs                        NUMBER;
    v_wpacharset                  VARCHAR2 (250) := '';
    v_key_char_set                VARCHAR2 (250);
    lastch                        VARCHAR2 (1) := ' ';
    lastchcount                   NUMBER := 1;
    letters                       VARCHAR2 (250) := ' ';
    letter                        VARCHAR2 (1);
    ch                            VARCHAR2 (1);
  BEGIN
    IF p_item_attributes_rec.wpa_length = 0
    THEN
      IF p_wpa_key IS NULL
      THEN
        v_err_msg := NULL;
        RETURN;
      ELSE
        v_err_msg := wpa_found_not_expected;
        RETURN;
      END IF;
    ELSE
      IF p_wpa_key IS NULL
      THEN
        v_err_msg := wpa_not_found_expected;
        RETURN;
      END IF;
    END IF;

    wpalen := LENGTH (p_wpa_key);

    IF p_item_attributes_rec.wpa_type = 'H'
    THEN
      SELECT MAX ('Y') AS v_hex
        INTO v_hex
        FROM DUAL
       WHERE REGEXP_LIKE (UPPER (p_wpa_key), '[^0-9 A-F]');

      IF v_hex IS NOT NULL
      THEN
        v_err_msg := wpa_not_hex;
        RETURN;
      END IF;

      wpalen := wpalen / 2;
    END IF;

    wpaabs := ABS (p_item_attributes_rec.wpa_length);

    IF (wpalen != wpaabs)
    THEN
      v_err_msg := wpa_length_error;
      RETURN;
    END IF;

    IF v_wpacharset IS NULL
    THEN
      SELECT alpha || NUMERIC || symbol
        INTO v_key_char_set
        FROM imec.key_charsets
       WHERE key_type = 'WPA' AND sub_type = p_item_attributes_rec.wpa_limited_char_set;
    END IF;

    FOR i IN 1 .. LENGTH (p_wpa_key)
    LOOP
      ch := SUBSTR (p_wpa_key, i, 1);

      IF INSTR (v_key_char_set, ch) = 0
      THEN
        v_err_msg := wpa_invalid_char || '-' || ch;
        RETURN;
      END IF;

      IF ch = lastch
      THEN
        lastchcount := lastchcount + 1;

        IF (lastchcount > 2)
        THEN
          v_err_msg := wpa_char_duplicate_error || ch;
          RETURN;
        END IF;
      ELSE
        lastch := ch;
        lastchcount := 1;
      END IF;

      letter := SUBSTR (p_wpa_key, i, 1);

      IF INSTR (letters, letter) = 0
      THEN
        letters := letters || letter;
      END IF;
    END LOOP;

    --XXNTGR_DEBUG_UTL.LOG('Inside Check WPA key 5 = '|| LENGTH(letters) ||'-'|| LENGTH(TRIM(letters)));
    IF LENGTH (TRIM (letters)) < p_item_attributes_rec.wpa_min
    THEN
      v_err_msg := wpa_minimum_chars;
      RETURN;
    END IF;

    SELECT serialnumber
      INTO v_serial_number
      FROM imec.assigned_wep
     WHERE wpakey = p_wpa_key;

--AND ROWNUM = 1;
    IF v_serial_number IS NOT NULL AND v_serial_number <> p_serial_number
    THEN
      v_err_msg := wpa_assigned_to_another || '-' || v_serial_number;
      RETURN;
    END IF;

    v_err_msg := NULL;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      v_err_msg := NULL;
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_WPA PROC: ' || SQLCODE || SQLERRM);
      v_err_msg := 'Error while executing CHECK_WPA PROC: ' || SQLCODE || SQLERRM;
  END check_wpa;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME: CHECK_WIFI
--------------------------------------------------------------------
  PROCEDURE check_wifi (
    p_uid                       IN       VARCHAR2
   ,p_serial_number                      VARCHAR2
   ,p_item_attributes_rec       IN       serial_control%ROWTYPE
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    v_u_id                        VARCHAR2 (30);
    v_sn                          VARCHAR2 (30);
  BEGIN
    IF p_item_attributes_rec.wifi_cal_file = 0
    THEN
      IF p_uid IS NOT NULL
      THEN
        v_err_msg := wifi_uid_found_not_expected;
        RETURN;
      ELSE
        v_err_msg := NULL;
        RETURN;
      END IF;
    ELSE
      IF p_uid IS NULL
      THEN
        v_err_msg := wifi_uid_not_found_expected;
        RETURN;
      END IF;
    END IF;

    SELECT u_id, sn
      INTO v_u_id, v_sn
      FROM imec.wifi_caldata
     WHERE u_id = p_uid;

    IF v_sn IS NOT NULL AND v_sn <> p_serial_number
    THEN
      v_err_msg := wifi_data_assigned_to_another || '-' || v_sn;
      RETURN;
    END IF;

    IF v_u_id IS NULL
    THEN
      v_err_msg := wifi_data_uid_not_found;
      RETURN;
    END IF;

    v_err_msg := NULL;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      v_err_msg := wifi_data_uid_not_found;
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.error ('Error while executing CHECK_WIFI PROC: ' || SQLCODE || SQLERRM);
      v_err_msg := 'Error while executing CHECK_WIFI PROC: ' || SQLCODE || SQLERRM;
  END;

--------------------------------------------------------------------
-- PROCEDURE
-- NAME: XXNTGR_BELL_EMAIL
--------------------------------------------------------------------
  PROCEDURE xxntgr_bell_email (
    p_email_ranges              IN       VARCHAR2
   ,v_err_msg                   OUT      VARCHAR2
  )
  IS
    v_vendor_id                   NUMBER;
    v_email_count                 NUMBER;
    v_manufacturer                VARCHAR2 (60);
    v_bell_mail_id                VARCHAR2 (1000);
    v_notification                VARCHAR2 (32000);
  BEGIN
    BEGIN
      SELECT description
        INTO v_bell_mail_id
        FROM fnd_lookup_values
       WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
         AND enabled_flag = 'Y'
         AND lookup_code = 'BELL_MOBILITY';
    EXCEPTION
      WHEN OTHERS
      THEN
        v_err_msg := 'Error while getting  mail id for Bell Mobility: ' || SQLCODE || SQLERRM;
    END;

    --get mail body from FND message
    BEGIN
      fnd_message.set_name ('XXNTGR', 'XXNTGR_PTM_BELL_MOBILITY_EMAIL');
      -- Set the ASN_NUMBER
      fnd_message.set_token ('RANGES', p_email_ranges);
      -- Retrieve the message
      v_notification := fnd_message.get;
    EXCEPTION
      WHEN OTHERS
      THEN
        v_err_msg := 'Error while setting value for FND MESSAGE: ' || SQLCODE || SQLERRM;
    END;

    BEGIN
      xxntgr_utl_mail.send (p_sender                     => 'donotreply@netgear.com'
                           ,p_recipients                 => v_bell_mail_id
                           ,p_cc                         => NULL
                           ,p_bcc                        => NULL
                           ,p_subject                    => 'Netgear sending IMEI Ranges to Bell Mobility'
                           ,p_message                    => v_notification
                           ,p_mime_type                  => 'text/html; charset=us-ascii'
                           );
    EXCEPTION
      WHEN OTHERS
      THEN
        v_err_msg := 'Error while sending mail: ' || SQLCODE || SQLERRM;
    END;
  EXCEPTION
    WHEN OTHERS
    THEN
      v_err_msg := 'Error while sending mail: ' || SQLCODE || SQLERRM;
  END;

-- GP END
------------
  PROCEDURE xxntgr_ptm_odm_asnmiss (
    errbuf                      OUT      VARCHAR2
   ,retcode                     OUT      VARCHAR2
   ,p_asn_no                    IN       VARCHAR2
   ,p_odm_name                  IN       VARCHAR2
   ,p_delete_asn                IN       VARCHAR2
   ,p_summary_email             IN       VARCHAR2
  )
  AS
----
---Get new shipments from shipment header table to insert into notification table
    CURSOR get_new_shipments
    IS
      SELECT DISTINCT h.shipment_num, h.shipment_header_id, h.vendor_id
                     ,TO_CHAR (h.creation_date, 'DD-MON-YYYY') creation_date
                     ,TO_CHAR (h.shipped_date, 'DD-MON-YYYY') shipped_date
                     ,TO_CHAR (h.expected_receipt_date, 'DD-MON-YYYY') expected_receipt_date
                     ,h.carrier_method
                 FROM rcv_shipment_headers h, rcv_shipment_lines l, mtl_system_items_b mst
                WHERE h.shipment_header_id = l.shipment_header_id
                  AND h.receipt_source_code = 'VENDOR'
                  AND h.asn_type = 'ASN'
                  AND l.source_document_code = 'PO'
                  AND l.item_id = mst.inventory_item_id
                  AND mst.organization_id = 488
--AND mst.item_type IN (SELECT lookup_code FROM fnd_lookup_values WHERE lookup_type = 'XXNTGR_PTM_ODM_ITEM_TYPE' AND enabled_flag='Y')
                  AND h.shipment_header_id >
                        (SELECT TO_NUMBER (description)
                           FROM fnd_lookup_values
                          WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                            AND lookup_code = 'LAST_RECT_HDR_ID'
                            AND enabled_flag = 'Y')
                  AND TO_CHAR (h.vendor_id) IN (
                             SELECT lookup_code
                               FROM fnd_lookup_values
                              WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                    AND enabled_flag = 'Y')
                  AND l.quantity_shipped > 0
                  AND (   h.vendor_id <> 6037                                              --FOXCONN
                       OR mst.segment1 NOT IN (
                            SELECT meaning
                              FROM fnd_lookup_values
                             WHERE lookup_type = 'XXNTGR_PTM_EXCLUDED_PARTS_FOX'
                               AND enabled_flag = 'Y')
                      )
             ORDER BY h.shipment_header_id;

--Get all the shipments(ASNs) to send notifications
    CURSOR get_asn_to_notify
    IS
      SELECT *
        FROM xxntgr_ptm_odm_ship_notif
       WHERE end_date_active IS NULL;

---Cursor for summary mail
    CURSOR group_asn_by_odm
    IS
      SELECT DISTINCT vendor_id
                 FROM xxntgr_ptm_odm_ship_notif
                WHERE end_date_active IS NULL
                  AND TRUNC (SYSDATE) >
                        (  NVL (shipped_date, creation_date)
                         + (SELECT TO_NUMBER (description)
                              FROM fnd_lookup_values
                             WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                               AND lookup_code = 'NOTIFY_DELAY'
                               AND enabled_flag = 'Y')
                        );

    CURSOR send_summary_mail (
      p_odm                                NUMBER
    )
    IS
      SELECT   asn_number, start_date_active, email_count, shipped_date, expected_receipt_date
              ,carrier_method
              , (SELECT 'Yes'
                   FROM DUAL
                  WHERE EXISTS (
                              SELECT NULL
                                FROM xxntgr_ptm_odm_shipment_arch
                               WHERE asn_number =
                                                 xxntgr_ptm_odm_ship_notif.asn_number))
                                                                                      upload_failed
          FROM xxntgr_ptm_odm_ship_notif
         WHERE vendor_id = p_odm
           AND end_date_active IS NULL
           AND TRUNC (SYSDATE) >
                 (  NVL (shipped_date, creation_date)
                  + (SELECT TO_NUMBER (description)
                       FROM fnd_lookup_values
                      WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                        AND lookup_code = 'NOTIFY_DELAY'
                        AND enabled_flag = 'Y')
                 )
      ORDER BY shipped_date;

    CURSOR chk_asn_befor_insert (
      p_asn                                VARCHAR2
     ,p_vendor_id                          NUMBER
    )
    IS
      SELECT DISTINCT asn_number
                 FROM xxntgr_ptm_po_shipment_trx
                WHERE asn_number = p_asn
                  AND UPPER (manufacturer_name) =
                        (SELECT meaning
                           FROM fnd_lookup_values
                          WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                            AND lookup_code = TO_CHAR (p_vendor_id)
                            AND enabled_flag = 'Y');

    CURSOR chk_asn_befor_notif (
      p_asn                                VARCHAR2
     ,p_odm                                VARCHAR2
    )
    IS
      SELECT DISTINCT asn_number
                 FROM xxntgr_ptm_po_shipment_trx
                WHERE asn_number = p_asn AND manufacturer_name = p_odm
      UNION
      SELECT DISTINCT h.shipment_num
                 FROM rcv_shipment_headers h, rcv_shipment_lines l
                WHERE h.shipment_header_id = l.shipment_header_id
                  AND h.receipt_source_code = 'VENDOR'
                  AND h.asn_type = 'ASN'
                  AND l.source_document_code = 'PO'
                  AND l.quantity_shipped = 0                                             --cancelled
                  AND h.shipment_num = p_asn
                  AND TO_CHAR (h.vendor_id) =
                        (SELECT lookup_code
                           FROM fnd_lookup_values
                          WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                            AND enabled_flag = 'Y'
                            AND meaning = p_odm);

----
    v_last_rect_id                NUMBER;
    v_asn                         VARCHAR2 (60);
    v_asn_no                      VARCHAR2 (60);
    v_notify_days0                NUMBER;
    v_notify_days1                NUMBER;
    v_notify_days2                NUMBER;
    v_notify_days3                NUMBER;
    v_notify_delay                NUMBER;
    v_manufacturer                VARCHAR2 (60);
    v_mailid                      VARCHAR2 (1000);
    v_notification                VARCHAR2 (32000);
    v_vendor_id                   NUMBER;
    v_email_count                 NUMBER;
    v_get_mail_error              VARCHAR2 (32000);
    v_odm_name                    VARCHAR2 (60);
    v_table_data                  VARCHAR2 (31800);
    v_length                      NUMBER;
    v_smail_count                 VARCHAR2 (10);
    v_asn_to_del                  VARCHAR2 (32767);
    v_asn_count                   NUMBER;
    v_del_asn                     VARCHAR2 (60);
  BEGIN
    --Delete ASN from notification table
    --If ASN_NUMBER is passed as INPUT paramter then disable that ASN in notification table, send mail and exit program
    IF (p_asn_no IS NOT NULL OR p_odm_name IS NOT NULL)
    THEN
      IF (p_asn_no IS NULL OR p_odm_name IS NULL)
      THEN
        xxntgr_debug_utl.LOG ('ASN Number OR ODM Name is null.', '2');
        retcode := 2;
      END IF;
    END IF;

    IF p_asn_no IS NOT NULL
    THEN
      IF p_odm_name IS NOT NULL
      THEN
        BEGIN
          SELECT lookup_code
            INTO v_vendor_id
            FROM fnd_lookup_values
           WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
             AND meaning = UPPER (p_odm_name)
             AND enabled_flag = 'Y';

          IF p_delete_asn = 'Y'
          THEN
            DELETE FROM xxntgr_ptm_odm_ship_notif
                  WHERE asn_number = p_asn_no AND vendor_id = v_vendor_id;

            COMMIT;
          ELSE
            UPDATE xxntgr_ptm_odm_ship_notif
               SET end_date_active = SYSDATE
             WHERE asn_number = p_asn_no AND vendor_id = v_vendor_id;

            COMMIT;
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
            v_vendor_id := NULL;
            xxntgr_debug_utl.LOG ('ODM Name ' || p_odm_name || ' is invalid.', '1');
            retcode := 2;
          WHEN OTHERS
          THEN
            v_vendor_id := NULL;
            xxntgr_debug_utl.LOG (   'Error while geting vendor id for ODM name : '
                                  || p_odm_name
                                  || '-'
                                  || SQLCODE
                                  || SQLERRM
                                 ,'1'
                                 );
            retcode := 2;
        END;
      END IF;
    ELSE
      IF p_asn_no IS NULL
      THEN
        IF p_delete_asn = 'Y'
        THEN
          v_asn_to_del := NULL;

          BEGIN
            fnd_message.set_name ('XXNTGR', 'XXNTGR_PTM_ODMFILEDELAY_DELETE');
            v_asn_to_del := fnd_message.get;
            v_asn_to_del := REPLACE (v_asn_to_del, CHR (10), '');
            v_asn_count :=
                        NVL ((LENGTH (REGEXP_REPLACE (v_asn_to_del, '[^,]')) / LENGTH (',') + 1)
                            ,0);

            IF v_asn_count = 0
            THEN
              IF v_asn_to_del IS NOT NULL
              THEN
                v_asn_count := 1;
              END IF;
            END IF;

            FOR i IN 1 .. v_asn_count
            LOOP
              v_del_asn :=
                       NVL (SUBSTR (v_asn_to_del, 1, (INSTR (v_asn_to_del, ',') - 1))
                           ,v_asn_to_del);

              DELETE FROM xxntgr_ptm_odm_ship_notif
                    WHERE asn_number = TRIM (v_del_asn);

              -- GP STARTS -- delete ASNs with a space in the start/end position -- 06-AUG-12
              DELETE FROM xxntgr_ptm_odm_ship_notif
                    WHERE asn_number = v_del_asn;

              v_asn_to_del :=
                          NVL (SUBSTR (v_asn_to_del, (INSTR (v_asn_to_del, ',') + 1))
                              ,v_asn_to_del);
            END LOOP;
          EXCEPTION
            WHEN OTHERS
            THEN
              xxntgr_debug_utl.LOG (   'Error while deleting ASNs listed in FND_MESSAGE: '
                                    || SQLCODE
                                    || SQLERRM
                                   ,'1'
                                   );
              retcode := 2;
          END;
        ELSE                                                  --Start send notification mail process
          v_last_rect_id := NULL;

          FOR i IN get_new_shipments
          LOOP
            v_asn := NULL;

            --Check if ASN exists in XXNTGR_PTM_PO_SHIPMENT_TRX table
            BEGIN
              OPEN chk_asn_befor_insert (i.shipment_num, i.vendor_id);

              FETCH chk_asn_befor_insert
               INTO v_asn;

              CLOSE chk_asn_befor_insert;

              IF v_asn IS NULL
              THEN
                --Check if ASN exists in notificatin table
                BEGIN
                  SELECT DISTINCT asn_number
                             INTO v_asn
                             FROM xxntgr_ptm_odm_ship_notif
                            WHERE asn_number = i.shipment_num AND vendor_id = i.vendor_id;
                EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                    -- mmore 3.0 Added column names in below insert statement
                    INSERT INTO xxntgr_ptm_odm_ship_notif
                                (asn_number, vendor_id, start_date_active, end_date_active
                                ,email_count, shipped_date, expected_receipt_date, carrier_method
                                ,created_by, creation_date, last_updated_by, last_update_date
                                )
                         VALUES (i.shipment_num,                                        --ASN NUMBER
                                                i.vendor_id,                            -- VENDOR_ID
                                                            i.creation_date,     --START_DATE_ACTIVE
                                                                            NULL
                                ,                                                  --END_DATE_ACTIVE
                                 0,                                                    --EMAIL_COUNT
                                   i.shipped_date,                                    --SHIPPED_DATE
                                                  i.expected_receipt_date, i.carrier_method
                                ,2500,                                                  --CREATED_BY
                                      SYSDATE,                                       --CREATION_DATE
                                              2500,                                --LAST_UPDATED_BY
                                                   SYSDATE
                                );                                                --LAST_UPDATE_DATE

                    COMMIT;
                  WHEN OTHERS
                  THEN
                    xxntgr_debug_utl.LOG
                             (   'ASN: '
                              || i.shipment_num
                              || ' ODM: '
                              || i.vendor_id
                              || ' -Error while checking for existing asn in notification table: '
                              || SQLCODE
                              || SQLERRM
                             ,'1'
                             );
                END;
              END IF;
            EXCEPTION
              WHEN OTHERS
              THEN
                NULL;
                xxntgr_debug_utl.LOG
                             (   'ASN: '
                              || i.shipment_num
                              || ' ODM: '
                              || i.vendor_id
                              || ' -Error while checking for existing asn in shipment_trx table: '
                              || SQLCODE
                              || SQLERRM
                             ,'1'
                             );
            END;

            v_last_rect_id := i.shipment_header_id;
          END LOOP;

          --UPDATE common lookup table with the last shipment_header_id
          IF v_last_rect_id IS NOT NULL
          THEN
            BEGIN
              UPDATE fnd_lookup_values
                 SET description = v_last_rect_id
               WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                 AND lookup_code = 'LAST_RECT_HDR_ID'
                 AND enabled_flag = 'Y';

              COMMIT;
            EXCEPTION
              WHEN OTHERS
              THEN
                xxntgr_debug_utl.LOG (   'Error while getting last shipment_header_id: '
                                      || SQLCODE
                                      || SQLERRM
                                     ,'1'
                                     );
            --XXNTGR_DEBUG_UTIL.LOG('Error while getting last shipment_header_id: '||sqlcode||sqlerrm);
            END;
          END IF;

          --Send notification mail process starts
          IF p_summary_email = 'N'
          THEN
            BEGIN
              FOR i IN get_asn_to_notify
              LOOP
                SELECT meaning
                  INTO v_odm_name
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                   AND lookup_code = TO_CHAR (i.vendor_id)
                   AND enabled_flag = 'Y';

                BEGIN
                  SELECT DISTINCT asn_number
                             INTO v_asn_no
                             FROM xxntgr_ptm_po_shipment_trx
                            WHERE asn_number = i.asn_number
                                  AND UPPER (manufacturer_name) = v_odm_name;

                  UPDATE xxntgr_ptm_odm_ship_notif
                     SET end_date_active = SYSDATE
                   WHERE asn_number = i.asn_number AND vendor_id = i.vendor_id;

                  COMMIT;
                EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                    IF i.email_count = 0
                    THEN
                      SELECT TO_NUMBER (description)
                        INTO v_notify_days0
                        FROM fnd_lookup_values
                       WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                         AND lookup_code = 'NOTIFY_DAYS0'
                         AND enabled_flag = 'Y';

                      IF TRUNC (SYSDATE) > i.start_date_active + v_notify_days0
                      THEN
                        --Send notification mail to ODM
                        BEGIN
                          xxntgr_ptm_odm_pkg.file_delay_mail (i.asn_number
                                                             ,v_odm_name
                                                             ,i.email_count
                                                             ,v_get_mail_error
                                                             );
                          xxntgr_debug_utl.LOG (v_get_mail_error, '1');
                        EXCEPTION
                          WHEN OTHERS
                          THEN
                            NULL;
                        END;
                      ----
                      END IF;

                      UPDATE xxntgr_ptm_odm_ship_notif
                         SET email_count = 1
                       WHERE asn_number = i.asn_number AND vendor_id = i.vendor_id;

                      COMMIT;
                    ELSE
                      IF i.email_count = 1
                      THEN
                        SELECT TO_NUMBER (description)
                          INTO v_notify_days1
                          FROM fnd_lookup_values
                         WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                           AND lookup_code = 'NOTIFY_DAYS1'
                           AND enabled_flag = 'Y';

                        IF TRUNC (SYSDATE) > i.start_date_active + v_notify_days1
                        THEN
                          --Send notification mail to ODM
                          BEGIN
                            xxntgr_ptm_odm_pkg.file_delay_mail (i.asn_number
                                                               ,v_odm_name
                                                               ,i.email_count
                                                               ,v_get_mail_error
                                                               );
                            xxntgr_debug_utl.LOG (v_get_mail_error, '1');
                          EXCEPTION
                            WHEN OTHERS
                            THEN
                              NULL;
                          END;
                        END IF;

                        UPDATE xxntgr_ptm_odm_ship_notif
                           SET email_count = 2
                         WHERE asn_number = i.asn_number AND vendor_id = i.vendor_id;

                        COMMIT;
                      ELSE
                        IF i.email_count = 2
                        THEN
                          SELECT TO_NUMBER (description)
                            INTO v_notify_days2
                            FROM fnd_lookup_values
                           WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                             AND lookup_code = 'NOTIFY_DAYS2'
                             AND enabled_flag = 'Y';

                          IF TRUNC (SYSDATE) > i.start_date_active + v_notify_days2
                          THEN
                            --Send notification mail to ODM
                            BEGIN
                              xxntgr_ptm_odm_pkg.file_delay_mail (i.asn_number
                                                                 ,v_odm_name
                                                                 ,i.email_count
                                                                 ,v_get_mail_error
                                                                 );
                              xxntgr_debug_utl.LOG (v_get_mail_error, '1');
                            EXCEPTION
                              WHEN OTHERS
                              THEN
                                NULL;
                            END;
                          END IF;

                          UPDATE xxntgr_ptm_odm_ship_notif
                             SET email_count = 3
                           WHERE asn_number = i.asn_number AND vendor_id = i.vendor_id;

                          COMMIT;
                        ELSE
                          IF i.email_count > 2
                          THEN
                            SELECT TO_NUMBER (description)
                              INTO v_notify_days3
                              FROM fnd_lookup_values
                             WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                               AND lookup_code = 'NOTIFY_DAYS3'
                               AND enabled_flag = 'Y';

                            IF MOD (TRUNC (TRUNC (SYSDATE) - i.start_date_active), v_notify_days3) =
                                                                                                   0
                            THEN                                      --send mail in every 5/10 days
                              --Send notification mail to ODM
                              BEGIN
                                xxntgr_ptm_odm_pkg.file_delay_mail (i.asn_number
                                                                   ,v_odm_name
                                                                   ,i.email_count
                                                                   ,v_get_mail_error
                                                                   );
                                xxntgr_debug_utl.LOG (v_get_mail_error, '1');
                              EXCEPTION
                                WHEN OTHERS
                                THEN
                                  NULL;
                              END;
                            END IF;

                            UPDATE xxntgr_ptm_odm_ship_notif
                               SET email_count = email_count + 1
                             WHERE asn_number = i.asn_number AND vendor_id = i.vendor_id;

                            COMMIT;
                          END IF;
                        END IF;
                      END IF;
                    END IF;
                  --END IF;
                  WHEN OTHERS
                  THEN
                    ROLLBACK;
                    xxntgr_debug_utl.LOG (   'ASN: '
                                          || i.asn_number
                                          || ' ODM: '
                                          || v_odm_name
                                          || ' -Error while sending notification mail: '
                                          || SQLCODE
                                          || SQLERRM
                                         ,'1'
                                         );
                    retcode := 2;
                END;
              END LOOP;
            END;
          ELSE                                                       --IF P_SUMMARY_EMAIL = 'Y' THEN
            BEGIN
              FOR dist_odm IN group_asn_by_odm
              LOOP
                BEGIN
                  SELECT meaning
                    INTO v_odm_name
                    FROM fnd_lookup_values
                   WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                     AND lookup_code = TO_CHAR (dist_odm.vendor_id)
                     AND enabled_flag = 'Y';
                EXCEPTION
                  WHEN OTHERS
                  THEN
                    xxntgr_debug_utl.LOG (   'ODM: '
                                          || v_odm_name
                                          || ' Error while geting ODM name: '
                                          || SQLCODE
                                          || SQLERRM
                                         ,'1'
                                         );
                END;

                v_table_data := NULL;
                v_length := NULL;

                FOR summ_mail IN send_summary_mail (dist_odm.vendor_id)
                LOOP
                  BEGIN
                    OPEN chk_asn_befor_notif (summ_mail.asn_number, v_odm_name);

                    v_asn_no := NULL;

                    FETCH chk_asn_befor_notif
                     INTO v_asn_no;

                    CLOSE chk_asn_befor_notif;

                    IF v_asn_no IS NOT NULL
                    THEN
                      UPDATE xxntgr_ptm_odm_ship_notif
                         SET end_date_active = SYSDATE
                       WHERE asn_number = summ_mail.asn_number AND vendor_id = dist_odm.vendor_id;

                      COMMIT;
                    ELSE
                      BEGIN
                        SELECT summ_mail.email_count + 1
                          INTO v_smail_count
                          FROM DUAL;

                        v_table_data :=
                             v_table_data
                          || '<tr><td>'
                          || summ_mail.asn_number
                          || '</td><td>'
                          || TO_CHAR (summ_mail.shipped_date, 'DD-MON-YYYY')
                          || '</td><td>'
                          || TO_CHAR (summ_mail.expected_receipt_date, 'DD-MON-YYYY')
                          || '</td><td>'
                          || summ_mail.carrier_method
                          || '</td><td>'
                          || TRUNC (SYSDATE - summ_mail.shipped_date)
                          || '</td><td>'
                          || v_smail_count
                          || '</td><td>'
                          || summ_mail.upload_failed
                          || '</td></tr>';
                      EXCEPTION
                        WHEN OTHERS
                        THEN
                          ROLLBACK;
                          xxntgr_debug_utl.LOG (   'Error while assigning value to v_table_data: '
                                                || SQLCODE
                                                || SQLERRM
                                               ,'1'
                                               );
                      END;

                      UPDATE xxntgr_ptm_odm_ship_notif
                         SET email_count = email_count + 1
                       WHERE asn_number = summ_mail.asn_number AND vendor_id = dist_odm.vendor_id;
                    END IF;
                  EXCEPTION
                    WHEN OTHERS
                    THEN
                      xxntgr_debug_utl.LOG
                                        (   'Error while checking for existing ASN in trx table: '
                                         || SQLCODE
                                         || SQLERRM
                                        ,'1'
                                        );
                  END;
                END LOOP;

                v_length := NVL (LENGTH (v_table_data), 0);

                --Send notification mail to ODM
                IF v_length < 1500
                THEN
                  IF v_length <> 0
                  THEN
                    BEGIN
                      xxntgr_ptm_odm_pkg.file_delay_summ_mail (v_odm_name
                                                              ,v_table_data
                                                              ,v_get_mail_error
                                                              );
                      xxntgr_debug_utl.LOG (   'File delay notification to '
                                            || v_odm_name
                                            || ' '
                                            || v_get_mail_error
                                           ,'1'
                                           );
                    EXCEPTION
                      WHEN OTHERS
                      THEN
                        xxntgr_debug_utl.LOG (   'Error while calling mail program: '
                                              || SQLCODE
                                              || SQLERRM
                                             ,'1'
                                             );
                    END;
                  END IF;
                ELSE
                  --get mail id's to send mail
                  BEGIN
                    SELECT description
                      INTO v_mailid
                      FROM fnd_lookup_values
                     WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
                       AND enabled_flag = 'Y'
                       AND SUBSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1))
                                  , (  INSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1))
                                             ,'_'
                                             )
                                     + 1
                                    )
                                  ) =
                             (SELECT lookup_code
                                FROM fnd_lookup_values
                               WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                                 AND meaning = UPPER (v_odm_name)
                                 AND enabled_flag = 'Y');
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      SELECT description
                        INTO v_mailid
                        FROM fnd_lookup_values
                       WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
                         AND lookup_code = 'ODM_SHIPMENT1'
                         AND enabled_flag = 'Y';
                    WHEN OTHERS
                    THEN
                      xxntgr_debug_utl.LOG (   'Error while getting manufacturer mail id: '
                                            || SQLCODE
                                            || SQLERRM
                                           ,'1'
                                           );
                  END;

                  --Send Mail
                  BEGIN
                    xxntgr_utl_mail.send
                      (p_sender                     => 'donotreply@netgear.com'
                      ,p_recipients                 => v_mailid
                      ,p_cc                         => NULL
                      ,p_bcc                        => NULL
                      ,p_subject                    =>    'PTM ODM Interface ('
                                                       || v_odm_name
                                                       || ') - Serial Number File DELAYED!!'
                      ,p_message                    =>    'This message is intended to notify all concerned parties that Netgear has not received Serial Number files for following Invoices:
                            <br><br>
                            <table border="1">
                            <tr bgcolor="gray">
                            <td>Invoice Number</td>
                            <td>Shipment Date</td>
                            <td>Expected Receipt Date</td>
                            <td>Ship Mode</td>
                            <td>Days Delayed</td>
                            <td>Notification#</td>
                            <td>Upload Failed</td>
                            </tr>'
                                                       || v_table_data
                                                       || '</table>
                            <br><br>
                            Delay in delivery of this file has an adverse effect on Netgears Sales Order fulfillment process.<br><br>PLEASE TAKE CORRECTIVE MEASURES AT THE EARLIEST.
                            <br><br>
                            - Netgear IT,'
                      ,p_mime_type                  => 'text/html; charset=us-ascii'
                      );
                  EXCEPTION
                    WHEN OTHERS
                    THEN
                      ROLLBACK;
                      xxntgr_debug_utl.LOG (   v_odm_name
                                            || ' :Error while sending mail : '
                                            || SQLCODE
                                            || SQLERRM
                                           ,'1'
                                           );
                      retcode := 2;
                  END;
                END IF;
              END LOOP;

              COMMIT;
            EXCEPTION
              WHEN OTHERS
              THEN
                ROLLBACK;
                xxntgr_debug_utl.LOG ('Error in summary mail program: ' || SQLCODE || SQLERRM, '1');
                retcode := 2;
            END;
          END IF;
        END IF;
      END IF;
    END IF;
  END;

-------------
-------------
------------------------------------
--PROCEDURE: XXNTGR_PTM_DC_GENSNFILE
------------------------------------
  PROCEDURE xxntgr_ptm_dc_gensnfile (
    errbuf                      OUT      VARCHAR2
   ,retcode                     OUT      VARCHAR2
   ,p_asn_number                         VARCHAR2
   ,p_mfg_name                           VARCHAR2
  )
  IS
    CURSOR get_dist_asn
    IS
      SELECT DISTINCT asn_number, UPPER (manufacturer_name) manufacturer_name, flv.description dsv
                 FROM xxntgr_ptm_odm_shipment_stg stg
                     ,rcv_shipment_headers rsh
                     ,rcv_shipment_lines rsl
                     ,fnd_lookup_values flv
                     ,mtl_system_items_b msi
                WHERE stg.asn_number = TRIM (rsh.shipment_num)
                  AND rsh.shipment_header_id = rsl.shipment_header_id
                  AND rsl.item_id = msi.inventory_item_id
                  AND stg.asn_number = NVL (p_asn_number, stg.asn_number)
                  AND UPPER (stg.manufacturer_name) =
                                                     NVL (p_mfg_name, UPPER (stg.manufacturer_name))
                  AND rsl.to_organization_id = flv.lookup_code
                  AND flv.lookup_type = 'XXNTGR_PTM_DC_NAMES'
                  AND flv.enabled_flag = 'Y'
                  AND stg.process_status = 'P'
                  AND msi.organization_id = 488
                  AND NVL (msi.attribute19, 'N') <> 'AIRCARD'
      UNION
      /*below query will give data only if asn and mfg is passed as parameter.
       this is added to handle the case if asn is already archived for the passed parameter (asn/mfg),
       and there is a requirment to create file.
       */
      SELECT DISTINCT asn_number, UPPER (manufacturer_name), flv.description dsv
                 FROM xxntgr_ptm_po_shipment_trx trx, fnd_lookup_values flv, mtl_system_items_b msi
                WHERE trx.asn_number = p_asn_number
                  AND trx.inventory_item_id = msi.inventory_item_id
                  AND UPPER (trx.manufacturer_name) = p_mfg_name
                  AND trx.ship_to_org_code = flv.meaning
                  AND flv.lookup_type = 'XXNTGR_PTM_DC_NAMES'
                  AND flv.enabled_flag = 'Y'
                  AND p_asn_number IS NOT NULL
                  AND msi.organization_id = 488
                  AND NVL (msi.attribute19, 'N') <> 'AIRCARD';

    CURSOR get_data (
      p_asn                                VARCHAR2
     ,p_manufacturer                       VARCHAR2
    )
    IS
      SELECT trx.pallet_id, trx.organization_id, trx.master_carton_id, trx.serial_number
            ,trx.item_number, ins.mac_address, trx.asn_number, trx.container_number, trx.po_number
            ,trx.manufacturer_name, ins.imei_number, ins.masterlock_number, ins.networklock_number
            ,ins.servicelock_number, ins.assembly_revision, ins.assembly_number, trx.item_revision
            ,ins.wep_key, ins.wifi_id, ins.access_code, ins.primary_ssid, ins.wpa_key
            ,ins.mac_id_cable, ins.mac_id_emta, ins.hardware_version, ins.firmware_version
            ,ins.ean_code, ins.software_version, ins.srm_password, ins.mac_id_rf, ins.mac_id_mta
            ,ins.mac_id_mta_man_router, ins.mac_id_mta_data, ins.mac_id_ethernet, ins.mac_id_usb
            ,ins.primaryssid_passphrase, ins.mac_id_cmci, ins.mac_id_lan, ins.mac_id_wan
            ,ins.mac_id_device, ins.mac_id_wireless, ins.mac_id_wifi_ssid1, ins.ssid1
            ,ins.ssid1_passphrase, ins.wpa_passphrase, ins.wps_pin_code, ins.pppoa_username
            ,ins.pppoa_passphrase, ins.tr069_unique_key_64bit, ins.fon_key, ins.ta_number
            ,ins.other_macs, ins.attribute1, ins.attribute2, ins.attribute3, ins.attribute4
            ,ins.attribute5, ins.attribute6, ins.attribute7, ins.attribute8, ins.attribute9
            ,ins.attribute10, ins.attribute11, ins.attribute12, ins.attribute13, ins.attribute14
            ,ins.attribute15, NULL attribute16, NULL attribute17, NULL attribute18
            ,NULL attribute19, NULL attribute20, NULL attribute21, NULL attribute22
            ,NULL attribute23, NULL attribute24, NULL attribute25, NULL attribute26
            ,NULL attribute27, NULL attribute28, NULL attribute29, NULL attribute30
        FROM xxntgr_ptm_item_instances ins, xxntgr_ptm_po_shipment_trx trx
       --xxntgr_ptm_trx_register reg
      WHERE  ins.instance_id = trx.instance_id
         --and trx.instance_id = reg.instance_id
         AND trx.asn_number = p_asn
         AND UPPER (trx.manufacturer_name) = p_manufacturer
         AND trx.organization_id IN (
                                    SELECT lookup_code
                                      FROM fnd_lookup_values
                                     WHERE lookup_type = 'XXNTGR_PTM_DC_NAMES'
                                           AND enabled_flag = 'Y')
--    AND ins.top_instance_id IS NULL
         AND LENGTH (master_carton_id) > 5;

    /*and not exists (select 1
                    from xxntgr_ptm_po_shipment_trx trx1
                    where trx1.asn_number = trx.asn_number
                    and trx1.master_carton_id in (
                                                select trx2.master_carton_id
                                                 from xxntgr_ptm_po_shipment_trx trx2
                                                 group by trx2.master_carton_id
                                                 having count(distinct trx2.asn_number) > 1)); */
    fhandle                       UTL_FILE.file_type;
    v_file_loc                    VARCHAR2 (100);
    v_out_file_loc                VARCHAR2 (100);
    v_file_name                   VARCHAR2 (100);
    v_header                      VARCHAR2 (32767);
    v_apl_header                  VARCHAR2 (32767);
    v_file_data                   VARCHAR2 (32767);
    v_request                     NUMBER;
    v_mailid                      VARCHAR2 (1000);
    v_err_msg                     VARCHAR2 (32000);
    v_asn_count                   NUMBER;
    v_serial_number_count         NUMBER;
    v_file_start                  VARCHAR2 (50);
    v_file_stop                   VARCHAR2 (50);
    v_unique_id                   VARCHAR2 (100);
    v_po_header_id                NUMBER;
    v_shipment_header_id          NUMBER;
    v_reason                      VARCHAR2 (4000);
    v_file_data_exists_counter    BOOLEAN;
    v_file_exists                 BOOLEAN;
    n_length                      NUMBER;
    n_bsize                       NUMBER;
  BEGIN
    --get the out file location
    BEGIN
      SELECT description
        INTO v_file_loc
        FROM fnd_lookup_values
       WHERE lookup_type = 'XXNTGR_PTM_COMMON'
         AND lookup_code = 'DC_RECT_OUT_FILE_LOC'
         AND enabled_flag = 'Y';
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        xxntgr_debug_utl.error ('Error while getting file location - ' || SQLCODE || SQLERRM);
      WHEN OTHERS
      THEN
        xxntgr_debug_utl.error ('Error while getting file location - ' || SQLCODE || SQLERRM);
    END;

    --get the destination file location
    BEGIN
      SELECT description
        INTO v_out_file_loc
        FROM fnd_lookup_values
       WHERE lookup_type = 'XXNTGR_PTM_COMMON'
         AND lookup_code = 'DC_RECT_DEST_FILE_LOC'
         AND enabled_flag = 'Y';
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        xxntgr_debug_utl.error (   'Error while getting destination file location - '
                                || SQLCODE
                                || SQLERRM
                               );
      WHEN OTHERS
      THEN
        xxntgr_debug_utl.error (   'Error while getting destination file location - '
                                || SQLCODE
                                || SQLERRM
                               );
    END;

    --column headers
    v_header :=
         'PALLET_ID'
      || CHR (9)
      || 'MASTER_CARTON_ID'
      || CHR (9)
      || 'SERIAL_NUMBER'
      || CHR (9)
      || 'ITEM_NUMBER'
      || CHR (9)
      || 'MAC_ADDRESS'
      || CHR (9)
      || 'ASN_NUMBER'
      || CHR (9)
      || 'CONTAINER_NUMBER'
      || CHR (9)
      || 'PO_NUMBER'
      || CHR (9)
      || 'MANUFACTURER_NAME'
      || CHR (9)
      || 'IMEI_NUMBER'
      || CHR (9)
      || 'MASTERLOCK_NUMBER'
      || CHR (9)
      || 'NETWORKLOCK_NUMBER'
      || CHR (9)
      || 'SERVICELOCK_NUMBER'
      || CHR (9)
      || 'FA Number Level Rev'
      || CHR (9)
      || 'FA Number'
      || CHR (9)
      || 'ITEM_REVISION'
      || CHR (9)
      || 'WEP_KEY'
      || CHR (9)
      || 'WIFI_ID'
      || CHR (9)
      || 'ACCESS_CODE'
      || CHR (9)
      || 'PRIMARY_SSID'
      || CHR (9)
      || 'WPA_KEY'
      || CHR (9)
      || 'MAC_ID_CABLE'
      || CHR (9)
      || 'MAC_ID_EMTA'
      || CHR (9)
      || 'HARDWARE_VERSION'
      || CHR (9)
      || 'FIRMWARE_VERSION'
      || CHR (9)
      || 'EAN_CODE'
      || CHR (9)
      || 'SOFTWARE_VERSION'
      || CHR (9)
      || 'SRM_PASSWORD'
      || CHR (9)
      || 'MAC_ID_RF'
      || CHR (9)
      || 'MAC_ID_MTA'
      || CHR (9)
      || 'MAC_ID_MTA_MAN_ROUTER'
      || CHR (9)
      || 'MAC_ID_MTA_DATA'
      || CHR (9)
      || 'MAC_ID_ETHERNET'
      || CHR (9)
      || 'MAC_ID_USB'
      || CHR (9)
      || 'PRIMARYSSID_PASSPHRASE'
      || CHR (9)
      || 'MAC_ID_CMCI'
      || CHR (9)
      || 'MAC_ID_LAN'
      || CHR (9)
      || 'MAC_ID_WAN'
      || CHR (9)
      || 'MAC_ID_DEVICE'
      || CHR (9)
      || 'MAC_ID_WIRELESS'
      || CHR (9)
      || 'MAC_ID_WIFI_SSID1'
      || CHR (9)
      || 'SSID1'
      || CHR (9)
      || 'SSID1_PASSPHRASE'
      || CHR (9)
      || 'WPA_PASSPHRASE'
      || CHR (9)
      || 'WPS_PIN_CODE'
      || CHR (9)
      || 'PPPOA_USERNAME'
      || CHR (9)
      || 'PPPOA_PASSPHRASE'
      || CHR (9)
      || 'TR069_UNIQUE_KEY_64BIT'
      || CHR (9)
      || 'FON_KEY'
      || CHR (9)
      || 'TA_NUMBER'
      || CHR (9)
      || 'OTHER_MACS'
      || CHR (9)
      || 'ATTRIBUTE1'
      || CHR (9)
      || 'ATTRIBUTE2'
      || CHR (9)
      || 'ATTRIBUTE3'
      || CHR (9)
      || 'ATTRIBUTE4'
      || CHR (9)
      || 'ATTRIBUTE5'
      || CHR (9)
      || 'ATTRIBUTE6'
      || CHR (9)
      || 'ATTRIBUTE7'
      || CHR (9)
      || 'ATTRIBUTE8'
      || CHR (9)
      || 'ATTRIBUTE9'
      || CHR (9)
      || 'ATTRIBUTE10'
      || CHR (9)
      || 'ATTRIBUTE11'
      || CHR (9)
      || 'ATTRIBUTE12'
      || CHR (9)
      || 'ATTRIBUTE13'
      || CHR (9)
      || 'ATTRIBUTE14'
      || CHR (9)
      || 'ATTRIBUTE15'
      || CHR (9)
      || 'ATTRIBUTE16'
      || CHR (9)
      || 'ATTRIBUTE17'
      || CHR (9)
      || 'ATTRIBUTE18'
      || CHR (9)
      || 'ATTRIBUTE19'
      || CHR (9)
      || 'ATTRIBUTE20'
      || CHR (9)
      || 'ATTRIBUTE21'
      || CHR (9)
      || 'ATTRIBUTE22'
      || CHR (9)
      || 'ATTRIBUTE23'
      || CHR (9)
      || 'ATTRIBUTE24'
      || CHR (9)
      || 'ATTRIBUTE25'
      || CHR (9)
      || 'ATTRIBUTE26'
      || CHR (9)
      || 'ATTRIBUTE27'
      || CHR (9)
      || 'ATTRIBUTE28'
      || CHR (9)
      || 'ATTRIBUTE29'
      || CHR (9)
      || 'ATTRIBUTE30';

    FOR i IN get_dist_asn
    LOOP
      BEGIN
        v_file_data := NULL;
        v_err_msg := NULL;
        v_file_data_exists_counter := FALSE;
        xxntgr_debug_utl.LOG (' Inside get_dist_asn - ');
        v_asn_count := get_dist_asn%ROWCOUNT;
        v_file_name :=
             'PTMDCO_'
          || i.dsv
          || '_'
          || i.asn_number
          || '_'
          || TO_CHAR (SYSDATE, 'YYYYMMDDHHMISS')
          || '.txt';

        IF i.dsv = 'DSV'
        THEN                                                                         -- gp may-06-13
          FOR j IN get_data (i.asn_number, i.manufacturer_name)
          LOOP
            IF NOT v_file_data_exists_counter
            THEN
              fhandle := UTL_FILE.fopen (v_file_loc, v_file_name, 'w', 32767);
              UTL_FILE.put_line (fhandle, v_header);
              v_file_data_exists_counter := TRUE;
            END IF;

            v_file_data :=
                 j.pallet_id
              || CHR (9)
              || j.master_carton_id
              || CHR (9)
              || j.serial_number
              || CHR (9)
              || j.item_number
              || CHR (9)
              || j.mac_address
              || CHR (9)
              || j.asn_number
              || CHR (9)
              || j.container_number
              || CHR (9)
              || j.po_number
              || CHR (9)
              || j.manufacturer_name
              || CHR (9)
              || j.imei_number
              || CHR (9)
              || j.masterlock_number
              || CHR (9)
              || j.networklock_number
              || CHR (9)
              || j.servicelock_number
              || CHR (9)
              || j.assembly_revision
              || CHR (9)
              || j.assembly_number
              || CHR (9)
              || j.item_revision
              || CHR (9)
              || j.wep_key
              || CHR (9)
              || j.wifi_id
              || CHR (9)
              || j.access_code
              || CHR (9)
              || j.primary_ssid
              || CHR (9)
              || j.wpa_key
              || CHR (9)
              || j.mac_id_cable
              || CHR (9)
              || j.mac_id_emta
              || CHR (9)
              || j.hardware_version
              || CHR (9)
              || j.firmware_version
              || CHR (9)
              || j.ean_code
              || CHR (9)
              || j.software_version
              || CHR (9)
              || j.srm_password
              || CHR (9)
              || j.mac_id_rf
              || CHR (9)
              || j.mac_id_mta
              || CHR (9)
              || j.mac_id_mta_man_router
              || CHR (9)
              || j.mac_id_mta_data
              || CHR (9)
              || j.mac_id_ethernet
              || CHR (9)
              || j.mac_id_usb
              || CHR (9)
              || j.primaryssid_passphrase
              || CHR (9)
              || j.mac_id_cmci
              || CHR (9)
              || j.mac_id_lan
              || CHR (9)
              || j.mac_id_wan
              || CHR (9)
              || j.mac_id_device
              || CHR (9)
              || j.mac_id_wireless
              || CHR (9)
              || j.mac_id_wifi_ssid1
              || CHR (9)
              || j.ssid1
              || CHR (9)
              || j.ssid1_passphrase
              || CHR (9)
              || j.wpa_passphrase
              || CHR (9)
              || j.wps_pin_code
              || CHR (9)
              || j.pppoa_username
              || CHR (9)
              || j.pppoa_passphrase
              || CHR (9)
              || j.tr069_unique_key_64bit
              || CHR (9)
              || j.fon_key
              || CHR (9)
              || j.ta_number
              || CHR (9)
              || j.other_macs
              || CHR (9)
              || j.attribute1
              || CHR (9)
              || j.attribute2
              || CHR (9)
              || j.attribute3
              || CHR (9)
              || j.attribute4
              || CHR (9)
              || j.attribute5
              || CHR (9)
              || j.attribute6
              || CHR (9)
              || j.attribute7
              || CHR (9)
              || j.attribute8
              || CHR (9)
              || j.attribute9
              || CHR (9)
              || j.attribute10
              || CHR (9)
              || j.attribute11
              || CHR (9)
              || j.attribute12
              || CHR (9)
              || j.attribute13
              || CHR (9)
              || j.attribute14
              || CHR (9)
              || j.attribute15
              || CHR (9)
              || j.attribute16
              || CHR (9)
              || j.attribute17
              || CHR (9)
              || j.attribute18
              || CHR (9)
              || j.attribute19
              || CHR (9)
              || j.attribute20
              || CHR (9)
              || j.attribute21
              || CHR (9)
              || j.attribute22
              || CHR (9)
              || j.attribute23
              || CHR (9)
              || j.attribute24
              || CHR (9)
              || j.attribute25
              || CHR (9)
              || j.attribute26
              || CHR (9)
              || j.attribute27
              || CHR (9)
              || j.attribute28
              || CHR (9)
              || j.attribute29
              || CHR (9)
              || j.attribute30;
            UTL_FILE.put_line (fhandle, v_file_data);
          END LOOP;

          IF UTL_FILE.is_open (fhandle)
          THEN
            UTL_FILE.fclose (fhandle);
          END IF;
        ELSIF i.dsv IN ('APL', 'KERRY')
        THEN                                                                      -- 4.2 Added Kerry
          xxntgr_debug_utl.LOG ('*** APL - ');
          v_apl_header := 'UNIQUE_ID' || CHR (9) || v_header;
          v_unique_id := NULL;
          v_po_header_id := NULL;
          v_shipment_header_id := NULL;
          v_reason := NULL;
          v_file_start := 'ASN_FILE_START';
          xxntgr_ptm_common_pkg.xxntgr_get_po_header_id (i.asn_number
                                                        ,i.manufacturer_name
                                                        ,v_po_header_id
                                                        ,v_reason
                                                        );               -- 3.19 Added new parameter
          xxntgr_ptm_common_pkg.xxntgr_get_shipment_header_id (i.asn_number
                                                              ,i.manufacturer_name
                                                              ,v_shipment_header_id
                                                              ,v_reason
                                                              );         -- 3.19 Added new parameter

          IF v_po_header_id IS NOT NULL AND v_shipment_header_id IS NOT NULL
          THEN
            v_unique_id := v_po_header_id || '-' || v_shipment_header_id;

            FOR j IN get_data (i.asn_number, i.manufacturer_name)
            LOOP
              IF NOT v_file_data_exists_counter
              THEN
                fhandle := UTL_FILE.fopen (v_file_loc, v_file_name, 'w', 32767);
                UTL_FILE.put_line (fhandle, v_file_start);
                UTL_FILE.put_line (fhandle, v_apl_header);
                v_file_data_exists_counter := TRUE;
              END IF;

              v_serial_number_count := get_data%ROWCOUNT;
              v_file_data :=
                   v_unique_id
                || CHR (9)
                || j.pallet_id
                || CHR (9)
                || j.master_carton_id
                || CHR (9)
                || j.serial_number
                || CHR (9)
                || j.item_number
                || CHR (9)
                || j.mac_address
                || CHR (9)
                || j.asn_number
                || CHR (9)
                || j.container_number
                || CHR (9)
                || j.po_number
                || CHR (9)
                || j.manufacturer_name
                || CHR (9)
                || j.imei_number
                || CHR (9)
                || j.masterlock_number
                || CHR (9)
                || j.networklock_number
                || CHR (9)
                || j.servicelock_number
                || CHR (9)
                || j.assembly_revision
                || CHR (9)
                || j.assembly_number
                || CHR (9)
                || j.item_revision
                || CHR (9)
                || j.wep_key
                || CHR (9)
                || j.wifi_id
                || CHR (9)
                || j.access_code
                || CHR (9)
                || j.primary_ssid
                || CHR (9)
                || j.wpa_key
                || CHR (9)
                || j.mac_id_cable
                || CHR (9)
                || j.mac_id_emta
                || CHR (9)
                || j.hardware_version
                || CHR (9)
                || j.firmware_version
                || CHR (9)
                || j.ean_code
                || CHR (9)
                || j.software_version
                || CHR (9)
                || j.srm_password
                || CHR (9)
                || j.mac_id_rf
                || CHR (9)
                || j.mac_id_mta
                || CHR (9)
                || j.mac_id_mta_man_router
                || CHR (9)
                || j.mac_id_mta_data
                || CHR (9)
                || j.mac_id_ethernet
                || CHR (9)
                || j.mac_id_usb
                || CHR (9)
                || j.primaryssid_passphrase
                || CHR (9)
                || j.mac_id_cmci
                || CHR (9)
                || j.mac_id_lan
                || CHR (9)
                || j.mac_id_wan
                || CHR (9)
                || j.mac_id_device
                || CHR (9)
                || j.mac_id_wireless
                || CHR (9)
                || j.mac_id_wifi_ssid1
                || CHR (9)
                || j.ssid1
                || CHR (9)
                || j.ssid1_passphrase
                || CHR (9)
                || j.wpa_passphrase
                || CHR (9)
                || j.wps_pin_code
                || CHR (9)
                || j.pppoa_username
                || CHR (9)
                || j.pppoa_passphrase
                || CHR (9)
                || j.tr069_unique_key_64bit
                || CHR (9)
                || j.fon_key
                || CHR (9)
                || j.ta_number
                || CHR (9)
                || j.other_macs
                || CHR (9)
                || j.attribute1
                || CHR (9)
                || j.attribute2
                || CHR (9)
                || j.attribute3
                || CHR (9)
                || j.attribute4
                || CHR (9)
                || j.attribute5
                || CHR (9)
                || j.attribute6
                || CHR (9)
                || j.attribute7
                || CHR (9)
                || j.attribute8
                || CHR (9)
                || j.attribute9
                || CHR (9)
                || j.attribute10
                || CHR (9)
                || j.attribute11
                || CHR (9)
                || j.attribute12
                || CHR (9)
                || j.attribute13
                || CHR (9)
                || j.attribute14
                || CHR (9)
                || j.attribute15
                || CHR (9)
                || j.attribute16
                || CHR (9)
                || j.attribute17
                || CHR (9)
                || j.attribute18
                || CHR (9)
                || j.attribute19
                || CHR (9)
                || j.attribute20
                || CHR (9)
                || j.attribute21
                || CHR (9)
                || j.attribute22
                || CHR (9)
                || j.attribute23
                || CHR (9)
                || j.attribute24
                || CHR (9)
                || j.attribute25
                || CHR (9)
                || j.attribute26
                || CHR (9)
                || j.attribute27
                || CHR (9)
                || j.attribute28
                || CHR (9)
                || j.attribute29
                || CHR (9)
                || j.attribute30;
              UTL_FILE.put_line (fhandle, v_file_data);
            END LOOP;

            IF UTL_FILE.is_open (fhandle)
            THEN
              v_file_stop := 'ASN_FILE_STOP' || CHR (9) || v_serial_number_count;
              UTL_FILE.put_line (fhandle, v_file_stop);
              UTL_FILE.fclose (fhandle);
            END IF;
          END IF;                                                 -- end if po_header_id is not null

          IF v_reason IS NOT NULL
          THEN
            retcode := 1;
            xxntgr_debug_utl.LOG ('Error -' || v_reason);
          END IF;
        END IF;                                                                 -- end if dc = 'apl'

        xxntgr_debug_utl.LOG ('V_FILE_NAME - ' || v_file_name);
        UTL_FILE.fgetattr (v_file_loc, v_file_name, v_file_exists, n_length, n_bsize);

        IF (v_file_exists)
        THEN
          --call netgear copy file program, to copy the file to the ftp/as2 location
          v_request :=
            fnd_request.submit_request
                   (application                  => 'xxntgr'
                   ,program                      => 'xxntgrcpyfile'      --netgear copy file program
                   ,start_time                   => SYSDATE
                   ,sub_request                  => FALSE
                   ,argument1                    => SUBSTR
                                                      (v_file_name
                                                      ,1
                                                      , (INSTR (v_file_name, '.txt') - 1)
                                                      )                --file name without extension
                   ,argument2                    => 'n'                             --request id y/n
                   ,argument3                    => SUBSTR
                                                      (v_file_name
                                                      ,1
                                                      , (INSTR (v_file_name, '.txt') - 1)
                                                      )         --output file name without extension
                   ,argument4                    => 'txt'                     --input file extension
                   ,argument5                    => 'txt'                    --output file extension
                   ,argument6                    => v_file_loc                --input file directory
                   ,argument7                    => v_out_file_loc           --output file directory
                   ,argument8                    => 'y'                      --remove input file y/n
                   );

          IF v_request = 0
          THEN
            xxntgr_debug_utl.error ('copy file program not submited!!!');
          ELSE
            xxntgr_debug_utl.DEBUG ('copy file program submited successfully!!!');
          END IF;
        ELSE
          xxntgr_debug_utl.DEBUG ('copy file program - file does not exists!');
        END IF;                                                                 -- end v_file_exists

        UPDATE xxntgr_ptm_odm_shipment_stg
           SET process_status = 'S'
         WHERE asn_number = i.asn_number
           AND UPPER (manufacturer_name) = i.manufacturer_name
           AND process_status = 'P';

        UPDATE xxntgr_ptm_po_shipment_trx
           SET process_status = 'S'
         WHERE asn_number = i.asn_number AND UPPER (manufacturer_name) = i.manufacturer_name;

        --insert processed data into archive table
        INSERT INTO xxntgr_ptm_odm_shipment_arch
          SELECT *
            FROM xxntgr_ptm_odm_shipment_stg
           WHERE asn_number = i.asn_number
             AND UPPER (manufacturer_name) = i.manufacturer_name
             AND process_status = 'S';

        --delete processed data from staging table after archive
        DELETE FROM xxntgr_ptm_odm_shipment_stg
              WHERE asn_number = i.asn_number
                AND UPPER (manufacturer_name) = i.manufacturer_name
                AND process_status = 'S';

        --commit;
        IF v_asn_count IS NULL
        THEN
          xxntgr_debug_utl.warn ('No data found to write...');
          retcode := 1;
        END IF;

        xxntgr_debug_utl.LOG ('Number of ASN files created: ' || v_asn_count || '-' || i.asn_number);
      EXCEPTION
        WHEN OTHERS
        THEN
          --rollback;
          v_err_msg := 'Error in file generation program: ' || SQLCODE || SQLERRM;
          raise_application_error (-20002, v_err_msg);
          xxntgr_debug_utl.error (v_err_msg);
          retcode := 2;

          BEGIN
            SELECT description
              INTO v_mailid
              FROM fnd_lookup_values
             WHERE lookup_code = 'ODM_SHIPMENT3' AND enabled_flag = 'Y';

            xxntgr_utl_mail.send (p_sender                     => 'donotreply@netgear.com'
                                 ,p_recipients                 => v_mailid
                                 ,p_cc                         => NULL
                                 ,p_bcc                        => NULL
                                 ,p_subject                    => 'ODM shipment - FTP to DC failed'
                                 ,p_message                    => SQLCODE || SQLERRM
                                 ,p_mime_type                  => 'text/html; charset=us-ascii'
                                 );
          EXCEPTION
            WHEN OTHERS
            THEN
              xxntgr_debug_utl.error ('Error while sending mail: ' || SQLCODE || SQLERRM);
          END;

          retcode := 2;
      END;

      IF v_err_msg IS NOT NULL
      THEN
        EXIT;
      END IF;
    END LOOP;

    -- the insert below archives the data even if dc is not enabled.
    COMMIT;
  --insert processed data into archive table
  /*
  insert into xxntgr_ptm_odm_shipment_arch
  select * from xxntgr_ptm_odm_shipment_stg
  where process_status = 'p';
  */

  -- delete processed data from staging table after archive
  /*
  delete from xxntgr_ptm_odm_shipment_stg
  where process_status = 'p';
  commit;
  */
  END xxntgr_ptm_dc_gensnfile;

------------------------------------
-- PROCEDURE: XXNTGR_PTM_DC_SHIP ---
------------------------------------
  PROCEDURE xxntgr_ptm_dc_ship (
    errbuf                      OUT      VARCHAR2
   ,retcode                     OUT      VARCHAR2
   ,p_dc_name                   IN       VARCHAR2
   ,p_input_file_id             IN       NUMBER
   ,p_delivery_id               IN       VARCHAR2
   ,p_ignr_dup_sn                        VARCHAR2
   ,p_match_to_delivery                  VARCHAR2
   ,p_delete_delivery                    VARCHAR2
  )
  AS
-- 4.6 Cursor to get the individual files with multiple Delivery IDs in it
    CURSOR get_files_mul_delivery_ids
    IS
      SELECT   input_file_id, file_name, UPPER (dc_name) dc_name, COUNT (DISTINCT delivery_id) cnt
          FROM xxntgr_ptm_dc_shipment_stg
         WHERE (processed_flag = 'N' OR processed_flag IS NULL)
           AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1))
           AND NVL (delivery_id, 1) = NVL (p_delivery_id, NVL (delivery_id, 1))
           AND UPPER (dc_name) = UPPER (p_dc_name)
      GROUP BY input_file_id, file_name, dc_name
        HAVING COUNT (DISTINCT delivery_id) > 1;

--To get distinct delivery_id, for mandatory field validation
    CURSOR get_dist_delivery
    IS
      SELECT DISTINCT delivery_id, UPPER (dc_name) dc_name, file_name, input_file_id
                     ,MAX (creation_date) creation_date
                 -- added input_file_id,creation_date 4.1 -- 4.6 added max(creation_date)
      FROM            xxntgr_ptm_dc_shipment_stg
                WHERE (processed_flag = 'N' OR processed_flag IS NULL)
                  AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1))
                  AND NVL (delivery_id, 1) = NVL (p_delivery_id, NVL (delivery_id, 1))
                  AND UPPER (dc_name) = UPPER (p_dc_name)
                  AND UPPER (dc_name) <> 'APL'
             GROUP BY delivery_id, dc_name, file_name, input_file_id;

--get mandatory columns to validate if they are blank
    CURSOR validate_fields (
      p_delivery                           NUMBER
     ,p_dc_name                            VARCHAR2
    )
    IS
      SELECT record_row_number, pallet_id, master_carton_id, item_number, dc_name, serial_number
            ,delivery_id, delivery_detail_id
        FROM xxntgr_ptm_dc_shipment_stg
       WHERE delivery_id = p_delivery
         AND NVL (UPPER (dc_name), '1') = NVL (UPPER (p_dc_name), '1')
         AND (processed_flag = 'N' OR processed_flag IS NULL);

--
--get DELIVERY to process
    CURSOR get_delivery
    IS
      SELECT DISTINCT delivery_id, UPPER (dc_name), file_name, input_file_id
                 FROM xxntgr_ptm_dc_shipment_stg
                WHERE (processed_flag = 'N' OR processed_flag IS NULL)
                  AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1))
                  AND NVL (delivery_id, 1) = NVL (p_delivery_id, NVL (delivery_id, 1))
                  AND UPPER (dc_name) = UPPER (p_dc_name)
                  AND UPPER (dc_name) <> 'APL'
             GROUP BY delivery_id, UPPER (dc_name), file_name, input_file_id
             ORDER BY delivery_id;

--to validate serial number for duplicacy and set process_mode
    CURSOR set_process_mode (
      p_delivery                           VARCHAR2
     ,p_dc                                 VARCHAR2
    )
    IS
      SELECT delivery_id, serial_number, dc_name, creation_date
        FROM xxntgr_ptm_dc_shipment_stg
       WHERE delivery_id = p_delivery
         AND UPPER (dc_name) = UPPER (p_dc)
         AND (processed_flag = 'N' OR processed_flag IS NULL);

--Validate quantity_shipped against shipment and item
    CURSOR validate_shipped_qty (
      p_delivery                           VARCHAR2
     ,p_dc                                 VARCHAR2
    )
    IS
      SELECT DISTINCT delivery_id, delivery_detail_id, item_number
                 FROM xxntgr_ptm_dc_shipment_stg
                WHERE delivery_id = p_delivery
                  AND UPPER (dc_name) = UPPER (p_dc)
                  AND item_number NOT IN (
                        SELECT msib.segment1                 --added NOT IN condition by ppatil 3.16
                          FROM fnd_lookup_values flv, apps.mtl_system_items_b msib
                         WHERE flv.lookup_type = 'XXNTGR_PTM_DC_EXCLUDE_ITEM'
                           AND NVL (flv.enabled_flag, 'N') = 'Y'
                           AND TRUNC (SYSDATE) BETWEEN TRUNC (NVL (flv.start_date_active, SYSDATE))
                                                   AND TRUNC (NVL (flv.end_date_active, SYSDATE))
                           AND msib.organization_id = 488
                           AND msib.segment1 = flv.lookup_code
                           AND NVL (flv.tag, 'N') = 'Y')
                  AND processed_flag <> 'P';

--get items to validate
    CURSOR validate_items (
      p_delivery                           VARCHAR2
     ,p_dc                                 VARCHAR2
    )
    IS
      SELECT DISTINCT pdss.delivery_id, pdss.delivery_detail_id, pdss.item_number
                     ,msi.inventory_item_id
                 FROM xxntgr_ptm_dc_shipment_stg pdss, mtl_system_items_b msi
                WHERE pdss.item_number = msi.segment1
                  AND msi.organization_id = 488
                  AND delivery_id = p_delivery
                  AND UPPER (dc_name) = UPPER (p_dc)
                  AND processed_flag <> 'P';

--get the duplicate Serial numbers
    CURSOR get_duplicate_sn (
      p_delivery                           VARCHAR2
     ,p_dc                                 VARCHAR2
    )
    IS
      SELECT   serial_number
          FROM xxntgr_ptm_dc_shipment_stg
         WHERE delivery_id = p_delivery AND UPPER (dc_name) = UPPER (p_dc) AND processed_flag <> 'P'
      GROUP BY serial_number
        HAVING COUNT (serial_number) > 1;

--to get any missing item number for delivery, from pallet file
/*CURSOR missed_delivery_line(p_delivery VARCHAR2,p_dc VARCHAR2) IS
SELECT DISTINCT da.delivery_detail_id
FROM wsh_delivery_details dd,wsh_new_deliveries nd,WSH_DELIVERY_ASSIGNMENTS da
WHERE da.delivery_id=p_delivery
AND nd.delivery_id=da.delivery_id
AND dd.delivery_detail_id=da.delivery_detail_id
MINUS
SELECT DISTINCT delivery_detail_id
FROM
XXNTGR_PTM_DC_SHIPMENT_STG
WHERE delivery_id = p_delivery
AND UPPER(dc_name) = UPPER(p_dc)
AND PROCESSED_FLAG <> 'P';*/

    /*---------added by ppatil-----to fetch item_id-----*/
    CURSOR missed_delivery_line (
      p_delivery                           VARCHAR2
     ,p_dc                                 VARCHAR2
    )
    IS
      SELECT DISTINCT da.delivery_detail_id, dd.inventory_item_id
                 FROM wsh_delivery_details dd, wsh_new_deliveries nd, wsh_delivery_assignments da
                WHERE da.delivery_id = p_delivery
                  AND nd.delivery_id = da.delivery_id
                  AND dd.delivery_detail_id = da.delivery_detail_id
                  AND dd.inventory_item_id NOT IN (
                        SELECT msib.inventory_item_id        --added NOT IN condition by ppatil 3.15
                          FROM fnd_lookup_values flv, apps.mtl_system_items_b msib
                         WHERE flv.lookup_type = 'XXNTGR_PTM_DC_EXCLUDE_ITEM'
                           AND NVL (flv.enabled_flag, 'N') = 'Y'
                           AND TRUNC (SYSDATE) BETWEEN TRUNC (NVL (flv.start_date_active, SYSDATE))
                                                   AND TRUNC (NVL (flv.end_date_active, SYSDATE))
                           AND msib.organization_id = 488
                           AND msib.segment1 = flv.lookup_code
                           AND NVL (flv.tag, 'N') = 'N'             --added condition by ppatil 3.16
                                                       )
                  --AND EXISTS (SELECT 1 FROM xxntgr_ptm_item_instances xpii WHERE xpii.inventory_item_id = dd.inventory_item_id) -- mmore 3.8 --Commented  4.0
                  --Added Lookup Values query insted of xxntgr_ptm_item_instances table for SKU Serialization items 4.0
                  AND EXISTS (
                        SELECT ffv.flex_value
                          FROM fnd_flex_values_vl ffv, fnd_flex_value_sets ffs
                         WHERE 1 = 1
                           AND ffv.description = TO_CHAR (dd.inventory_item_id)
                           AND ffv.flex_value_set_id = ffs.flex_value_set_id
                           AND ffs.flex_value_set_name = 'XXNTGR_INCLUDE_SERIAL_FLAG'
                           --Added for 4.5 Start
                           AND NVL (ffv.enabled_flag, 'N') = 'Y'
                           AND TRUNC (SYSDATE) BETWEEN TRUNC (NVL (ffv.start_date_active, SYSDATE))
                                                   AND TRUNC (NVL (ffv.end_date_active, SYSDATE))
                                                                                                 -- End 4.5
                      )
      --END 4.0
      MINUS
      SELECT DISTINCT delivery_detail_id, m.inventory_item_id
                 FROM xxntgr_ptm_dc_shipment_stg s, mtl_system_items_b m
                WHERE delivery_id = p_delivery
                  AND m.segment1 = s.item_number
                  AND UPPER (dc_name) = UPPER (p_dc)
                  AND NVL (processed_flag, 'N') <> 'P';                 ----added NVL by ppatil 3.15

/*------end ppatil--------------------------*/
---
    CURSOR get_dc_data (
      p_delivery                           VARCHAR2
     ,p_dc                                 VARCHAR2
    )
    IS
      SELECT *
        FROM xxntgr_ptm_dc_shipment_stg
       WHERE delivery_id = p_delivery AND UPPER (dc_name) = UPPER (p_dc) AND processed_flag = 'P';

    TYPE ptm_dc_ship IS TABLE OF get_dc_data%ROWTYPE;

    dc_ship                       ptm_dc_ship;

---Version 3.18 Start
    CURSOR cur_pal_cart (
      p_delivery_id                        NUMBER
    )
    IS
      SELECT DISTINCT stg.item_number, stg.pallet_id, stg.master_carton_id
                     ,NVL (cpc_ship.acct_site_use_id, cpc_bill.acct_site_use_id) site_use_id
                 FROM xxntgr_ptm_dc_shipment_stg stg
                     ,apps.wsh_new_deliveries wnd
                     ,apps.oe_order_headers_all ooh
                     ,xxntgr_apps.xxntgr_ptm_cust_pallet_carton cpc_ship
                     ,xxntgr_apps.xxntgr_ptm_cust_pallet_carton cpc_bill
                WHERE stg.delivery_id = p_delivery_id
                  AND wnd.delivery_id = stg.delivery_id
                  AND ooh.header_id = wnd.source_header_id
                  AND cpc_ship.acct_site_use_id(+) = ooh.ship_to_org_id
                  AND cpc_ship.site_use_code(+) = 'SHIP_TO'
                  AND cpc_bill.acct_site_use_id(+) = ooh.invoice_to_org_id
                  AND cpc_bill.site_use_code(+) = 'BILL_TO';

    ---Version 3.18 End

    ---Start 4.7
    CURSOR cur_dist_item (
      p_delivery_id                        NUMBER
    )
    IS
      SELECT DISTINCT stg.item_number
                 FROM xxntgr_ptm_dc_shipment_stg stg
                WHERE stg.delivery_id = p_delivery_id;

    ---End 4.7

    -----
    v_user_id                     NUMBER;
    v_errmsg                      VARCHAR2 (1000);
    v_delivery                    NUMBER;
    v_deliveryid                  NUMBER;
    v_delivery_id                 NUMBER;
    v_delv_id                     NUMBER;
    v_delv_dt_id                  NUMBER;
    v_delv_line_id                NUMBER;
    v_cnt_delv_line               NUMBER;
    v_invalid_delivery            VARCHAR2 (100);
    v_invalid_dc                  VARCHAR2 (100);
    v_dc_name                     VARCHAR2 (60);
    v_dc                          VARCHAR2 (60);
    v_duplicate_del_sn            VARCHAR2 (100);
    v_dup_del_sn                  NUMBER;
    v_valid_item                  NUMBER;
    v_item_num                    VARCHAR2 (60);
    v_missed_line                 NUMBER;
    v_item_id                     NUMBER;
    v_item_no                     VARCHAR2 (40);
    v_quantity_shipped            NUMBER;
    v_quantity_picked             NUMBER;                                          --added by ppatil
    v_confirm_date                DATE;                                            --added by ppatil
    v_unprocess_flag              VARCHAR2 (4);                          -- Added by Subbareddy 3.17
    v_instance_id                 NUMBER;
    v_inventory_item_id           NUMBER;
    v_organization_id             NUMBER;
    v_trans_type                  VARCHAR2 (100);
    v_proc_status                 VARCHAR2 (10);
    v_reqstid                     NUMBER;
    v_duplicate_sn                VARCHAR2 (60);
    v_validate_fields             VARCHAR2 (32000);
    v_chk_duplicate_sn            VARCHAR2 (60);
    v_process_mode                VARCHAR2 (10);
    v_inst_id                     NUMBER;
    v_reason                      VARCHAR2 (32000);
    v_get_mail_error              VARCHAR2 (32000);
    v_file_name                   VARCHAR2 (100);
    v_rec_deleted                 NUMBER;
    v_aircard_item_exists         VARCHAR2 (1);
    v_log_err_msg                 VARCHAR2 (32000) := NULL;                              --mmore 2.8
    v_file_prsd                   VARCHAR2 (32000) := NULL;                              --mmore 2.8
    v_item_name                   VARCHAR2 (40);                                         --mmore 3.8
    v_err_loc                     VARCHAR2 (40);                                         --mmore 3.9
    --skonanki 3.18 start
    v_out_pal_flag                VARCHAR2 (1);
    v_out_pal_msg                 VARCHAR2 (4000);
    v_out_cart_flag               VARCHAR2 (1);
    v_out_cart_msg                VARCHAR2 (4000);
    exp_pallet_raise_error        EXCEPTION;
    exp_carton_raise_error        EXCEPTION;
    --skonanki 3.18 End
    -----SKONANKI 4.1
    p_header_message              VARCHAR2 (4000);
    v_processed_flag              VARCHAR2 (25);
    lv_process_message            VARCHAR2 (4000);
    gv_user_id                    NUMBER DEFAULT fnd_profile.VALUE ('USER_ID');
    l_header_error_flag           VARCHAR2 (10);
    l_sfs_flag_count              NUMBER;
    l_p_flag_count                NUMBER;
    l_sfs_process_flag            VARCHAR2 (10);
    l_p_process_flag              VARCHAR2 (10);
    l_err_count                   NUMBER;
    v_validate_header             VARCHAR2 (32000);
    v_input_file_id               NUMBER;
------SKONANKI 4.1
--Start 4.7
    l_product_line                VARCHAR2 (40);
    l_product_category            VARCHAR2 (40);
    l_product_family              VARCHAR2 (40);
    l_sn_type                     VARCHAR2 (10);
    l_sn_err_meg                  VARCHAR2 (4000);

    TYPE g_varchar_tbl IS TABLE OF VARCHAR2 (50)
      INDEX BY VARCHAR2 (50);

    gvt_sn_item                   g_varchar_tbl;

    TYPE g_outb_sn_tbl IS TABLE OF VARCHAR2 (1)
      INDEX BY VARCHAR2 (50);                                                   -- 5.1 Added a table

    outb_sn_num_tbl               g_outb_sn_tbl;                       -- 5.1 Variable of table type
    l_outb_sn_flag                VARCHAR2 (1);                     -- 5.1 Variable to hold the flag
    l_serial_count                NUMBER;
    v_sn_error                    VARCHAR2 (32000);                                            --5.0
  ---End 4.7
  BEGIN                                                                                          --1
    v_reqstid := fnd_global.conc_request_id;
    xxntgr_debug_utl.DEBUG ('CONC REQUEST ID : ' || v_reqstid);

    --mmore start 2.8
    BEGIN
      IF p_delete_delivery = 'Y'
      THEN
        v_file_prsd := '\n\tDelete data for Delivery: ' || p_delivery_id;
      ELSE
        FOR i IN (SELECT DISTINCT input_file_id, delivery_id
                             FROM xxntgr_ptm_dc_shipment_stg
                            WHERE (processed_flag = 'N' OR processed_flag IS NULL)
                              AND NVL (input_file_id, 1) =
                                                       NVL (p_input_file_id, NVL (input_file_id, 1))
                              AND NVL (delivery_id, 1) = NVL (p_delivery_id, NVL (delivery_id, 1))
                              AND UPPER (dc_name) = UPPER (p_dc_name)
                              AND UPPER (dc_name) <> 'APL'
                         GROUP BY input_file_id, delivery_id)
        LOOP
          v_file_prsd := v_file_prsd || i.input_file_id || ' (' || i.delivery_id || ') , ';
        END LOOP;

        IF v_file_prsd IS NOT NULL
        THEN
          v_file_prsd := SUBSTR (v_file_prsd, 1, LENGTH (v_file_prsd) - 2);
          v_file_prsd := '\n\tProcessed File ID (Delivery ID): ' || v_file_prsd;
        END IF;
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;

    --mmore end 2.8
    IF p_delete_delivery = 'Y'
    THEN
      IF p_delivery_id IS NOT NULL
      THEN
        --Delete records from master tables, and exit
        BEGIN
          DELETE FROM xxntgr_ptm_trx_register
                WHERE transaction_source_header_id IN (
                        SELECT trx.so_shipment_trx_id
                          FROM xxntgr_ptm_item_instances ins
                              ,xxntgr_ptm_so_shipment_trx trx
                              ,xxntgr_ptm_trx_register reg
                         WHERE ins.instance_id = trx.instance_id
                           AND trx.instance_id = reg.instance_id
                           AND trx.so_shipment_trx_id = reg.transaction_source_header_id
                           AND reg.transaction_type = '04'
                           AND trx.delivery_id = p_delivery_id
                           AND NVL (trx.input_file_id, 1) =
                                                   NVL (p_input_file_id, NVL (trx.input_file_id, 1)));

          BEGIN
            DELETE FROM xxntgr_ptm_so_shipment_trx
                  WHERE delivery_id = p_delivery_id
                    AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1));

            v_rec_deleted := SQL%ROWCOUNT;
            xxntgr_debug_utl.DEBUG ('Total rows deleted:' || v_rec_deleted);
          END;

          DELETE FROM xxntgr_ptm_dc_shipment_stg
                WHERE delivery_id = p_delivery_id
                  AND NVL (input_file_id, 1) = NVL (p_input_file_id, NVL (input_file_id, 1));

          IF v_rec_deleted <> 0
          THEN
            xxntgr_debug_utl.DEBUG (   'All the records for Delivery_id# '
                                    || p_delivery_id
                                    || ' and Batch# '
                                    || p_input_file_id
                                    || ' are removed from staging and master tables.'
                                   );
          END IF;
        EXCEPTION
          WHEN OTHERS
          THEN
            retcode := 1;
            xxntgr_debug_utl.error (   'Error while deleting the records for delivery#'
                                    || p_delivery_id
                                    || ' '
                                    || SQLCODE
                                    || SQLERRM
                                   );
        END;
      END IF;
    ELSE
      -- 4.6 Validate if the file contains multiple Delivery IDs
      BEGIN
        FOR file_multiple_deliv IN get_files_mul_delivery_ids
        LOOP
          v_validate_header := 'File Contains Multiple Delivery IDs';
          fnd_file.put_line (fnd_file.LOG
                            , 'Multiple Delivery File ID:' || file_multiple_deliv.input_file_id
                            );

          INSERT INTO xxntgr_apps.xxntgr_ptm_dc_file_header
                      (input_file_id, file_name, file_type
                      ,partner_id, process_flag, ack_flag, process_message, request_id
                      ,created_by, creation_date, last_updated_by, last_update_date
                      )
               VALUES (file_multiple_deliv.input_file_id, file_multiple_deliv.file_name, 'PTMDCI'
                      ,1010, 'E', 'N', v_validate_header, fnd_global.conc_request_id
                      ,fnd_global.user_id, SYSDATE, fnd_global.user_id, SYSDATE
                      );

          fnd_file.put_line (fnd_file.LOG, 'Inserted Header');

          BEGIN
            xxntgr_ptm_odm_pkg.ptm_dc_ship_mail (NULL
                                                ,file_multiple_deliv.dc_name
                                                ,file_multiple_deliv.file_name
                                                ,v_validate_header
                                                ,v_get_mail_error
                                                );
            xxntgr_debug_utl.error (v_get_mail_error);
          EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
              NULL;
            WHEN OTHERS
            THEN
              NULL;
          END;

          fnd_file.put_line (fnd_file.LOG, 'Sent Email');

          UPDATE xxntgr_ptm_dc_shipment_stg
             SET processed_flag = 'E'
                ,process_message =
                                 SUBSTR (process_message || ' ' || 'MULTIPLE_DELIVERY_IDS', 1, 4000)
           WHERE input_file_id = file_multiple_deliv.input_file_id
             AND NVL (UPPER (dc_name), '1') = NVL (UPPER (file_multiple_deliv.dc_name), '1');

          --Insert error data into archive table
          INSERT INTO xxntgr_ptm_dc_shipment_arch
            SELECT *
              FROM xxntgr_ptm_dc_shipment_stg
             WHERE input_file_id = file_multiple_deliv.input_file_id
               AND NVL (UPPER (dc_name), '1') = NVL (UPPER (file_multiple_deliv.dc_name), '1');

          --Delete error data from staging table
          DELETE FROM xxntgr_ptm_dc_shipment_stg
                WHERE input_file_id = file_multiple_deliv.input_file_id
                  AND NVL (UPPER (dc_name), '1') = NVL (UPPER (file_multiple_deliv.dc_name), '1');

          COMMIT;
        END LOOP;
      EXCEPTION
        WHEN OTHERS
        THEN
          xxntgr_debug_utl.error (   'Error while getting files with multiple Delivery IDs '
                                  || SQLCODE
                                  || SQLERRM
                                 );
      END;

      -- End 4.6

      --Start field level validation
      BEGIN
        FOR dist_del IN get_dist_delivery
        LOOP
          --- Start 4.1
          v_validate_header := NULL;
          v_validate_fields := NULL;
          -- Calling INSERT_DC_HEADER procedure to insert data into table xxntgr_ptm_dc_file_header
          p_header_message := NULL;
          xxntgr_ptm_common_pkg.insert_dc_header (dist_del.input_file_id
                                                 ,dist_del.delivery_id
                                                 ,dist_del.file_name
                                                 ,dist_del.dc_name
                                                 ,gv_user_id
                                                 ,dist_del.creation_date
                                                 ,p_header_message
                                                 );

          IF p_header_message IS NOT NULL
          THEN
            v_validate_header :=
                        SUBSTR (' Error in INSERT_DC_HEADER ' || p_header_message || ').', 1, 4000);
            fnd_file.put_line (fnd_file.LOG, v_validate_header);
          -- RAISE exp_main_raise_error;
          END IF;

          --- End 4.1
          ----Start 4.7
          FOR rec_cur_dist_item IN cur_dist_item (dist_del.delivery_id)
          LOOP
            l_product_line := NULL;
            l_product_category := NULL;
            l_product_family := NULL;
            l_sn_type := NULL;

            BEGIN
              SELECT mc.segment1, mc.segment2, mc.segment3
                INTO l_product_line, l_product_category, l_product_family
                FROM mtl_system_items_b msi
                    ,mtl_item_categories mic
                    ,mtl_categories mc
                    ,mtl_category_sets mcs
               WHERE msi.organization_id = mic.organization_id
                 AND msi.inventory_item_id = mic.inventory_item_id
                 AND mic.category_id = mc.category_id
                 AND mic.category_set_id = mcs.category_set_id
                 AND msi.organization_id = 488
                 AND msi.segment1 = rec_cur_dist_item.item_number
                 AND mcs.description = 'NG Line | Category | Family';
            EXCEPTION
              WHEN OTHERS
              THEN
                v_log_err_msg := '\n\tError: WO12 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
                --  v_validate_fields := SUBSTR('Error While getting item details for item# '||rec_cur_dist_item.item_number||' In delivery ID '||dist_del.delivery_id,1,4000);
                fnd_file.put_line (fnd_file.LOG, v_log_err_msg || SQLERRM);
            END;

            BEGIN
              SELECT COALESCE ((SELECT serial_number_type
                                  FROM xxntgr_apps.xxntgr_ptm_item_cfg
                                 WHERE 1 = 1 AND item_number = rec_cur_dist_item.item_number)
                              , (SELECT serial_number_type
                                   FROM xxntgr_apps.xxntgr_ptm_item_cfg
                                  WHERE 1 = 1 AND product_family = l_product_family)
                              , (SELECT serial_number_type
                                   FROM xxntgr_apps.xxntgr_ptm_item_cfg
                                  WHERE 1 = 1 AND product_category = l_product_category)
                              , (SELECT serial_number_type
                                   FROM xxntgr_apps.xxntgr_ptm_item_cfg
                                  WHERE 1 = 1 AND product_line = l_product_line)
                              ,'A'
                              )
                INTO l_sn_type
                FROM DUAL;
            EXCEPTION
              WHEN OTHERS
              THEN
                fnd_file.put_line (fnd_file.LOG
                                  ,    'Error While getting SN type for item '
                                    || rec_cur_dist_item.item_number
                                    || ' In delivery ID '
                                    || v_delivery_id
                                    || ' - '
                                    || SQLERRM
                                  );
                l_sn_type := 'X';
            END;

            --- Storing SN type for the items in table type variable
            gvt_sn_item (rec_cur_dist_item.item_number) := l_sn_type;

            -- 5.1 Check if outbound serial numbers must be captured for this item
            BEGIN
              SELECT 'Y'
                INTO l_outb_sn_flag
                FROM apps.fnd_lookup_values
               WHERE lookup_type = 'XXNTGR_PTM_OUTB_SN_SKU_PREFIX'
                 AND enabled_flag = 'Y'
                 AND SYSDATE BETWEEN NVL (start_date_active, SYSDATE) AND NVL (end_date_active
                                                                              ,SYSDATE
                                                                              )
                 AND rec_cur_dist_item.item_number LIKE lookup_code || '%';
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                l_outb_sn_flag := 'N';
            END;

            outb_sn_num_tbl (rec_cur_dist_item.item_number) := l_outb_sn_flag;
          -- End 5.1
          END LOOP;

          ---End 4.7
          FOR i IN validate_fields (dist_del.delivery_id, dist_del.dc_name)
          LOOP
--      v_validate_fields := NULL;

            --validate pallet id if it is null
            -- GP commented out pallet_id validation as it is not a required filed - 11-JUL-12
            /*
            IF i.pallet_id IS NULL  THEN
                v_validate_fields := ' Pallet ID cannot be blank (row# '||i.RECORD_ROW_NUMBER||').';

                UPDATE XXNTGR_PTM_DC_SHIPMENT_STG
                SET PROCESSED_FLAG = 'E', process_message = SUBSTR(process_message||' '||'PALLET_ID_NULL',1,4000)
                WHERE delivery_id = dist_del.delivery_id
                AND nvl(upper(dc_name),'1') = nvl(upper(dist_del.dc_name),'1')
                AND PROCESSED_FLAG <> 'P';

            ELSE
                null;
            END IF;
            */
            --validate master_carton_id if it is null
            IF i.master_carton_id IS NULL
            THEN
              v_validate_fields :=
                                 ' Carton ID cannot be blank (row# ' || i.record_row_number || ').';

              UPDATE xxntgr_ptm_dc_shipment_stg
                 SET processed_flag = 'E'
                    ,process_message = SUBSTR (process_message || ' ' || 'CARTON_NULL', 1, 4000)
               WHERE delivery_id = dist_del.delivery_id
                 AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                 AND processed_flag <> 'P';
            ELSE
              NULL;
            END IF;

            --validate item_number if it is null
            IF i.item_number IS NULL
            THEN
              v_validate_fields :=
                SUBSTR (   v_validate_fields
                        || '<br>'
                        || ' SKU cannot be blank (row# '
                        || i.record_row_number
                        || ').'
                       ,1
                       ,4000
                       );

              UPDATE xxntgr_ptm_dc_shipment_stg
                 SET processed_flag = 'E'
                    ,process_message = SUBSTR (process_message || ' ' || 'SKU_NULL', 1, 4000)
               WHERE delivery_id = dist_del.delivery_id
                 AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                 AND processed_flag <> 'P';
            ELSE
              NULL;
            END IF;

            --validate dc_name if it is null
            IF i.dc_name IS NULL
            THEN
              v_validate_fields :=
                SUBSTR (   v_validate_fields
                        || '<br>'
                        || ' DC_Name cannot be blank (row# '
                        || i.record_row_number
                        || ').'
                       ,1
                       ,4000
                       );

              UPDATE xxntgr_ptm_dc_shipment_stg
                 SET processed_flag = 'E'
                    ,process_message = SUBSTR (process_message || ' ' || 'SKU_NULL', 1, 4000)
               WHERE delivery_id = dist_del.delivery_id
                 AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                 AND processed_flag <> 'P';
            ELSE
              NULL;
            END IF;

            --validate serial_number if it is null
            IF i.serial_number IS NULL
            THEN
              v_validate_fields :=
                SUBSTR (   v_validate_fields
                        || '<br>'
                        || ' Serial Number cannot be blank (row# '
                        || i.record_row_number
                        || ').'
                       ,1
                       ,4000
                       );

              UPDATE xxntgr_ptm_dc_shipment_stg
                 SET processed_flag = 'E'
                    ,process_message = SUBSTR (process_message || ' ' || 'SN_NULL', 1, 4000)
               WHERE delivery_id = dist_del.delivery_id
                 AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                 AND processed_flag <> 'P';
            ELSE
              ---start 4.7
              l_serial_count := '0';

              SELECT COUNT (serial_number)
                INTO l_serial_count
                FROM xxntgr_ptm_item_instances
               WHERE serial_number = i.serial_number AND end_date_active IS NULL;

              IF l_serial_count = '0' AND outb_sn_num_tbl (i.item_number) = 'N'
              THEN                   -- 5.1 Added the condition to skip the serial number validation
                BEGIN
                  l_sn_type := NULL;
                  l_sn_err_meg := NULL;
                  -- getting Serial Number type from table type record
                  l_sn_type := gvt_sn_item (i.item_number);

                  IF l_sn_type NOT IN ('X')
                  THEN
                    xxntgr_ptm_common_pkg.validate_serial_number (i.serial_number
                                                                 ,l_sn_type
                                                                 ,'DC'
                                                                 ,l_sn_err_meg
                                                                 );
                  END IF;

                  IF l_sn_err_meg IS NOT NULL
                  THEN
                    v_validate_fields :=
                      SUBSTR (   v_validate_fields
                              || '<br>'
                              || ' Incorrect Serial Number Format: '
                              || i.serial_number
                             ,1
                             ,4000
                             );

                    UPDATE xxntgr_ptm_dc_shipment_stg
                       SET process_message =
                                       SUBSTR (process_message || ' ' || 'SN_WRONG_FORMAT', 1, 4000)
                     WHERE delivery_id = dist_del.delivery_id
                       AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                       AND serial_number = i.serial_number
                       AND processed_flag <> 'P';

                    UPDATE xxntgr_ptm_dc_shipment_stg
                       SET processed_flag = 'E'
                     WHERE delivery_id = dist_del.delivery_id
                       AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                       AND processed_flag <> 'P';
                  END IF;
                END;
              END IF;
            ---End 4.7

            -- NULL;  Commented for 4.7
            END IF;

            --validate delivery_id if it is null
            IF i.delivery_id IS NULL
            THEN
              v_validate_fields :=
                SUBSTR (   v_validate_fields
                        || '<br>'
                        || ' Delivery id cannot be blank (row# '
                        || i.record_row_number
                        || ').'
                       ,1
                       ,4000
                       );

              UPDATE xxntgr_ptm_dc_shipment_stg
                 SET processed_flag = 'E'
                    ,process_message = SUBSTR (process_message || ' ' || 'DELIVERY_NULL', 1, 4000)
               WHERE delivery_id = dist_del.delivery_id
                 AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                 AND processed_flag <> 'P';
            ELSE
              NULL;
            END IF;

            --validate delivery_detail_id if it is null
            IF i.delivery_detail_id IS NULL
            THEN
              v_validate_fields :=
                SUBSTR (   v_validate_fields
                        || '<br>'
                        || ' Delivery detail id cannot be blank (row# '
                        || i.record_row_number
                        || ').'
                       ,1
                       ,4000
                       );

              UPDATE xxntgr_ptm_dc_shipment_stg
                 SET processed_flag = 'E'
                    ,process_message =
                                    SUBSTR (process_message || ' ' || 'DELIVERY_LINE_NULL', 1, 4000)
               WHERE delivery_id = dist_del.delivery_id
                 AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                 AND processed_flag <> 'P';
            ELSE
              NULL;
            END IF;

            IF v_validate_fields IS NOT NULL
            THEN
              -- If serial number format is not correct, validate the next serial number
              IF l_sn_err_meg IS NOT NULL
              THEN
                CONTINUE;
              END IF;

              -- Else exit the loop
              EXIT;
            END IF;
          END LOOP;

          ---3.18 Start SKONANKI
          IF v_validate_fields IS NULL
          THEN
            fnd_file.put_line
                        (fnd_file.LOG
                        ,'************************************************************************'
                        );
            fnd_file.put_line (fnd_file.LOG
                              ,    'Starting Pallet and Carton Validation for Delivery ID:'
                                || dist_del.delivery_id
                              );
            fnd_file.put_line
                         (fnd_file.LOG
                         ,'************************************************************************'
                         );

            FOR rec_pal_cart IN cur_pal_cart (dist_del.delivery_id)
            LOOP
              BEGIN
                IF rec_pal_cart.site_use_id IS NOT NULL
                THEN
                  -- call the procedure to validate pallet
                  xxntgr_ptm_common_pkg.validate_pallet (rec_pal_cart.site_use_id
                                                        ,rec_pal_cart.pallet_id
                                                        ,v_out_pal_flag
                                                        ,v_out_pal_msg
                                                        );

                  IF v_out_pal_flag = 'E'
                  THEN
                    v_validate_fields :=
                      SUBSTR (   v_validate_fields
                              || '<br>'
                              || ' PALLET_ID_INVALID (Pallet ID# '
                              || rec_pal_cart.pallet_id
                              || ').'
                             ,1
                             ,4000
                             );
                    RAISE exp_pallet_raise_error;
                  END IF;

                  -- call the procedure to validate carton
                  xxntgr_ptm_common_pkg.validate_carton (rec_pal_cart.site_use_id
                                                        ,rec_pal_cart.master_carton_id
                                                        ,v_out_cart_flag
                                                        ,v_out_cart_msg
                                                        );

                  IF v_out_cart_flag = 'E'
                  THEN
                    v_validate_fields :=
                      SUBSTR (   v_validate_fields
                              || '<br>'
                              || ' CARTON_ID_INVALID (Carton ID# '
                              || rec_pal_cart.master_carton_id
                              || ').'
                             ,1
                             ,4000
                             );
                    RAISE exp_carton_raise_error;
                  END IF;
                END IF;
              EXCEPTION
                WHEN exp_pallet_raise_error
                THEN
                  fnd_file.put_line (fnd_file.LOG, 'Pallet Invalid:' || rec_pal_cart.pallet_id);

                  UPDATE xxntgr_ptm_dc_shipment_stg
                     SET processed_flag = 'E'
                        ,process_message =
                                       SUBSTR (process_message || ';' || v_validate_fields, 1, 4000)
                   WHERE delivery_id = dist_del.delivery_id
                     AND pallet_id = rec_pal_cart.pallet_id
                     AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                     AND processed_flag <> 'P';

                  retcode := 1;
                WHEN exp_carton_raise_error
                THEN
                  fnd_file.put_line (fnd_file.LOG
                                    , 'Carton Invalid:' || rec_pal_cart.master_carton_id
                                    );

                  UPDATE xxntgr_ptm_dc_shipment_stg
                     SET processed_flag = 'E'
                        ,process_message =
                                       SUBSTR (process_message || ';' || v_validate_fields, 1, 4000)
                   WHERE delivery_id = dist_del.delivery_id
                     AND pallet_id = rec_pal_cart.pallet_id
                     AND master_carton_id = rec_pal_cart.master_carton_id
                     AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1')
                     AND processed_flag <> 'P';

                  retcode := 1;
              END;
            END LOOP;
          END IF;

          ---3.18 End SKONANKI
          -- Start 4.1
          IF v_validate_header IS NOT NULL
          THEN
            v_validate_fields := SUBSTR (v_validate_fields || '<br>' || v_validate_header, 1, 4000);
          END IF;

          ---End 4.1
          IF v_validate_fields IS NOT NULL
          THEN
            --Call the mail program to send notification for validation failure
            BEGIN
              xxntgr_ptm_odm_pkg.ptm_dc_ship_mail (dist_del.delivery_id
                                                  ,UPPER (dist_del.dc_name)
                                                  ,dist_del.file_name
                                                  ,SUBSTR (v_validate_fields, 1, 1500)
                                                  ,v_get_mail_error
                                                  );
              xxntgr_debug_utl.error (v_get_mail_error);
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                NULL;
              WHEN OTHERS
              THEN
                NULL;
            END;

            xxntgr_debug_utl.error
                             (   'Error while doing validation for mandatory fields for DELIVERY : '
                              || dist_del.delivery_id
                              || '-'
                              || v_validate_fields
                             );

            ---Start 4.1
            UPDATE xxntgr_ptm_dc_file_header
               SET process_flag = 'E'
             WHERE delivery_id = dist_del.delivery_id AND input_file_id = dist_del.input_file_id;

            --End  4.1

            --Insert error data into archive table
            INSERT INTO xxntgr_ptm_dc_shipment_arch
              SELECT *
                FROM xxntgr_ptm_dc_shipment_stg
               WHERE delivery_id = dist_del.delivery_id
                 AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1');

            --AND nvl(PROCESSED_FLAG,'NULL') NOT IN ('P','S');

            --Delete error data from staging table
            DELETE FROM xxntgr_ptm_dc_shipment_stg
                  WHERE delivery_id = dist_del.delivery_id
                    AND NVL (UPPER (dc_name), '1') = NVL (UPPER (dist_del.dc_name), '1');

            --AND nvl(PROCESSED_FLAG,'NULL') NOT IN ('P','S');
            COMMIT;
            retcode := 1;
          END IF;
        END LOOP;
      EXCEPTION
        WHEN OTHERS
        THEN
          xxntgr_debug_utl.error
                       (   'Error while checking for mandatory columns in the file for DELIVERY : '
                        || SQLCODE
                        || SQLERRM
                       );
--mmore start 2.8
          v_log_err_msg := '\n\tError: WO01 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
--mmore end 2.8
      END;

      -- Start processig if all the mandatory columns are provided
      BEGIN                                                                                      --1
        OPEN get_delivery;

        LOOP
          FETCH get_delivery
           INTO v_delivery, v_dc_name, v_file_name, v_input_file_id;

          EXIT WHEN get_delivery%NOTFOUND;
          xxntgr_debug_utl.DEBUG ('Delivery ID processing :' || v_delivery);
          v_invalid_dc := NULL;
          v_reason := NULL;
          v_proc_status := NULL;
          v_aircard_item_exists := 'N';
          l_header_error_flag := NULL;                                                       ---4.1
          l_p_process_flag := NULL;

          -- 5.3 Initialized the variable to null for the new file to be processed

          --Validate DC
          BEGIN
            SELECT description
              INTO v_dc
              FROM fnd_lookup_values
             WHERE description = UPPER (v_dc_name)
               AND lookup_type = 'XXNTGR_PTM_DC_NAMES'
               AND enabled_flag = 'Y';
          EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
              v_invalid_dc := 'DC Name ' || v_dc_name || ' is invalid.';

              UPDATE xxntgr_ptm_dc_shipment_stg
                 SET processed_flag = 'E'
                    ,process_message = SUBSTR (process_message || ' ' || 'INVALID DC', 1, 4000)
               WHERE delivery_id = v_delivery
                 AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                 AND (processed_flag = 'N' OR processed_flag IS NULL);

              --Insert error data into archive table
              INSERT INTO xxntgr_ptm_dc_shipment_arch
                SELECT *
                  FROM xxntgr_ptm_dc_shipment_stg
                 WHERE delivery_id = v_delivery
                   AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                   AND processed_flag <> 'P';

              --Delete error data from staging table
              DELETE FROM xxntgr_ptm_dc_shipment_stg
                    WHERE delivery_id = v_delivery
                      AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                      AND processed_flag <> 'P';

              l_header_error_flag := 'E';                                                     ---4.1

              BEGIN
                xxntgr_ptm_odm_pkg.ptm_dc_ship_mail (v_delivery
                                                    ,UPPER (v_dc_name)
                                                    ,v_file_name
                                                    ,SUBSTR (v_invalid_dc, 1, 1500)
                                                    ,v_get_mail_error
                                                    );
                xxntgr_debug_utl.error ('Delivery:' || v_delivery || '-' || v_get_mail_error);
              EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                  NULL;
                WHEN OTHERS
                THEN
                  NULL;
              END;

              retcode := 1;
            WHEN OTHERS
            THEN
              xxntgr_debug_utl.error (   'DELIVERY:'
                                      || v_delivery
                                      || '-'
                                      || 'Error while validating for valid ODM name: '
                                      || SQLCODE
                                      || SQLERRM
                                     );
          END;

          IF v_invalid_dc IS NULL
          THEN
            --Validate DELIVERY_ID
            BEGIN
              SELECT delivery_id
                INTO v_deliveryid
                FROM wsh_new_deliveries
               WHERE delivery_id = v_delivery;
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                v_invalid_delivery := 'Delivery ID ' || v_delivery || ' is Invalid.';
                xxntgr_debug_utl.warn (v_invalid_delivery);
                v_deliveryid := NULL;
                --mmore start 2.8
                v_log_err_msg := '\n\tError: WO02 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
              --mmore end 2.8
              WHEN OTHERS
              THEN
                xxntgr_debug_utl.error ('Error while validating Delivery ID: ' || v_delivery);
                v_deliveryid := NULL;
                --mmore start 2.8
                v_log_err_msg := '\n\tError: WO03 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
            --mmore end 2.8
            END;
          ELSE
            v_deliveryid := NULL;
          END IF;

          IF v_deliveryid IS NOT NULL
          THEN
            v_delivery_id := v_delivery;

            BEGIN                                                                               --2
              IF v_delivery_id IS NOT NULL
              THEN
                  /* VALIDATION starts here */
                --set process_mode, and validate serial_number for duplicacy
                FOR i IN set_process_mode (v_delivery_id, v_dc_name)
                LOOP
                                    --  BEGIN
                  /*  Added new functionality for DSV  by ppatil 3.6 */
                  BEGIN
                    SELECT trx.delivery_id
                      INTO v_dup_del_sn
                      FROM xxntgr_ptm_so_shipment_trx trx
                     WHERE trx.serial_number = i.serial_number     --trx.delivery_id = i.delivery_id
                       AND trx.dc_name = UPPER (i.dc_name);

                    IF v_dup_del_sn <> i.delivery_id
                    THEN
                      IF (i.creation_date) <> TRUNC (SYSDATE)
                      THEN
                        v_process_mode := 'INSERT';
                      ELSE
                        v_reason :=
                          SUBSTR (   v_reason
                                  || 'Serial Number XXXX already shipped in a different delivery ID.'
                                 ,1
                                 ,4000
                                 );
                      END IF;
                    ELSE
                      v_process_mode := 'UPDATE';
                    END IF;
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      v_process_mode := 'INSERT';
                    WHEN OTHERS
                    THEN
                      v_errmsg := 'Error while setting process mode: ' || SQLCODE || SQLERRM;

                      UPDATE xxntgr_ptm_dc_shipment_stg
                         SET processed_flag = 'E'
                            ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                       WHERE delivery_id = v_delivery
                         AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                         AND processed_flag <> 'P'
                         AND serial_number = i.serial_number;

                      l_header_error_flag := 'E';                                             ---4.1
                      xxntgr_debug_utl.error ('Deliver ID:' || v_delivery_id || '-' || v_errmsg);
                  END;

                  IF (p_ignr_dup_sn = 'N')
                  THEN                            --changed the conditon on 7 dec 2014 by ppatil 3.6
                    IF v_process_mode = 'INSERT'
                    THEN
                      BEGIN
                        SELECT serial_number
                          INTO v_chk_duplicate_sn
                          FROM xxntgr_ptm_so_shipment_trx
                         WHERE serial_number = i.serial_number;

                        UPDATE xxntgr_ptm_dc_shipment_stg
                           SET processed_flag = 'E'
                              ,process_message =
                                         SUBSTR (process_message || ' ' || 'DUPLICATE SN2', 1, 4000)
                         WHERE delivery_id = v_delivery
                           AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                           AND processed_flag <> 'P'
                           AND serial_number = i.serial_number;

                        l_header_error_flag := 'E';                                           ---4.1
                        v_reason :=
                          SUBSTR (   v_reason
                                  || ' Serial Number '
                                  || i.serial_number
                                  || ' has previously been used.'
                                 ,1
                                 ,4000
                                 );
                      EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                          NULL;
                        WHEN OTHERS
                        THEN
                          v_errmsg :=
                            'Error while finding duplicate SN in PTM table: ' || SQLCODE || SQLERRM;

                          UPDATE xxntgr_ptm_dc_shipment_stg
                             SET processed_flag = 'E'
                                ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                           WHERE delivery_id = v_delivery
                             AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                             AND processed_flag <> 'P'
                             AND serial_number = i.serial_number;

                          l_header_error_flag := 'E';                                         ---4.1
                          xxntgr_debug_utl.error ('DELIVERY:' || v_delivery_id || '-' || v_errmsg);
                          --mmore start 2.8
                          v_log_err_msg :=
                                  '\n\tError: WO04 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
                      --mmore end 2.8
                      END;
                    END IF;
                  END IF;
                END LOOP;

                /* DSV changed functionality end 3.6*/
                       --Validation for Item Number
                FOR i IN validate_items (v_delivery_id, v_dc_name)
                LOOP
                  BEGIN
                    SELECT dd.inventory_item_id
                      INTO v_valid_item
                      FROM wsh_delivery_details dd
                          ,wsh_new_deliveries nd
                          ,wsh_delivery_assignments da
                     WHERE da.delivery_id = i.delivery_id
                       AND da.delivery_detail_id = i.delivery_detail_id
                       AND dd.inventory_item_id = i.inventory_item_id
                       AND nd.delivery_id = da.delivery_id
                       AND dd.delivery_detail_id = da.delivery_detail_id;
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      UPDATE xxntgr_ptm_dc_shipment_stg
                         SET processed_flag = 'E'
                            ,process_message =
                                          SUBSTR (process_message || ' ' || ' INVALID SKU', 1, 4000)
                       WHERE delivery_id = v_delivery
                         AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                         AND processed_flag <> 'P'
                         AND item_number = i.item_number;

                      l_header_error_flag := 'E';                                             ---4.1
                      v_reason :=
                        SUBSTR (   v_reason
                                || '<br>'
                                || 'Delivery Line ID:'
                                || i.delivery_detail_id
                                || ' - SKU '
                                || i.item_number
                                || ' is invalid.'
                               ,1
                               ,4000
                               );
                    WHEN OTHERS
                    THEN
                      v_errmsg := 'Error in Item number validation. ' || SQLCODE || '-' || SQLERRM;

                      UPDATE xxntgr_ptm_dc_shipment_stg
                         SET processed_flag = 'E'
                            ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                       WHERE delivery_id = v_delivery
                         AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                         AND processed_flag <> 'P'
                         AND item_number = i.item_number;

                      l_header_error_flag := 'E';                                             ---4.1
                      xxntgr_debug_utl.error ('DELIVERY:' || v_delivery_id || '-' || v_errmsg);
                      --mmore start 2.8
                      v_log_err_msg :=
                                  '\n\tError: WO05 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
                  --mmore end 2.8
                  END;
                END LOOP;

                --validation for any item number missing from file in comparision with the delivery, only if P_MATCH_TO_SN parameter is set to 'Y'
                IF p_match_to_delivery = 'Y'
                THEN
                  BEGIN
                    BEGIN
                      SELECT DISTINCT 'Y'
                                 INTO v_aircard_item_exists
                                 FROM wsh_delivery_details dd
                                     ,wsh_new_deliveries nd
                                     ,wsh_delivery_assignments da
                                     ,mtl_system_items_b m
                                WHERE da.delivery_id = v_delivery
                                  AND nd.delivery_id = da.delivery_id
                                  AND dd.delivery_detail_id = da.delivery_detail_id
                                  AND dd.inventory_item_id = m.inventory_item_id
                                  AND m.attribute19 = 'AIRCARD'
                                  AND m.organization_id = 488;
                    EXCEPTION
                      WHEN NO_DATA_FOUND
                      THEN
                        v_aircard_item_exists := 'N';
                      WHEN OTHERS
                      THEN
                        xxntgr_debug_utl.error
                                (   'DELIVERY ID: ERROR while finding if there is an aircard item '
                                 || v_delv_id
                                 || '-'
                                 || v_errmsg
                                );
                        --mmore start 2.8
                        v_log_err_msg :=
                                  '\n\tError: WO06 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
                    --mmore end 2.8
                    END;

                    --IF v_aircard_item_exists = 'N' THEN
                    OPEN missed_delivery_line (v_delivery_id, v_dc_name);

                    LOOP
                      v_missed_line := NULL;                                      --added by ppatil
                      v_item_id := NULL;                                          --added by ppatil

                      FETCH missed_delivery_line
                       INTO v_missed_line, v_item_id;

                      EXIT WHEN missed_delivery_line%NOTFOUND;

                      IF v_missed_line IS NOT NULL
                      THEN
                        UPDATE xxntgr_ptm_dc_shipment_stg
                           SET processed_flag = 'E'
                              ,process_message =
                                          SUBSTR (process_message || ' ' || 'ITEM_MISSING', 1, 4000)
                         WHERE delivery_id = v_delivery
                           AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                           AND NVL (processed_flag, 'N') <> 'P';          --added NVL by ppatil 3.15

                        l_header_error_flag := 'E';                                           ---4.1

                        --mmore 3.8 start
                        BEGIN
                          SELECT segment1
                            INTO v_item_name
                            FROM mtl_system_items_b
                           WHERE inventory_item_id = v_item_id AND organization_id = 488;
                        EXCEPTION
                          WHEN OTHERS
                          THEN
                            v_item_name := NULL;
                        END;

                        IF v_item_name IS NULL
                        THEN
                          v_item_name := v_item_id;
                        END IF;

                        --mmore 3.8 end
                          --v_reason := SUBSTR(v_reason||'<br>'||'Delivery line ID '||v_missed_line||' ITEM '||v_item_id ||' is missing from file (but exists on delivery).',1,4000); mmore 3.8 commented
                        v_reason :=
                          SUBSTR (   v_reason
                                  || '<br>'
                                  || 'Delivery line ID '
                                  || v_missed_line
                                  || ' ITEM '
                                  || v_item_name
                                  || ' is missing from file (but exists on delivery).'
                                 ,1
                                 ,4000
                                 );                                                      --mmore 3.8
                        fnd_file.put_line (fnd_file.LOG
                                          , ' v_reason for  ' || v_missed_line || 'is' || v_reason
                                          );
                      ELSE
                        NULL;
                      END IF;
                    END LOOP;

                    CLOSE missed_delivery_line;
                  -- END IF;
                  END;
                ELSE
                  NULL;
                END IF;

                --validation for quantity_shipped, item number and duplicate SN
                OPEN validate_shipped_qty (v_delivery_id, v_dc_name);

                LOOP
                  FETCH validate_shipped_qty
                   INTO v_delv_id, v_delv_dt_id, v_item_num;

                  EXIT WHEN validate_shipped_qty%NOTFOUND;
                  v_unprocess_flag := NULL;                                     -- 3.17  Subbareddy

                  /*Validation for quantity_shipped*/
                  BEGIN
                    SELECT   NVL (SUM (dd.shipped_quantity), 0)
                            ,NVL (SUM (dd.picked_quantity), 0), nd.confirm_date    --added by ppatil
                        INTO v_quantity_shipped
                            ,v_quantity_picked, v_confirm_date
                        FROM wsh_delivery_details dd
                            ,wsh_new_deliveries nd
                            ,wsh_delivery_assignments da
                       WHERE 1 = 1
                         AND da.delivery_id = v_delv_id
                         AND da.delivery_detail_id = v_delv_dt_id
                         AND nd.delivery_id = da.delivery_id
                         AND dd.delivery_detail_id = da.delivery_detail_id
                    GROUP BY nd.confirm_date;
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      v_quantity_shipped := 0;
                      v_quantity_picked := 0;
                      xxntgr_debug_utl.warn
                        (   'Error while getting Shipped_Quantity or Picked_Quantity for DELIVERY: '
                         || v_delv_id
                         || SQLCODE
                         || SQLERRM
                        );
                    WHEN OTHERS
                    THEN
                      v_quantity_shipped := 0;
                      v_errmsg :=
                           'Error while getting Shipped_Quantity or Picked_Quantity for DELIVERY: '
                        || v_delv_id
                        || SQLCODE
                        || SQLERRM;

                      UPDATE xxntgr_ptm_dc_shipment_stg
                         SET processed_flag = 'E'
                            ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                       WHERE delivery_id = v_delv_id
                         AND delivery_detail_id = v_delv_dt_id
                         AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                         AND processed_flag <> 'P';

                      l_header_error_flag := 'E';                                             ---4.1
                      xxntgr_debug_utl.error ('DELIVERY ID:' || v_delv_id || '-' || v_errmsg);
                      --mmore start 2.8
                      v_log_err_msg :=
                                  '\n\tError: WO07 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
                  --mmore end 2.8
                  END;

                  BEGIN
                    --count the total line of staging table for delivery line, to match the total quantity shipped
                    SELECT COUNT (*)
                      INTO v_cnt_delv_line
                      FROM xxntgr_ptm_dc_shipment_stg
                     WHERE delivery_id = v_delv_id
                       AND delivery_detail_id = v_delv_dt_id
                       AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                       AND processed_flag <> 'P';

                    IF v_cnt_delv_line <> 0
                    THEN
                      -- Subbareddy start 3.17
                      IF v_cnt_delv_line <> v_quantity_picked
                      THEN
                        --The order line is Shiped Confirm and Delivery line and quantity picked not matched then will failing the file.
                        IF v_confirm_date IS NOT NULL
                        THEN
                          UPDATE xxntgr_ptm_dc_shipment_stg
                             SET processed_flag = 'E'
                                ,process_message =
                                       SUBSTR (process_message || ' ' || 'QTY_SN_MISMATCH', 1, 4000)
                           WHERE delivery_id = v_delv_id
                             AND delivery_detail_id = v_delv_dt_id
                             AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                             AND processed_flag <> 'P';

                          l_header_error_flag := 'E';                                         ---4.1
                          v_reason :=
                            SUBSTR
                              (   v_reason
                               || '<br>'
                               || 'The quantity picked does not match the total serial numbers in file for SKU '
                               || v_item_num
                              ,1
                              ,4000
                              );
                        ELSE
                          --The order line is not yet ship confirmed ,then the PROCESSED_FLAG status clumn should be stored in table xxntgr_ptm_dc_shipment_stg as 'N'
                          v_unprocess_flag := 'N';

                          UPDATE xxntgr_ptm_dc_shipment_stg
                             SET processed_flag = 'N'
                           WHERE delivery_id = v_delv_id
                             AND delivery_detail_id = v_delv_dt_id
                             AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                             AND processed_flag <> 'P';
                        END IF;
                      END IF;
                    -- Subbareddy End 3.17
                    END IF;
                  EXCEPTION
                    WHEN OTHERS
                    THEN
                      v_errmsg := 'Error in Quantity_Shipped validation. ' || SQLCODE || SQLERRM;

                      UPDATE xxntgr_ptm_dc_shipment_stg
                         SET processed_flag = 'E'
                            ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                       WHERE delivery_id = v_delv_id
                         AND delivery_detail_id = v_delv_dt_id
                         AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                         AND processed_flag <> 'P';

                      l_header_error_flag := 'E';                                             ---4.1
                      xxntgr_debug_utl.error ('DELIVERY:' || v_delv_id || '-' || v_errmsg);
                      --mmore start 2.8
                      v_log_err_msg :=
                                  '\n\tError: WO08 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
                  --mmore end 2.8
                  END;
                END LOOP;

                CLOSE validate_shipped_qty;

                --valiation for duplicate SNs in delivery file
                BEGIN
                  OPEN get_duplicate_sn (v_delivery_id, v_dc_name);

                  LOOP
                    FETCH get_duplicate_sn
                     INTO v_duplicate_sn;

                    EXIT WHEN get_duplicate_sn%NOTFOUND;

                    UPDATE xxntgr_ptm_dc_shipment_stg
                       SET processed_flag = 'E'
                          ,process_message =
                                          SUBSTR (process_message || ' ' || 'DUPLICATE SN', 1, 4000)
                     WHERE delivery_id = v_delivery
                       AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                       AND processed_flag <> 'P'
                       --AND DELIVERY_DETAIL_ID = v_delv_line_id
                       AND serial_number = v_duplicate_sn;

                    l_header_error_flag := 'E';                                               ---4.1
                    v_reason :=
                      SUBSTR (   v_reason
                              || '<br>'
                              || 'Serial Number '
                              || v_duplicate_sn
                              || ' is duplicated within the DELIVERY# '
                              || v_delivery_id
                             ,1
                             ,4000
                             );
                  END LOOP;

                  CLOSE get_duplicate_sn;
                END;

                IF v_reason IS NOT NULL
                THEN
                  --Call the send mail program to send mail to contacts
                  BEGIN
                    xxntgr_ptm_odm_pkg.ptm_dc_ship_mail (v_delivery
                                                        ,UPPER (v_dc_name)
                                                        ,v_file_name
                                                        ,SUBSTR (v_reason, 1, 1500)
                                                        ,v_get_mail_error
                                                        );
                    xxntgr_debug_utl.error ('DELIVERY:' || v_delivery || '-' || v_get_mail_error);
                  EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                      NULL;
                    WHEN OTHERS
                    THEN
                      NULL;
                  END;

                  xxntgr_debug_utl.error ('Program failed for following reasons: ' || v_reason);
                END IF;
              /*VALIDATION ends here*/
              END IF;
            END;                                                                                 --2

            xxntgr_debug_utl.DEBUG ('Delivery ID record processing :' || v_delivery);

            /*====Record processing starts here===*/
            BEGIN                                                                                --3
              BEGIN
                SELECT DISTINCT processed_flag
                           INTO v_proc_status
                           FROM xxntgr_ptm_dc_shipment_stg
                          WHERE delivery_id = v_delivery_id AND UPPER (dc_name) = UPPER (v_dc_name);
              --AND PROCESSED_FLAG = 'E'   Commented Subbareddy 3.17
              EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                  v_proc_status := 'E';                                               --- Added 4.1
                -- NULL; -- Commented 4.1
                WHEN OTHERS
                THEN
                  --Start 4.1
                  l_err_count := 0;

                  SELECT COUNT (1)
                    INTO l_err_count
                    FROM xxntgr_ptm_dc_shipment_stg
                   WHERE delivery_id = v_delivery_id
                     AND UPPER (dc_name) = UPPER (v_dc_name)
                     AND processed_flag = 'E';

                  IF l_err_count > 0
                  THEN
                    v_proc_status := 'E';
                  END IF;
                  -- End 4.1
              -- NULL; -- Commented 4.1
              END;

              IF v_proc_status = 'E'
              THEN
                --Insert error data into archive table
                INSERT INTO xxntgr_ptm_dc_shipment_arch
                  SELECT *
                    FROM xxntgr_ptm_dc_shipment_stg
                   WHERE delivery_id = v_delivery_id
                     AND UPPER (dc_name) = UPPER (v_dc_name)
                     AND processed_flag <> 'P';

                --Delete error data from staging table
                DELETE FROM xxntgr_ptm_dc_shipment_stg
                      WHERE delivery_id = v_delivery_id
                        AND UPPER (dc_name) = UPPER (v_dc_name)
                        AND processed_flag <> 'P';

                l_header_error_flag := 'E';                                                   ---4.1
                retcode := 1;
                -- Subbareddy start 3.17
              -- The order line is not yet ship confirmed ,then the PROCESSED_FLAG status clumn should be stored in table xxntgr_ptm_dc_shipment_stg as 'N'
              ELSIF v_proc_status = 'N' AND v_unprocess_flag = 'N'
              THEN
                NULL;
                l_header_error_flag := 'N';                                                  ---4.1
              -- Subbareddy End 3.17
              ELSE
                /*Call procedure to handle bundled items 3.16 added by ppatil*/
                xxntgr_ptm_common_pkg.ptm_dc_archive_bundled_items (v_delivery, v_dc_name);

                /*End handle of bundled items 3.16 end*/
                  -- v2.0 Start Move Data from xxntgr_ptm_dc_shipment_stg to xxntgr_ptm_ac_sfs_stg
                UPDATE xxntgr_ptm_dc_shipment_stg xpdss
                   SET xpdss.processed_flag = 'SFS'
                 WHERE (xpdss.processed_flag = 'N' OR xpdss.processed_flag IS NULL)
                   AND xpdss.delivery_id = v_delivery_id
                   AND UPPER (xpdss.dc_name) = UPPER (v_dc_name)
                   AND EXISTS (
                         SELECT 1
                           FROM mtl_system_items_b msi
                          WHERE msi.segment1 = xpdss.item_number
                            AND NVL (attribute19, 'XXXX') = 'AIRCARD');

                -- Start 4.1
                l_sfs_flag_count := 0;
                l_sfs_process_flag := 'N';

                SELECT COUNT (1)
                  INTO l_sfs_flag_count
                  FROM xxntgr_ptm_dc_shipment_stg
                 WHERE delivery_id = v_delivery_id AND processed_flag = 'SFS';

                fnd_file.put_line (fnd_file.LOG
                                  ,    'Total records with having processed flag as SFS :'
                                    || l_sfs_flag_count
                                  );

                IF l_sfs_flag_count > 0
                THEN
                  l_sfs_process_flag := 'Y';
                END IF;

                -- End   4.1
                xxntgr_ptm_apl_dc_ship_pkg.move_to_sfs_stg (v_delivery_id, NULL, v_dc_name);

                -- v2.0 End Move Data from xxntgr_ptm_dc_shipment_stg to xxntgr_ptm_ac_sfs_stg
                BEGIN                                                                            --4
                  --Set the PROCESSED_FLAG to "P" (Processed) for validated records
                  UPDATE xxntgr_ptm_dc_shipment_stg
                     SET processed_flag = 'P'
                   WHERE (processed_flag = 'N' OR processed_flag IS NULL)
                     AND delivery_id = v_delivery_id
                     AND UPPER (dc_name) = UPPER (v_dc_name);

                  ---Start 4.1
                  l_p_flag_count := 0;
                  l_p_process_flag := 'N';

                  SELECT COUNT (1)
                    INTO l_p_flag_count
                    FROM xxntgr_ptm_dc_shipment_stg
                   WHERE delivery_id = v_delivery_id AND processed_flag = 'P';

                  fnd_file.put_line (fnd_file.LOG
                                    ,    'Total records with having processed flag as P :'
                                      || l_p_flag_count
                                    );

                  IF l_p_flag_count > 0
                  THEN
                    l_p_process_flag := 'Y';
                  END IF;

                  --End  4.1
                  xxntgr_debug_utl.DEBUG (   'DELIVERY: '
                                          || v_delivery_id
                                          || ' - Process Mode : '
                                          || v_process_mode
                                         );
                  fnd_file.put_line (fnd_file.LOG
                                    ,    'Process Mode: '
                                      || v_process_mode
                                      || ' Delivery ID: '
                                      || v_delivery_id
                                    );                                                   --mmore 3.9
                  v_sn_error := NULL;                                                         ---5.0

                  OPEN get_dc_data (v_delivery_id, v_dc_name);

                  LOOP
                    FETCH get_dc_data
                    BULK COLLECT INTO dc_ship;

                    FOR i IN 1 .. dc_ship.COUNT
                    LOOP
                      --get inventory_item_id to populate in the main tables
                      SELECT inventory_item_id
                        INTO v_inventory_item_id
                        FROM mtl_system_items_b
                       WHERE segment1 = dc_ship (i).item_number AND organization_id = 488;

                      --get organization_id to populate in the main table
                      SELECT dd.organization_id
                        INTO v_organization_id
                        FROM wsh_delivery_details dd
                            ,wsh_new_deliveries nd
                            ,wsh_delivery_assignments da
                       WHERE da.delivery_id = dc_ship (i).delivery_id
                         AND da.delivery_detail_id = dc_ship (i).delivery_detail_id
                         AND nd.delivery_id = da.delivery_id
                         AND dd.delivery_detail_id = da.delivery_detail_id;

                      BEGIN
                        v_err_loc := '001';                                             --mmore 3.9

                        SELECT instance_id
                          INTO v_instance_id
                          FROM xxntgr_ptm_item_instances
                         WHERE serial_number = dc_ship (i).serial_number AND end_date_active IS NULL;

                        -- mmore added 3.9
                        v_err_loc := '002';                                              --mmore 3.9

                        ---5.0 start Sreenath Konanki
                        BEGIN
                          v_dup_del_sn := NULL;
                          v_process_mode := NULL;

                          SELECT trx.delivery_id
                            INTO v_dup_del_sn
                            FROM xxntgr_ptm_so_shipment_trx trx
                           WHERE trx.serial_number = dc_ship (i).serial_number
                             --trx.delivery_id = i.delivery_id
                             AND trx.dc_name = UPPER (dc_ship (i).dc_name);

                          IF v_dup_del_sn <> dc_ship (i).delivery_id
                          THEN
                            IF (dc_ship (i).creation_date) <> TRUNC (SYSDATE)
                            THEN
                              v_process_mode := 'INSERT';
                            ELSE
                              v_reason :=
                                SUBSTR
                                  (   v_reason
                                   || 'Serial Number XXXX already shipped in a different delivery ID.'
                                  ,1
                                  ,4000
                                  );
                            END IF;
                          ELSE
                            v_process_mode := 'UPDATE';
                          END IF;
                        EXCEPTION
                          WHEN NO_DATA_FOUND
                          THEN
                            v_process_mode := 'INSERT';
                          WHEN OTHERS
                          THEN
                            v_errmsg := 'Error while setting process mode: ' || SQLCODE || SQLERRM;

                            UPDATE xxntgr_ptm_dc_shipment_stg
                               SET processed_flag = 'E'
                                  ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                             WHERE delivery_id = v_delivery
                               AND NVL (UPPER (dc_name), '1') = NVL (UPPER (v_dc_name), '1')
                               AND processed_flag <> 'P'
                               AND serial_number = dc_ship (i).serial_number;

                            l_header_error_flag := 'E';                                       ---4.1
                            xxntgr_debug_utl.error (   'Deliver ID:'
                                                    || dc_ship (i).delivery_id
                                                    || '-'
                                                    || v_errmsg
                                                   );
                        END;

                        ---5.0 End Sreenath Konanki
                        IF v_process_mode = 'INSERT'
                        THEN
                          INSERT INTO xxntgr_ptm_so_shipment_trx
                                      (instance_id, so_shipment_trx_id
                                      ,pallet_id, master_carton_id
                                      ,item_number, inventory_item_id
                                      ,organization_id, serial_number
                                      ,delivery_id, delivery_detail_id
                                      ,dc_name, created_by, creation_date, last_updated_by
                                      ,last_update_date, input_file_id
                                      ,processed_flag, request_id
                                      ,attribute1, attribute2
                                      )
                               /*ATTRIBUTE3,
                               ATTRIBUTE4,
                               ATTRIBUTE5,
                               ATTRIBUTE6,
                               ATTRIBUTE7,
                               ATTRIBUTE8,
                               ATTRIBUTE9,
                               ATTRIBUTE10,
                               ATTRIBUTE11,
                               ATTRIBUTE12,
                               ATTRIBUTE13,
                               ATTRIBUTE14,
                               ATTRIBUTE15)*/
                          VALUES      (v_instance_id, xxntgr_ptm_so_shipment_trx_s.NEXTVAL
                                      ,dc_ship (i).pallet_id, dc_ship (i).master_carton_id
                                      ,dc_ship (i).item_number, v_inventory_item_id
                                      ,v_organization_id, dc_ship (i).serial_number
                                      ,dc_ship (i).delivery_id, dc_ship (i).delivery_detail_id
                                      ,dc_ship (i).dc_name, 2500, dc_ship (i).creation_date, 2500
                                      ,dc_ship (i).last_update_date, dc_ship (i).input_file_id
                                      ,dc_ship (i).processed_flag, v_reqstid
                                      ,dc_ship (i).attribute1, dc_ship (i).attribute2
                                      );

                          /*i.ATTRIBUTE3,
                          i.ATTRIBUTE4,
                          i.ATTRIBUTE5,
                          i.ATTRIBUTE6,
                          i.ATTRIBUTE7,
                          i.ATTRIBUTE8,
                          i.ATTRIBUTE9,
                          i.ATTRIBUTE10,
                          i.ATTRIBUTE11,
                          i.ATTRIBUTE12,
                          i.ATTRIBUTE13,
                          i.ATTRIBUTE14,
                          i.ATTRIBUTE15);*/
                          UPDATE xxntgr_ptm_item_instances
                             SET last_transaction_type = '04'
                                ,last_update_date = SYSDATE                          -- GP 22-AUG-12
                           WHERE instance_id = v_instance_id;

                          --Insert into XXNTGR_PTM_TRX_REGISTER table
                          INSERT INTO xxntgr_ptm_trx_register
                                      (transaction_id, instance_id
                                      ,item_number, inventory_item_id
                                      ,organization_id, serial_number, transaction_type
                                      ,transaction_source_type, transaction_source_header_id
                                      ,transaction_source_line_id,
                                                                  --CREATED_BY,
                                                                  creation_date,
                                                                                --LAST_UPDATED_BY,
                                                                                last_update_date
                                      )
                               /*ATTRIBUTE1,
                               ATTRIBUTE2,
                               ATTRIBUTE3,
                               ATTRIBUTE4,
                               ATTRIBUTE5,
                               ATTRIBUTE6,
                               ATTRIBUTE7,
                               ATTRIBUTE8,
                               ATTRIBUTE9,
                               ATTRIBUTE10,
                               ATTRIBUTE11,
                               ATTRIBUTE12,
                               ATTRIBUTE13,
                               ATTRIBUTE14,
                               ATTRIBUTE15)*/
                          VALUES      (xxntgr_ptm_trx_register_s.NEXTVAL, v_instance_id
                                      ,dc_ship (i).item_number, v_inventory_item_id
                                      ,v_organization_id, dc_ship (i).serial_number, '04'
                                      ,                                           --TRANSACTION_TYPE
                                       'SO',                               --TRANSACTION_SOURCE_TYPE
                                            xxntgr_ptm_so_shipment_trx_s.CURRVAL
                                      ,NULL,
                                            --i.created_by,
                                            dc_ship (i).creation_date,
                                                                      --i.last_updated_by,
                                                                      dc_ship (i).last_update_date
                                      );
                        /*i.ATTRIBUTE1,
                        i.ATTRIBUTE2,
                        i.ATTRIBUTE3,
                        i.ATTRIBUTE4,
                        i.ATTRIBUTE5,
                        i.ATTRIBUTE6,
                        i.ATTRIBUTE7,
                        i.ATTRIBUTE8,
                        i.ATTRIBUTE9,
                        i.ATTRIBUTE10,
                        i.ATTRIBUTE11,
                        i.ATTRIBUTE12,
                        i.ATTRIBUTE13,
                        i.ATTRIBUTE14,
                        i.ATTRIBUTE15);*/
                        ELSE
                          --(Process_mode = UPDATE) - UPDATE the PTM tables with the new data, based on old_instance_id
                          v_err_loc := '003';                                           --mmore 3.9

                          SELECT trx.instance_id, reg.transaction_type
                            INTO v_inst_id, v_trans_type
                            FROM xxntgr_ptm_so_shipment_trx trx
                                ,xxntgr_ptm_item_instances inst
                                ,xxntgr_ptm_trx_register reg
                           WHERE trx.instance_id = inst.instance_id
                             AND inst.instance_id = reg.instance_id
                             AND trx.delivery_id = dc_ship (i).delivery_id
                             AND trx.dc_name = UPPER (dc_ship (i).dc_name)
                             AND trx.serial_number = dc_ship (i).serial_number
                             AND inst.end_date_active IS NULL
                             AND reg.transaction_type = '04';                     -- mmore added 3.9

                          v_err_loc := '004';                                            --mmore 3.9

                          --Update XXNTGR_PTM_ITEM_INSTANCES table
                          UPDATE xxntgr_ptm_item_instances
                             SET last_transaction_type = '04'
                                ,last_update_date = SYSDATE                          -- GP 22-AUG-12
                           WHERE instance_id = v_inst_id;

                          --Update XXNTGR_PTM_SO_SHIPMENT_TRX  table
                          UPDATE xxntgr_ptm_so_shipment_trx
                             SET instance_id = v_instance_id
                                ,so_shipment_trx_id = xxntgr_ptm_so_shipment_trx_s.NEXTVAL
                                ,pallet_id = dc_ship (i).pallet_id
                                ,master_carton_id = dc_ship (i).master_carton_id
                                ,item_number = dc_ship (i).item_number
                                ,inventory_item_id = v_inventory_item_id
                                ,organization_id = v_organization_id
                                ,serial_number = dc_ship (i).serial_number
                                ,delivery_id = dc_ship (i).delivery_id
                                ,delivery_detail_id = dc_ship (i).delivery_detail_id
                                ,dc_name = dc_ship (i).dc_name
                                ,last_update_date = dc_ship (i).last_update_date
                                ,input_file_id = dc_ship (i).input_file_id
                                ,processed_flag = dc_ship (i).processed_flag
                                ,request_id = v_reqstid
                                ,attribute1 = dc_ship (i).attribute1
                                ,attribute2 = dc_ship (i).attribute2
                           /*ATTRIBUTE3 = i.ATTRIBUTE3,
                           ATTRIBUTE4 = i.ATTRIBUTE4,
                           ATTRIBUTE5 = i.ATTRIBUTE5,
                           ATTRIBUTE6 = i.ATTRIBUTE6,
                           ATTRIBUTE7 = i.ATTRIBUTE7,
                           ATTRIBUTE8 = i.ATTRIBUTE8,
                           ATTRIBUTE9 = i.ATTRIBUTE9,
                           ATTRIBUTE10 = i.ATTRIBUTE10,
                           ATTRIBUTE11 = i.ATTRIBUTE11,
                           ATTRIBUTE12 = i.ATTRIBUTE12,
                           ATTRIBUTE13 = i.ATTRIBUTE13,
                           ATTRIBUTE14 = i.ATTRIBUTE14,
                           ATTRIBUTE15 = i.ATTRIBUTE15*/
                          WHERE  instance_id = v_inst_id;

                          --Update XXNTGR_PTM_TRX_REGISTER table
                          UPDATE xxntgr_ptm_trx_register
                             SET transaction_id = xxntgr_ptm_trx_register_s.NEXTVAL
                                ,instance_id = v_instance_id
                                ,item_number = dc_ship (i).item_number
                                ,inventory_item_id = v_inventory_item_id
                                ,organization_id = v_organization_id
                                ,serial_number = dc_ship (i).serial_number
                                ,transaction_type = '04'
                                ,transaction_source_type = 'SO'
                                ,transaction_source_header_id = xxntgr_ptm_so_shipment_trx_s.CURRVAL
                                ,transaction_source_line_id = NULL
                                ,last_update_date = dc_ship (i).last_update_date
                           /*ATTRIBUTE1 = i.ATTRIBUTE1,
                           ATTRIBUTE2 = i.ATTRIBUTE2,
                           ATTRIBUTE3 = i.ATTRIBUTE3,
                           ATTRIBUTE4 = i.ATTRIBUTE4,
                           ATTRIBUTE5 = i.ATTRIBUTE5,
                           ATTRIBUTE6 = i.ATTRIBUTE6,
                           ATTRIBUTE7 = i.ATTRIBUTE7,
                           ATTRIBUTE8 = i.ATTRIBUTE8,
                           ATTRIBUTE9 = i.ATTRIBUTE9,
                           ATTRIBUTE10 = i.ATTRIBUTE10,
                           ATTRIBUTE11 = i.ATTRIBUTE11,
                           ATTRIBUTE12 = i.ATTRIBUTE12,
                           ATTRIBUTE13 = i.ATTRIBUTE13,
                           ATTRIBUTE14 = i.ATTRIBUTE14,
                           ATTRIBUTE15 = i.ATTRIBUTE15*/
                          WHERE  instance_id = v_inst_id AND transaction_type = v_trans_type;
                        END IF;
                      EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                          -- 5.1  Add code to insert data into the PTM tables
                          IF outb_sn_num_tbl (dc_ship (i).item_number) = 'Y'
                          THEN
                            SELECT xxntgr_ptm_item_instances_s.NEXTVAL
                              INTO v_instance_id
                              FROM DUAL;

                            INSERT INTO xxntgr_ptm_so_shipment_trx
                                        (instance_id, so_shipment_trx_id
                                        ,pallet_id, master_carton_id
                                        ,item_number, inventory_item_id
                                        ,organization_id, serial_number
                                        ,delivery_id, delivery_detail_id
                                        ,dc_name, created_by, creation_date
                                        ,last_updated_by, last_update_date
                                        ,input_file_id, processed_flag
                                        ,request_id, attribute1, attribute2
                                        )
                                 VALUES (v_instance_id, xxntgr_ptm_so_shipment_trx_s.NEXTVAL
                                        ,dc_ship (i).pallet_id, dc_ship (i).master_carton_id
                                        ,dc_ship (i).item_number, v_inventory_item_id
                                        ,v_organization_id, dc_ship (i).serial_number
                                        ,dc_ship (i).delivery_id, dc_ship (i).delivery_detail_id
                                        ,dc_ship (i).dc_name, 2500, dc_ship (i).creation_date
                                        ,2500, dc_ship (i).last_update_date
                                        ,dc_ship (i).input_file_id, dc_ship (i).processed_flag
                                        ,v_reqstid, dc_ship (i).attribute1, dc_ship (i).attribute2
                                        );

                            INSERT INTO xxntgr_ptm_item_instances
                                        (instance_id, serial_number
                                        ,item_number, inventory_item_id, created_by
                                        ,creation_date, last_updated_by, last_update_date
                                        ,last_transaction_type
                                        )
                                 VALUES (v_instance_id, dc_ship (i).serial_number
                                        ,dc_ship (i).item_number, v_inventory_item_id, 2500
                                        ,SYSDATE, 2500, SYSDATE
                                        ,'04'
                                        );

                            --Insert into XXNTGR_PTM_TRX_REGISTER table
                            INSERT INTO xxntgr_ptm_trx_register
                                        (transaction_id, instance_id
                                        ,item_number, inventory_item_id
                                        ,organization_id, serial_number, transaction_type
                                        ,transaction_source_type, transaction_source_header_id
                                        ,transaction_source_line_id, created_by, creation_date
                                        ,last_updated_by, last_update_date
                                        )
                                 VALUES (xxntgr_ptm_trx_register_s.NEXTVAL, v_instance_id
                                        ,dc_ship (i).item_number, v_inventory_item_id
                                        ,v_organization_id, dc_ship (i).serial_number, '04'
                                        ,                                         --TRANSACTION_TYPE
                                         'SO',                             --TRANSACTION_SOURCE_TYPE
                                              xxntgr_ptm_so_shipment_trx_s.CURRVAL
                                        ,NULL, 2500, dc_ship (i).creation_date
                                        ,2500, dc_ship (i).last_update_date
                                        );
                          ELSE
                            --- 5.0 Start
                            v_errmsg :=
                                 'Serial number not available in PTM system. '
                              || dc_ship (i).serial_number;
                            xxntgr_debug_utl.error ('DELIVERY:' || v_delivery_id || '-' || v_errmsg);

                            UPDATE xxntgr_ptm_dc_shipment_stg
                               SET processed_flag = 'E'
                                  ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                             WHERE delivery_id = v_delivery
                               AND UPPER (dc_name) = UPPER (v_dc_name)
                               AND serial_number = dc_ship (i).serial_number;

                            v_sn_error := v_sn_error || ',' || dc_ship (i).serial_number;
                          ---5.0 End
                          END IF;
                        -- End 5.1
                        WHEN OTHERS
                        THEN
                          ROLLBACK;
                          v_errmsg :=
                               'Error while inserting/updating data into main tables. '
                            || SQLCODE
                            || '-'
                            || SQLERRM;
                          xxntgr_debug_utl.error ('DELIVERY:' || v_delivery_id || '-' || v_errmsg);

                          UPDATE xxntgr_ptm_dc_shipment_stg
                             SET processed_flag = 'E'
                                ,process_message =
                                                SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                           WHERE delivery_id = v_delivery AND UPPER (dc_name) = UPPER (v_dc_name);

                          --AND PROCESSED_FLAG = 'P';

                          --Insert error data into archive table
                          INSERT INTO xxntgr_ptm_dc_shipment_arch
                            SELECT *
                              FROM xxntgr_ptm_dc_shipment_stg
                             WHERE delivery_id = v_delivery
                               AND UPPER (dc_name) = UPPER (v_dc_name)
                               AND processed_flag = 'E';

                          --Delete error data from staging table
                          DELETE FROM xxntgr_ptm_dc_shipment_stg
                                WHERE delivery_id = v_delivery
                                  AND UPPER (dc_name) = UPPER (v_dc_name)
                                  AND processed_flag = 'E';

                          l_header_error_flag := 'E';                                         ---4.1
                          retcode := 1;
                          --mmore start 2.8
                          v_log_err_msg :=
                               '\n\tError: WO09 @'
                            || v_err_loc
                            || ' Sr.# '
                            || dc_ship (i).serial_number
                            || ' => '
                            || SQLERRM
                            || '\n\nRegards\nNETGEAR Support';
                      --mmore end 2.8
                      END;
                    END LOOP;

                    EXIT WHEN get_dc_data%NOTFOUND;
                  END LOOP;

                  CLOSE get_dc_data;

                  ---5.0 Start
                  IF v_sn_error IS NOT NULL
                  THEN
                    v_sn_error := 'Serial numbers are not available in PTM system -' || v_sn_error;

                    UPDATE xxntgr_ptm_dc_shipment_stg
                       SET processed_flag = 'E'
                     WHERE delivery_id = v_delivery AND UPPER (dc_name) = UPPER (v_dc_name);

                    --AND PROCESSED_FLAG = 'P';

                    --Insert error data into archive table
                    INSERT INTO xxntgr_ptm_dc_shipment_arch
                      SELECT *
                        FROM xxntgr_ptm_dc_shipment_stg
                       WHERE delivery_id = v_delivery
                         AND UPPER (dc_name) = UPPER (v_dc_name)
                         AND processed_flag = 'E';

                    --Delete error data from staging table
                    DELETE FROM xxntgr_ptm_dc_shipment_stg
                          WHERE delivery_id = v_delivery
                            AND UPPER (dc_name) = UPPER (v_dc_name)
                            AND processed_flag = 'E';

                    -- XXNTGR_PTM_ODM_PKG.PTM_DC_SHIP_MAIL(v_delivery,UPPER(v_dc_name),v_file_name,SUBSTR(v_sn_error,1,1500),v_get_mail_error);
                    xxntgr_debug_utl.error ('DELIVERY:' || v_delivery || '-' || v_get_mail_error);
                    l_header_error_flag := 'E';                                               ---4.1
                    v_log_err_msg :=
                                '\n\tError Message:' || v_sn_error || '\n\nRegards\nNETGEAR Support';
                    retcode := 1;
                  END IF;

                  ---5.0 End

                  --Archive and purge processed records
                  INSERT INTO xxntgr_ptm_dc_shipment_arch
                    SELECT *
                      FROM xxntgr_ptm_dc_shipment_stg
                     WHERE delivery_id = v_delivery
                       AND UPPER (dc_name) = UPPER (v_dc_name)
                       AND processed_flag = 'P';

                  --Delete error data from staging table
                  DELETE FROM xxntgr_ptm_dc_shipment_stg
                        WHERE delivery_id = v_delivery
                          AND UPPER (dc_name) = UPPER (v_dc_name)
                          AND processed_flag = 'P';
                EXCEPTION
                  WHEN OTHERS
                  THEN
                    ROLLBACK;
                    v_errmsg :=
                         'Error while updating process status into PTM tables. '
                      || SQLCODE
                      || '-'
                      || SQLERRM;
                    xxntgr_debug_utl.error ('DELIVERY:' || v_delivery || '-' || v_errmsg);

                    UPDATE xxntgr_ptm_dc_shipment_stg
                       SET processed_flag = 'E'
                          ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                     WHERE delivery_id = v_delivery AND UPPER (dc_name) = UPPER (v_dc_name);

                    --AND PROCESSED_FLAG = 'P';

                    --Insert error data into archive table
                    INSERT INTO xxntgr_ptm_dc_shipment_arch
                      SELECT *
                        FROM xxntgr_ptm_dc_shipment_stg
                       WHERE delivery_id = v_delivery
                         AND UPPER (dc_name) = UPPER (v_dc_name)
                         AND processed_flag = 'E';

                    --Delete error data from staging table
                    DELETE FROM xxntgr_ptm_dc_shipment_stg
                          WHERE delivery_id = v_delivery
                            AND UPPER (dc_name) = UPPER (v_dc_name)
                            AND processed_flag = 'E';

                    l_header_error_flag := 'E';                                               ---4.1
                    retcode := 1;
                    --mmore start 2.8
                    v_log_err_msg :=
                                  '\n\tError: WO10 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
                --mmore end 2.8
                END;                                                                             --4
              END IF;
            EXCEPTION
              WHEN OTHERS
              THEN
                ROLLBACK;
                v_errmsg := 'Error in the main program. ' || SQLCODE || '-' || SQLERRM;

                UPDATE xxntgr_ptm_dc_shipment_stg
                   SET processed_flag = 'E'
                      ,process_message = SUBSTR (process_message || ' ' || v_errmsg, 1, 4000)
                 WHERE delivery_id = v_delivery AND UPPER (dc_name) = UPPER (v_dc_name);

                xxntgr_debug_utl.error ('Delivery:' || v_delivery || '-' || v_errmsg);

                --Insert error data into archive table
                INSERT INTO xxntgr_ptm_dc_shipment_arch
                  SELECT *
                    FROM xxntgr_ptm_dc_shipment_stg
                   WHERE delivery_id = v_delivery
                     AND UPPER (dc_name) = UPPER (v_dc_name)
                     AND processed_flag = 'E';

                --Delete error data from staging table
                DELETE FROM xxntgr_ptm_dc_shipment_stg
                      WHERE delivery_id = v_delivery
                        AND UPPER (dc_name) = UPPER (v_dc_name)
                        AND processed_flag = 'E';

                --mmore start 2.8
                v_log_err_msg := '\n\tError: WO11 => ' || SQLERRM || '\n\nRegards\nNETGEAR Support';
                --mmore end 2.8
                l_header_error_flag := 'E';                                                   ---4.1
            END;                                                                                 --3
          /*====Record processing ends here===*/
          ELSE
            v_delivery_id := NULL;
          END IF;

          ---Start 4.1
          IF l_header_error_flag = 'E'
          THEN
            UPDATE xxntgr_ptm_dc_file_header
               SET process_flag = l_header_error_flag
             WHERE delivery_id = v_delivery AND input_file_id = v_input_file_id;
          ELSIF l_sfs_process_flag = 'Y' AND l_p_process_flag = 'Y'
          THEN
            UPDATE xxntgr_ptm_dc_file_header
               SET process_flag = 'PSFS'
             WHERE delivery_id = v_delivery AND input_file_id = v_input_file_id;
          ELSIF l_sfs_process_flag = 'Y' AND l_p_process_flag = 'N'
          THEN
            UPDATE xxntgr_ptm_dc_file_header
               SET process_flag = 'SFS'
             WHERE delivery_id = v_delivery AND input_file_id = v_input_file_id;
          ELSIF l_p_process_flag = 'Y' AND l_sfs_process_flag = 'N'
          THEN
            UPDATE xxntgr_ptm_dc_file_header
               SET process_flag = 'P'
             WHERE delivery_id = v_delivery AND input_file_id = v_input_file_id;
          ELSIF l_header_error_flag = 'N'
          THEN
            DELETE FROM xxntgr_ptm_dc_file_header
                  WHERE delivery_id = v_delivery AND input_file_id = v_input_file_id;
          END IF;

          ---End 4.1

          --COMMIT;
          COMMIT;                                                           -- mmore 3.7 INC00245914
        END LOOP;

        CLOSE get_delivery;
      END;                                                                                       --1
    END IF;

--mmore start 2.8
    IF v_log_err_msg IS NOT NULL
    THEN
      log_error_email
        ('XXNTGR_PTM_DC_SHIP'
        ,'PTM DC Shipment File Import ORAERROR'
        ,    'Hi Team,\n\n\tThere is an error in program PTM DC Shipment File Import.\n\tConcurrent Request ID: '
          || v_reqstid
        , v_file_prsd || v_log_err_msg
        );
      v_log_err_msg := NULL;
    END IF;
--mmore end 2.8
  END xxntgr_ptm_dc_ship;

--------------------------------------------
-- PROCEDURE XXNTGR_PTM_DC_PROCESS_RCPT
--------------------------------------------
  PROCEDURE xxntgr_ptm_dc_process_rcpt (
    errbuf                      OUT      VARCHAR2
   ,retcode                     OUT      VARCHAR2
   ,p_shipment_header_id        IN       NUMBER
   ,p_shipment_line_id          IN       NUMBER
  )
  IS
------
    CURSOR get_rcv_ship_lines
    IS
      SELECT l.shipment_header_id, l.shipment_line_id, h.shipment_num, f.meaning odm_name
            ,m.segment1 item_number, l.quantity_received
        FROM rcv_shipment_headers h
            ,rcv_shipment_lines l
            ,mtl_system_items_b m
            ,fnd_lookup_values f
       WHERE h.shipment_header_id = l.shipment_header_id
         AND l.item_id = m.inventory_item_id
         AND l.to_organization_id = m.organization_id
         AND h.vendor_id = f.lookup_code
         AND f.lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
         AND f.enabled_flag = 'Y'
         AND l.attribute1 = 'U';

    TYPE rcv_lines_type IS TABLE OF get_rcv_ship_lines%ROWTYPE;

    rcv_lines                     rcv_lines_type;
    v_asn                         VARCHAR2 (60);
    v_odm                         VARCHAR2 (60);
    v_item_no                     VARCHAR2 (40);
    v_count                       NUMBER;
    v_upd_count                   NUMBER;
    v_instance_id                 NUMBER;
    v_item_number                 VARCHAR2 (40);
    v_inv_item_id                 NUMBER;
    v_item_rev                    VARCHAR2 (60);
    v_organization_id             NUMBER;
    v_subinv_code                 VARCHAR2 (10);
    v_trans_src_hdr_id            NUMBER;
    v_trans_src_ln_id             NUMBER;
  BEGIN
    IF (p_shipment_header_id IS NOT NULL OR p_shipment_line_id IS NOT NULL)
    THEN
      UPDATE rcv_shipment_lines
         SET attribute1 = 'S'
       WHERE shipment_header_id = NVL (p_shipment_header_id, shipment_header_id)
         AND shipment_line_id = NVL (p_shipment_line_id, shipment_line_id);

      COMMIT;
    END IF;

    OPEN get_rcv_ship_lines;

    LOOP
      FETCH get_rcv_ship_lines
      BULK COLLECT INTO rcv_lines;

      FOR i IN 1 .. rcv_lines.COUNT
      LOOP
        BEGIN
          SELECT DISTINCT asn_number, manufacturer_name, item_number
                     INTO v_asn, v_odm, v_item_no
                     FROM xxntgr_ptm_po_shipment_trx
                    WHERE asn_number = rcv_lines (i).shipment_num
                      AND manufacturer_name = rcv_lines (i).odm_name
                      AND item_number = rcv_lines (i).item_number;

          v_upd_count := 0;

          FOR j IN (SELECT serial_number
                      FROM (SELECT serial_number
                              FROM xxntgr_ptm_po_shipment_trx
                             WHERE asn_number = v_asn
                               AND in_transit = 'Y'
                               AND manufacturer_name = v_odm
                               AND item_number = v_item_no
                               AND ROWNUM <= rcv_lines (i).quantity_received))
          LOOP
            BEGIN
              UPDATE xxntgr_ptm_po_shipment_trx
                 SET in_transit = 'N'
                    ,last_update_date = SYSDATE
               WHERE asn_number = v_asn
                 AND manufacturer_name = v_odm
                 AND item_number = v_item_no
                 AND serial_number = j.serial_number;

              v_upd_count := v_upd_count + SQL%ROWCOUNT;
            END;

            BEGIN
              SELECT trx.instance_id, trx.item_number, trx.inventory_item_id, trx.item_revision
                    ,trx.organization_id, reg.subinventory_code, reg.transaction_source_header_id
                    ,reg.transaction_source_line_id
                INTO v_instance_id, v_item_number, v_inv_item_id, v_item_rev
                    ,v_organization_id, v_subinv_code, v_trans_src_hdr_id
                    ,v_trans_src_ln_id
                FROM xxntgr_ptm_trx_register reg, xxntgr_ptm_po_shipment_trx trx
               WHERE reg.instance_id = trx.instance_id
                 AND trx.asn_number = v_asn
                 AND trx.manufacturer_name = v_odm
                 AND reg.item_number = v_item_no
                 AND reg.serial_number = j.serial_number;

              INSERT INTO xxntgr_ptm_trx_register
                          (transaction_id, instance_id, item_number
                          ,inventory_item_id, item_revision, organization_id, subinventory_code
                          ,serial_number, transaction_type, transaction_source_type
                          ,transaction_source_header_id, transaction_source_line_id, creation_date
                          ,last_update_date
                          )
                   VALUES (xxntgr_ptm_trx_register_s.NEXTVAL, v_instance_id, v_item_number
                          ,v_inv_item_id, v_item_rev, v_organization_id, v_subinv_code
                          ,j.serial_number, '02', 'PO'
                          ,v_trans_src_hdr_id, v_trans_src_ln_id, SYSDATE
                          ,SYSDATE
                          );
            EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                xxntgr_debug_utl.warn ('No data found to insert in register table :' || SQLERRM);
              WHEN OTHERS
              THEN
                ROLLBACK;
                xxntgr_debug_utl.error
                                    (   'Error while getting records to insert in register table :'
                                     || SQLERRM
                                    );
            END;
          END LOOP;

          COMMIT;
          xxntgr_debug_utl.DEBUG ('Total records updated :' || v_upd_count);

          IF v_upd_count = rcv_lines (i).quantity_received
          THEN
            UPDATE rcv_shipment_lines
               SET attribute1 = 'S'
             WHERE shipment_header_id = rcv_lines (i).shipment_header_id
               AND shipment_line_id = rcv_lines (i).shipment_line_id;
          ELSE
            UPDATE rcv_shipment_lines
               SET attribute1 = 'E'
             WHERE shipment_header_id = rcv_lines (i).shipment_header_id
               AND shipment_line_id = rcv_lines (i).shipment_line_id;
          END IF;

          COMMIT;
        EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
            xxntgr_debug_utl.warn ('No data found for receiving lines :' || SQLERRM);
          WHEN OTHERS
          THEN
            xxntgr_debug_utl.error ('Error while getting PTM data for receiving lines :' || SQLERRM);
        END;
      END LOOP;

      EXIT WHEN get_rcv_ship_lines%NOTFOUND;
    END LOOP;

    CLOSE get_rcv_ship_lines;
  END;

-------
  PROCEDURE xxntgr_ptm_dc_delmiss (
    errbuf                      OUT      VARCHAR2
   ,retcode                     OUT      VARCHAR2
   ,p_delivery_id               IN       VARCHAR2
   ,p_delete_delivery           IN       VARCHAR2
   ,p_summary_email             IN       VARCHAR2
  )
  AS
----
---Get new shipments from shipment header table to insert into notification table
    CURSOR get_new_deliveries
    IS
      SELECT DISTINCT nd.delivery_id, nd.organization_id
                     ,TO_CHAR (nd.creation_date, 'DD-MON-YYYY') creation_date, NULL shipped_date
                 FROM wsh_delivery_details dd
                     ,wsh_new_deliveries nd
                     ,wsh_delivery_assignments da
                     ,mtl_system_items_b mst
                     ,fnd_lookup_values flv
                WHERE 1 = 1
                  AND nd.delivery_id = da.delivery_id
                  AND dd.delivery_detail_id = da.delivery_detail_id
                  AND dd.inventory_item_id = mst.inventory_item_id
                  AND dd.organization_id = mst.organization_id
                  --AND mst.item_type IN (SELECT lookup_code FROM fnd_lookup_values WHERE lookup_type = 'XXNTGR_PTM_ODM_ITEM_TYPE' AND enabled_flag='Y')
                  AND nd.delivery_id NOT IN (SELECT delivery_id
                                               FROM xxntgr_ptm_dc_ship_notif)
                  AND dd.organization_id = flv.lookup_code
                  AND flv.lookup_type = 'XXNTGR_PTM_DC_NAMES'
                  AND flv.enabled_flag = 'Y'
                  AND nd.delivery_id >
                        (SELECT TO_NUMBER (description)
                           FROM fnd_lookup_values
                          WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                            AND lookup_code = 'LAST_DELIVERY_ID'
                            AND enabled_flag = 'Y')
                  AND NVL (dd.shipped_quantity, 0) > 0
             ORDER BY nd.delivery_id;

---Cursor for summary mail
    CURSOR group_delv_by_dc
    IS
      SELECT DISTINCT notif.organization_id
                 FROM xxntgr_ptm_dc_ship_notif notif, wsh_new_deliveries nd
                WHERE notif.delivery_id = nd.delivery_id
                  AND end_date_active IS NULL
                  AND TRUNC (SYSDATE) >
                        (  NVL (notif.shipped_date, nd.last_update_date)
                         + (SELECT TO_NUMBER (description)
                              FROM fnd_lookup_values
                             WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                               AND lookup_code = 'DC_NOTIFY_DELAY'
                               AND enabled_flag = 'Y')
                        );

    CURSOR send_summary_mail (
      p_dc                                 NUMBER
    )
    IS
      SELECT   notif.*, nd.last_update_date delv_last_update_date
          FROM xxntgr_ptm_dc_ship_notif notif, wsh_new_deliveries nd
         WHERE notif.delivery_id = nd.delivery_id
           AND notif.organization_id = p_dc
           AND end_date_active IS NULL
           AND TRUNC (SYSDATE) >
                 (  NVL (notif.shipped_date, nd.last_update_date)
                  + (SELECT TO_NUMBER (description)
                       FROM fnd_lookup_values
                      WHERE lookup_type = 'XXNTGR_PTM_COMMON'
                        AND lookup_code = 'DC_NOTIFY_DELAY'
                        AND enabled_flag = 'Y')
                 )
      ORDER BY shipped_date;

    CURSOR chk_delv_befor_insert (
      p_delv_id                            NUMBER
     ,p_dc_id                              NUMBER
    )
    IS
      SELECT DISTINCT delivery_id
                 FROM xxntgr_ptm_so_shipment_trx
                WHERE delivery_id = p_delv_id
                  AND UPPER (dc_name) =
                        (SELECT meaning
                           FROM fnd_lookup_values
                          WHERE lookup_type = 'XXNTGR_PTM_DC_NAMES'
                            AND lookup_code = TO_CHAR (p_dc_id)
                            AND enabled_flag = 'Y');

    CURSOR chk_delv_befor_notif (
      p_delv_id                            NUMBER
     ,p_dc                                 VARCHAR2
    )
    IS
      SELECT DISTINCT delivery_id
                 FROM xxntgr_ptm_so_shipment_trx
                WHERE delivery_id = p_delv_id AND UPPER (dc_name) = p_dc;

----
    v_last_delv_id                NUMBER;
    v_delivery                    NUMBER;
    v_delv_id                     NUMBER;
    v_delivery_id                 NUMBER;
    --v_notify_days0    NUMBER;
    --v_notify_days1    NUMBER;
    --v_notify_days2    NUMBER;
    --v_notify_days3    NUMBER;
    --v_notify_delay    NUMBER;
    v_mailid                      VARCHAR2 (1000);
    v_notification                VARCHAR2 (32000);
    v_dc_id                       NUMBER;
    v_email_count                 NUMBER;
    v_get_mail_error              VARCHAR2 (32000);
    v_dc_name                     VARCHAR2 (60);
    v_table_data                  VARCHAR2 (31800);
    v_length                      NUMBER;
    v_smail_count                 VARCHAR2 (10);
  BEGIN
    v_last_delv_id := NULL;

    FOR i IN get_new_deliveries
    LOOP
      v_delivery := NULL;

      --Check if delivery exists in XXNTGR_PTM_SO_SHIPMENT_TRX table
      BEGIN
        OPEN chk_delv_befor_insert (i.delivery_id, i.organization_id);

        FETCH chk_delv_befor_insert
         INTO v_delivery;

        CLOSE chk_delv_befor_insert;

        IF v_delivery IS NULL
        THEN
          --Check if delivery exists in notificatin table
          BEGIN
            SELECT DISTINCT delivery_id
                       INTO v_delivery
                       FROM xxntgr_ptm_dc_ship_notif
                      WHERE delivery_id = i.delivery_id AND organization_id = i.organization_id;
          EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
              INSERT INTO xxntgr_ptm_dc_ship_notif
                   VALUES (i.delivery_id,                                             --delivery_id
                                         i.organization_id,                      -- organization_id
                                                           i.creation_date,     --START_DATE_ACTIVE
                                                                           NULL,  --END_DATE_ACTIVE
                                                                                0,    --EMAIL_COUNT
                                                                                  i.shipped_date
                          ,                                                          --SHIPPED_DATE
                           2500,                                                       --CREATED_BY
                                SYSDATE,                                            --CREATION_DATE
                                        2500,                                     --LAST_UPDATED_BY
                                             SYSDATE);                           --LAST_UPDATE_DATE

              COMMIT;
            WHEN OTHERS
            THEN
              xxntgr_debug_utl.error
                          (   'DELIVERY: '
                           || i.delivery_id
                           || ' Error while checking for existing delivery in notification table: '
                           || SQLCODE
                           || SQLERRM
                          );
          END;
        END IF;
      EXCEPTION
        WHEN OTHERS
        THEN
          NULL;
          xxntgr_debug_utl.error
                          (   'DELIVERY: '
                           || i.delivery_id
                           || ' Error while checking for existing delivery in shipment_trx table: '
                           || SQLCODE
                           || SQLERRM
                          );
      END;

      v_last_delv_id := i.delivery_id;
    END LOOP;

    --UPDATE common lookup table with the last delivery_id
    IF v_last_delv_id IS NOT NULL
    THEN
      BEGIN
        UPDATE fnd_lookup_values
           SET description = v_last_delv_id
         WHERE lookup_type = 'XXNTGR_PTM_COMMON'
           AND lookup_code = 'LAST_DELIVERY_ID'
           AND enabled_flag = 'Y';

        COMMIT;
      EXCEPTION
        WHEN OTHERS
        THEN
          xxntgr_debug_utl.error ('Error while getting last delivery_id: ' || SQLCODE || SQLERRM);
      END;
    END IF;

    --Send notification mail process starts

    --If DELIVERY_ID is passed as INPUT paramter then delete/disable that DELIVERY in notification table, send mail and exit program
    IF p_delivery_id IS NOT NULL
    THEN
      IF p_delete_delivery = 'Y'
      THEN
        DELETE FROM xxntgr_ptm_dc_ship_notif
              WHERE delivery_id = p_delivery_id;

        COMMIT;
      ELSE
        UPDATE xxntgr_ptm_dc_ship_notif
           SET end_date_active = SYSDATE
         WHERE delivery_id = p_delivery_id;

        COMMIT;
      END IF;
    --END IF;
    ELSE           --loop on notification table to send notifications for all delayes shipment files
      IF p_summary_email = 'Y'
      THEN
        BEGIN
          FOR dist_dc IN group_delv_by_dc
          LOOP
            BEGIN
              SELECT description
                INTO v_dc_name
                FROM fnd_lookup_values
               WHERE lookup_type = 'XXNTGR_PTM_DC_NAMES'
                 AND lookup_code = TO_CHAR (dist_dc.organization_id)
                 AND enabled_flag = 'Y';
            EXCEPTION
              WHEN OTHERS
              THEN
                xxntgr_debug_utl.error ('Error while geting DC name: ' || SQLCODE || SQLERRM);
            END;

            v_table_data := NULL;
            v_length := NULL;

            FOR summ_mail IN send_summary_mail (dist_dc.organization_id)
            LOOP
              BEGIN
                OPEN chk_delv_befor_notif (summ_mail.delivery_id, v_dc_name);

                FETCH chk_delv_befor_notif
                 INTO v_delivery_id;

                CLOSE chk_delv_befor_notif;

                IF v_delivery_id IS NOT NULL
                THEN
                  UPDATE xxntgr_ptm_dc_ship_notif
                     SET end_date_active = SYSDATE
                   WHERE delivery_id = summ_mail.delivery_id
                     AND organization_id = dist_dc.organization_id;

                  COMMIT;
                ELSE
                  BEGIN
                    SELECT summ_mail.email_count + 1
                      INTO v_smail_count
                      FROM DUAL;

                    v_table_data :=
                         v_table_data
                      || '<tr><td>'
                      || summ_mail.delivery_id
                      || '</td><td>'
                      || TO_CHAR (NVL (summ_mail.shipped_date, summ_mail.delv_last_update_date)
                                 ,'DD-MON-YYYY'
                                 )
                      || '</td><td>'
                      || TRUNC (  SYSDATE
                                - NVL (summ_mail.shipped_date, summ_mail.delv_last_update_date)
                               )
                      || '</td><td>'
                      || v_smail_count
                      || '</td></tr>';
                  EXCEPTION
                    WHEN OTHERS
                    THEN
                      ROLLBACK;
                      xxntgr_debug_utl.error (   'Error while assigning value to v_table_data: '
                                              || SQLCODE
                                              || SQLERRM
                                             );
                  END;

                  UPDATE xxntgr_ptm_dc_ship_notif
                     SET email_count = email_count + 1
                        ,last_update_date = SYSDATE
                   WHERE delivery_id = summ_mail.delivery_id
                     AND organization_id = dist_dc.organization_id;

                  IF summ_mail.shipped_date IS NULL
                  THEN
                    UPDATE xxntgr_ptm_dc_ship_notif
                       SET shipped_date = summ_mail.delv_last_update_date
                     WHERE delivery_id = summ_mail.delivery_id
                       AND organization_id = dist_dc.organization_id;
                  END IF;
                END IF;
              EXCEPTION
                WHEN OTHERS
                THEN
                  xxntgr_debug_utl.error
                                    (   'Error while checking for existing delivery in trx table: '
                                     || SQLCODE
                                     || SQLERRM
                                    );
              END;
            END LOOP;

            v_length := NVL (LENGTH (v_table_data), 0);

            --Send notification mail to DC
            IF v_length < 1500
            THEN
              IF v_length <> 0
              THEN
                BEGIN
                  xxntgr_ptm_odm_pkg.dcfile_delay_summ_mail (v_dc_name
                                                            ,v_table_data
                                                            ,v_get_mail_error
                                                            );
                  xxntgr_debug_utl.error (   'File delay notification to '
                                          || v_dc_name
                                          || ' '
                                          || v_get_mail_error
                                         );
                EXCEPTION
                  WHEN OTHERS
                  THEN
                    xxntgr_debug_utl.error ('Error while calling mail program: ' || SQLCODE
                                            || SQLERRM
                                           );
                END;
              END IF;
            ELSE
              --get mail id's to send mail
              BEGIN
                SELECT description
                  INTO v_mailid
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
                   AND enabled_flag = 'Y'
                   AND SUBSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1))
                              , (INSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1)), '_')
                                 + 1
                                )
                              ) =
                         (SELECT lookup_code
                            FROM fnd_lookup_values
                           WHERE lookup_type = 'XXNTGR_PTM_DC_NAMES'
                             AND description = UPPER (v_dc_name)
                             AND enabled_flag = 'Y');
              EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                  SELECT description
                    INTO v_mailid
                    FROM fnd_lookup_values
                   WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
                     AND lookup_code = 'ODM_SHIPMENT1'
                     AND enabled_flag = 'Y';
                WHEN OTHERS
                THEN
                  xxntgr_debug_utl.error ('Error while getting DC mail id: ' || SQLCODE || SQLERRM);
              END;

              --Send Mail
              BEGIN
                xxntgr_utl_mail.send
                  (p_sender                     => 'donotreply@netgear.com'
                  ,p_recipients                 => v_mailid
                  ,p_cc                         => NULL
                  ,p_bcc                        => NULL
                  ,p_subject                    =>    'PTM DC Interface ('
                                                   || v_dc_name
                                                   || ') - Serial Number File DELAYED!!'
                  ,p_message                    =>    'This message is intended to notify all concerned parties that Netgear has not received Serial Number files for following
                              Deliveries:
                              <br><br>
                              <table border="1">
                              <tr bgcolor="gray">
                              <td>Delivery</td>
                              <td>Shipped Date</td>
                              <td>Days Delayed</td>
                              <td>Notification#</td>
                              </tr>'
                                                   || v_table_data
                                                   || '</table>
                              <br><br>
                              Delay in delivery of this file has an adverse effect on Netgears Sales Order fulfillment process.<br><br>PLEASE TAKE CORRECTIVE MEASURES AT THE EARLIEST.
                              <br><br>
                              - Netgear IT'
                  ,p_mime_type                  => 'text/html; charset=us-ascii'
                  );
              EXCEPTION
                WHEN OTHERS
                THEN
                  ROLLBACK;
                  xxntgr_debug_utl.error ('Error while sending mail : ' || SQLCODE || SQLERRM);
                  retcode := 2;
              END;
            END IF;
          END LOOP;

          COMMIT;
        EXCEPTION
          WHEN OTHERS
          THEN
            ROLLBACK;
            xxntgr_debug_utl.error ('Error in summary mail program: ' || SQLCODE || SQLERRM);
            retcode := 2;
        END;
      END IF;
    END IF;
  END;

------Mail Programs
  PROCEDURE ptm_odm_ship_mail (
    p_asn_number                         VARCHAR2
   ,p_manufacturer                       VARCHAR2
   ,p_file_name                          VARCHAR2
   ,p_reason                             VARCHAR2
   ,p_get_error                 OUT      VARCHAR2
  )
  IS
    ----
    v_error_message               VARCHAR2 (32000);
    v_mailid                      VARCHAR2 (1000);
    v_sku                         VARCHAR2 (100);
  ----
  BEGIN
    --get mail id's to send mail, in case of validation error
    BEGIN
      SELECT description
        INTO v_mailid
        FROM fnd_lookup_values
       WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
         AND enabled_flag = 'Y'
         AND SUBSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1))
                    , (INSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1)), '_') + 1)
                    ) =
               (SELECT lookup_code
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                   AND meaning = UPPER (p_manufacturer)
                   AND enabled_flag = 'Y');
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        SELECT description
          INTO v_mailid
          FROM fnd_lookup_values
         WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
           AND lookup_code = 'ODM_SHIPMENT1'
           AND enabled_flag = 'Y';
      WHEN OTHERS
      THEN
        --XXNTGR_DEBUG_UTL.LOG('Error while getting manufacturer mail id: '||SQLCODE||SQLERRM,'1');
        p_get_error := 'Error while getting manufacturer mail id: ' || SQLCODE || SQLERRM;
    END;

    BEGIN
      SELECT DISTINCT item_number
                 INTO v_sku
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_number AND odm_name = p_manufacturer;
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;

    --get mail body from FND message
    BEGIN
      fnd_message.set_name ('XXNTGR', 'XXNTGR_PTM_ODMBIZERR_EMAIL');
      --Set the INVOICE,MANUFACTURER and REASON message token with the current values
      fnd_message.set_token ('FILENAME', p_file_name);
      fnd_message.set_token ('INVOICE', p_asn_number);
      fnd_message.set_token ('SKU', v_sku);
      fnd_message.set_token ('MANUFACTURER', p_manufacturer);
      fnd_message.set_token ('REASON', p_reason);
      --Retrieve the message
      v_error_message := fnd_message.get;
    EXCEPTION
      WHEN OTHERS
      THEN
        p_get_error := 'Error while setting value for FND MESSAGE: ' || SQLCODE || SQLERRM;
    END;

    BEGIN
      xxntgr_utl_mail.send
          (p_sender                     => 'donotreply@netgear.com'
          ,p_recipients                 => v_mailid
          ,p_cc                         => NULL
          ,p_bcc                        => NULL
          ,p_subject                    =>    'PTM ODM Interface - Serial Number file validation failed! - Invoice# '
                                           || p_asn_number
          ,p_message                    => v_error_message
          ,p_mime_type                  => 'text/html; charset=us-ascii'
          );
    EXCEPTION
      WHEN OTHERS
      THEN
        p_get_error := 'Error while sending mail: ' || SQLCODE || SQLERRM;
    END;
  END;

--------------------------------------------------
--PROCEDURE PTM_ODM_SUCCESS_MAIL_NOTIF
--------------------------------------------------
  PROCEDURE ptm_odm_success_mail_notif (
    p_asn_number                         VARCHAR2
   ,p_manufacturer                       VARCHAR2
   ,p_file_name                          VARCHAR2
   ,p_reason                             VARCHAR2
   ,p_get_error                 OUT      VARCHAR2
  )
  IS
    ----
    v_error_message               VARCHAR2 (32000);
    v_mailid                      VARCHAR2 (1000);
    v_sku                         VARCHAR2 (100);
  ----
  BEGIN
    --get mail id's to send successful notification mail
    BEGIN
      SELECT description
        INTO v_mailid
        FROM fnd_lookup_values
       WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
         AND enabled_flag = 'Y'
         AND SUBSTR (lookup_code, 1, 11) = 'ODM_SUCCESS'
         AND SUBSTR (lookup_code, INSTR (lookup_code, '_', -1) + 1) =
               (SELECT lookup_code
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                   AND meaning = UPPER (p_manufacturer)
                   AND enabled_flag = 'Y');
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        SELECT description
          INTO v_mailid
          FROM fnd_lookup_values
         WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
           AND lookup_code = 'ODM_SHIPMENT1'
           AND enabled_flag = 'Y';
      WHEN OTHERS
      THEN
        --XXNTGR_DEBUG_UTL.LOG('Error while getting manufacturer mail id: '||SQLCODE||SQLERRM,'1');
        p_get_error := 'Error while getting manufacturer mail id: ' || SQLCODE || SQLERRM;
    END;

    BEGIN
      SELECT DISTINCT item_number
                 INTO v_sku
                 FROM xxntgr_ptm_odm_shipment_stg
                WHERE asn_number = p_asn_number AND odm_name = p_manufacturer;
    EXCEPTION
      WHEN OTHERS
      THEN
        --P_GET_ERROR := 'Error while getting SKU from staging: '||SQLCODE||SQLERRM; -- mmore 3.4 to fix ORA-01422 error commented below code
        NULL;                                                                          -- mmore 3.4
    END;

    --get mail body from FND message
    BEGIN
      fnd_message.set_name ('XXNTGR', 'XXNTGR_PTM_ODMBIZSUCCESS_EMAIL');
      --Set the INVOICE,MANUFACTURER and REASON message token with the current values
      fnd_message.set_token ('FILENAME', p_file_name);
      fnd_message.set_token ('INVOICE', p_asn_number);
      fnd_message.set_token ('SKU', v_sku);
      fnd_message.set_token ('MANUFACTURER', p_manufacturer);
      --Retrieve the message
      v_error_message := fnd_message.get;
    EXCEPTION
      WHEN OTHERS
      THEN
        p_get_error := 'Error while setting value for FND MESSAGE: ' || SQLCODE || SQLERRM;
    END;

    BEGIN
      xxntgr_utl_mail.send (p_sender                     => 'donotreply@netgear.com'
                           ,p_recipients                 => v_mailid
                           ,p_cc                         => NULL
                           ,p_bcc                        => NULL
                           ,p_subject                    =>    'PTM ODM - SN File SUCCESSFULLY PROCESSED - Invoice# '
                                                            || p_asn_number
                           ,p_message                    => v_error_message
                           ,p_mime_type                  => 'text/html; charset=us-ascii'
                           );
    EXCEPTION
      WHEN OTHERS
      THEN
        p_get_error := 'Error while sending mail: ' || SQLCODE || SQLERRM;
    END;
  END ptm_odm_success_mail_notif;

---------------------------------------------------------------------------
-- PROCEDURE FILE_DELAY_MAIL
---------------------------------------------------------------------------
  PROCEDURE file_delay_mail (
    p_asn_number                         VARCHAR2
   ,p_manufacturer                       VARCHAR2
   ,p_email_count                        NUMBER
   ,p_get_error                 OUT      VARCHAR2
  )
  IS
    v_vendor_id                   NUMBER;
    v_email_count                 NUMBER;
    v_manufacturer                VARCHAR2 (60);
    v_mailid                      VARCHAR2 (1000);
    v_notification                VARCHAR2 (32000);
  BEGIN
    BEGIN
      SELECT description
        INTO v_mailid
        FROM fnd_lookup_values
       WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
         AND enabled_flag = 'Y'
         AND SUBSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1))
                    , (INSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1)), '_') + 1)
                    ) =
               (SELECT lookup_code
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                   AND meaning = UPPER (p_manufacturer)
                   AND enabled_flag = 'Y');
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        SELECT description
          INTO v_mailid
          FROM fnd_lookup_values
         WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
           AND lookup_code = 'ODM_SHIPMENT1'
           AND enabled_flag = 'Y';
      WHEN OTHERS
      THEN
        p_get_error := 'Error while getting ODM mail id: ' || SQLCODE || SQLERRM;
    END;

    --get mail body from FND message
    BEGIN
      fnd_message.set_name ('XXNTGR', 'XXNTGR_PTM_ODMFILEDELAY_EMAIL');
      --Set the INVOICE,MANUFACTURER and NOTIFICATION NUMNER, token with the current values
      fnd_message.set_token ('INVOICE', p_asn_number);
      fnd_message.set_token ('MANUFACTURER', p_manufacturer);
      fnd_message.set_token ('NOTIFY', p_email_count + 1);
      --Retrieve the message
      v_notification := fnd_message.get;
    EXCEPTION
      WHEN OTHERS
      THEN
        p_get_error := 'Error while setting value for FND MESSAGE: ' || SQLCODE || SQLERRM;
    END;

    BEGIN
      xxntgr_utl_mail.send
                     (p_sender                     => 'donotreply@netgear.com'
                     ,p_recipients                 => v_mailid
                     ,p_cc                         => NULL
                     ,p_bcc                        => NULL
                     ,p_subject                    =>    'PTM ODM Interface - Serial Number file DELAYED! - Invoice#'
                                                      || p_asn_number
                                                      || '! - Notification ('
                                                      || TO_CHAR (p_email_count + 1)
                                                      || ')'
                     ,p_message                    => v_notification
                     ,p_mime_type                  => 'text/html; charset=us-ascii'
                     );
    EXCEPTION
      WHEN OTHERS
      THEN
        p_get_error := 'Error while sending mail: ' || SQLCODE || SQLERRM;
    END;
  END;

  PROCEDURE file_delay_summ_mail (
    p_odm_name                           VARCHAR2
   ,p_table_data                         VARCHAR2
   ,p_get_error                 OUT      VARCHAR2
  )
  IS
    v_mailid                      VARCHAR2 (1000);
    v_notification                VARCHAR2 (32000);
  BEGIN
    BEGIN
      SELECT description
        INTO v_mailid
        FROM fnd_lookup_values
       WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
         AND enabled_flag = 'Y'
         AND SUBSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1))
                    , (INSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1)), '_') + 1)
                    ) =
               (SELECT lookup_code
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXNTGR_PTM_PILOT_ODM_NAMES'
                   AND meaning = UPPER (p_odm_name)
                   AND enabled_flag = 'Y');
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        SELECT description
          INTO v_mailid
          FROM fnd_lookup_values
         WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
           AND lookup_code = 'ODM_SHIPMENT1'
           AND enabled_flag = 'Y';
      WHEN OTHERS
      THEN
        p_get_error := 'Error while getting ODM mail id: ' || SQLCODE || SQLERRM;

        SELECT description
          INTO v_mailid
          FROM fnd_lookup_values
         WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
           AND lookup_code = 'ODM_SHIPMENT1'
           AND enabled_flag = 'Y';
    END;

    --get mail body from FND message
    BEGIN
      fnd_message.set_name ('XXNTGR', 'XXNTGR_PTM_ODMFILEDELAY_SEMAIL');
      fnd_message.set_token ('TABLEDATA', p_table_data);
      --Retrieve the message
      v_notification := fnd_message.get;
    EXCEPTION
      WHEN OTHERS
      THEN
        v_notification := NULL;
        p_get_error := 'Error while setting value for FND MESSAGE: ' || SQLCODE || SQLERRM;
    END;

    IF v_notification IS NOT NULL
    THEN
      BEGIN
        xxntgr_utl_mail.send (p_sender                     => 'donotreply@netgear.com'
                             ,p_recipients                 => v_mailid
                             ,p_cc                         => NULL
                             ,p_bcc                        => NULL
                             ,p_subject                    =>    'PTM ODM Interface ('
                                                              || p_odm_name
                                                              || ') - Serial Number File DELAYED!!'
                             ,p_message                    => v_notification
                             ,p_mime_type                  => 'text/html; charset=us-ascii'
                             );
      EXCEPTION
        WHEN OTHERS
        THEN
          p_get_error := 'Error while sending mail : ' || SQLCODE || SQLERRM;
      END;
    END IF;
  END;

------------
  PROCEDURE ptm_dc_ship_mail (
    p_delivery_id                        VARCHAR2
   ,p_dc_name                            VARCHAR2
   ,p_file_name                          VARCHAR2
   ,p_reason                             VARCHAR2
   ,p_get_error                 OUT      VARCHAR2
  )
  IS
    ----
    v_error_message               VARCHAR2 (32000);
    v_mailid                      VARCHAR2 (1000);
    v_bill_to_customer            VARCHAR2 (100);
  ----
  BEGIN
--get mail id's to send mail, in case of validation error
     ------- Change Done for incident INC00537942 IF ORA Error then Send Email PTM Support Team.

		IF INSTR(p_reason,'ORA-0') > 0 THEN
			--v_mailid := 'abhatka@netgear.com';
			v_mailid := 'NTGRPTMSupport@netgear.com';
		ELSE

				BEGIN

				  SELECT description
					INTO v_mailid
					FROM fnd_lookup_values
				   WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
					 AND enabled_flag = 'Y'
					 AND lookup_code LIKE '%DC%SHIPMENT%'
					 AND SUBSTR (lookup_code, 13) =
						   (SELECT lookup_code
							  FROM fnd_lookup_values
							 WHERE description = UPPER (p_dc_name)
							   AND lookup_type = 'XXNTGR_PTM_DC_NAMES'
							   AND enabled_flag = 'Y');
				EXCEPTION
				  WHEN NO_DATA_FOUND
				  THEN
					SELECT description
					  INTO v_mailid
					  FROM fnd_lookup_values
					 WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
					   AND lookup_code = 'ODM_SHIPMENT1'
					   AND enabled_flag = 'Y';
				  WHEN OTHERS
				  THEN
					p_get_error := 'Error while getting DC mail id: ' || SQLCODE || SQLERRM;

			END;
		END IF;
/*  --get Bill_To_Customer_name --*/
    BEGIN
      SELECT DISTINCT hca.account_name
                 INTO v_bill_to_customer
                 FROM wsh_delivery_details dd
                     ,wsh_new_deliveries nd
                     ,wsh_delivery_assignments da
                     ,apps.hz_cust_accounts_all hca
                --, xxntgr_apps.XXNTGR_EDI_CUST_EXTRACT_LIST cst
      WHERE           1 = 1
                  AND nd.delivery_id = da.delivery_id
                  AND dd.delivery_detail_id = da.delivery_detail_id
                  AND hca.cust_account_id = dd.customer_id
                  -- AND dd.customer_id = cst.BILL_TO_CUST_ORIG_SYS_REF
                  -- AND hca.cust_account_id = cst.BILL_TO_CUST_ORIG_SYS_REF
                  AND nd.delivery_id = p_delivery_id;
    EXCEPTION
      WHEN OTHERS
      THEN
        xxntgr_debug_utl.error (   'Error while fetching the Bill_To_Customer_name for delivery#'
                                || p_delivery_id
                                || ' '
                                || SQLCODE
                                || SQLERRM
                               );
    END;

    --get mail body from FND message
    BEGIN
      fnd_message.set_name ('XXNTGR', 'XXNTGR_PTM_DCBIZERR_EMAIL');
      --Set the DELIVERY,DC and REASON message token with the current values
      fnd_message.set_token ('FILENAME', p_file_name);
      fnd_message.set_token ('DELIVERY', p_delivery_id);
      fnd_message.set_token ('DCNAME', p_dc_name);
      fnd_message.set_token ('REASON', p_reason);
      --Retrieve the message
      v_error_message := fnd_message.get;
    EXCEPTION
      WHEN OTHERS
      THEN
        p_get_error := 'Error while setting value for FND MESSAGE: ' || SQLCODE || SQLERRM;
    END;

    BEGIN
      xxntgr_utl_mail.send (p_sender                     => 'donotreply@netgear.com'
                           ,p_recipients                 => v_mailid
                           ,p_cc                         => NULL
                           ,p_bcc                        => NULL
                           ,p_subject                    =>    'PTM '
                                                            || p_dc_name
                                                            || ' DC Shipment - FAILURE - DeliveryID: '
                                                            || p_delivery_id
                                                            || '  Customer: '
                                                            || v_bill_to_customer
                           ,p_message                    => v_error_message
                           ,p_mime_type                  => 'text/html; charset=us-ascii'
                           );
    EXCEPTION
      WHEN OTHERS
      THEN
        p_get_error := 'Error while sending mail: ' || SQLCODE || SQLERRM;
    END;
  END;

---------
  PROCEDURE dcfile_delay_summ_mail (
    p_dc_name                            VARCHAR2
   ,p_table_data                         VARCHAR2
   ,p_get_error                 OUT      VARCHAR2
  )
  IS
    v_mailid                      VARCHAR2 (1000);
    v_notification                VARCHAR2 (32000);
  BEGIN
    BEGIN
      SELECT description
        INTO v_mailid
        FROM fnd_lookup_values
       WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
         AND enabled_flag = 'Y'
         AND SUBSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1))
                    , (INSTR (SUBSTR (lookup_code, (INSTR (lookup_code, '_') + 1)), '_') + 1)
                    ) =
               (SELECT lookup_code
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXNTGR_PTM_DC_NAMES'
                   AND description = UPPER (p_dc_name)
                   AND enabled_flag = 'Y');
    EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        SELECT description
          INTO v_mailid
          FROM fnd_lookup_values
         WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
           AND lookup_code = 'ODM_SHIPMENT1'
           AND enabled_flag = 'Y';
      WHEN OTHERS
      THEN
        p_get_error := 'Error while getting DC mail id: ' || SQLCODE || SQLERRM;

        SELECT description
          INTO v_mailid
          FROM fnd_lookup_values
         WHERE lookup_type = 'XXNTGR_PTM_EMAIL_NOTIFICATION'
           AND lookup_code = 'ODM_SHIPMENT1'
           AND enabled_flag = 'Y';
    END;

    --get mail body from FND message
    BEGIN
      fnd_message.set_name ('XXNTGR', 'XXNTGR_PTM_DCFILEDELAY_SEMAIL');
      fnd_message.set_token ('TABLEDATA', p_table_data);
      --Retrieve the message
      v_notification := fnd_message.get;
    EXCEPTION
      WHEN OTHERS
      THEN
        v_notification := NULL;
        p_get_error := 'Error while setting value for FND MESSAGE: ' || SQLCODE || SQLERRM;
    END;

    IF v_notification IS NOT NULL
    THEN
      BEGIN
        xxntgr_utl_mail.send (p_sender                     => 'donotreply@netgear.com'
                             ,p_recipients                 => v_mailid
                             ,p_cc                         => NULL
                             ,p_bcc                        => NULL
                             ,p_subject                    =>    'PTM DC Interface ('
                                                              || p_dc_name
                                                              || ')-Serial Number File DELAYED'
                             ,p_message                    => v_notification
                             ,p_mime_type                  => 'text/html; charset=us-ascii'
                             );
      EXCEPTION
        WHEN OTHERS
        THEN
          p_get_error := 'Error while sending mail : ' || SQLCODE || SQLERRM;
      END;
    END IF;
  END;

-----
--mmore start 2.8
  PROCEDURE log_error_email (
    p_program                   IN       VARCHAR2
   ,p_email_subject             IN       VARCHAR2
   ,p_email_body1               IN       VARCHAR2
   ,p_email_body2               IN       VARCHAR2
  )
  IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    v_location                    VARCHAR2 (4);
    v_instance                    VARCHAR2 (10);
    v_request_id                  NUMBER;
    v_email_address               VARCHAR2 (2000);
    v_user                        NUMBER := fnd_global.user_id;              --added by ppatil 3.13
  BEGIN
    v_location := 'V001';

    SELECT db_unique_name
      INTO v_instance
      FROM v$database;

    v_location := 'V002';

    --IF p_program = 'XXNTGR_PTM_DC_SHIP' THEN -- mmore 3.4
    IF p_program IN ('XXNTGR_PTM_DC_SHIP', 'XXNTGR_PTM_ODM_SHIP')
    THEN                                                                                -- mmore 3.4
      IF v_instance = 'EPROD'
      THEN
        v_email_address := 'NTGRPTMSupport@netgear.com';
      ELSE
        /* added by ppatil 3.13 start*/
        BEGIN
          SELECT email_address
            INTO v_email_address
            FROM apps.fnd_user
           WHERE user_id = v_user;
        EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
            v_email_address := 'NTGRPTMSupport@netgear.com';
        END;
      /* added by ppatil 3.13 end*/
      END IF;
    END IF;

    v_location := 'V003';
    v_request_id :=
      fnd_request.submit_request (application                  => 'XXNTGR'
                                 ,program                      => 'XXNTGRSENDEMAILMUT'
                                 ,description                  => NULL
                                 ,start_time                   => NULL
                                 ,sub_request                  => FALSE
                                 ,argument1                    => NULL
                                 ,argument2                    => NULL
                                 ,argument3                    => p_email_subject
                                 ,argument4                    => v_email_address
                                 ,argument5                    => 'commonemailbody.txt'
                                 ,argument6                    => NULL
                                 ,argument7                    => NULL
                                 ,argument8                    => NULL
                                 ,argument9                    => NULL
                                 ,argument10                   => NULL
                                 ,argument11                   => NULL
                                 ,argument12                   => '3'
                                 ,argument13                   => NULL
                                 ,argument14                   => p_email_body1
                                 ,argument15                   => p_email_body2
                                 );
    v_location := 'V004';
    COMMIT;
    xxntgr_debug_utl.LOG ('xxntgr_ptm_odm_pkg.xxntgr_log_errors.v_request_id= ' || v_request_id);
  EXCEPTION
    WHEN OTHERS
    THEN
      xxntgr_debug_utl.LOG (   'Error while submitting mail request at location '
                            || v_location
                            || '. Error: '
                            || SQLCODE
                            || ' => '
                            || SQLERRM
                           ,'1'
                           );
  END log_error_email;
--mmore end 2.8
END xxntgr_ptm_odm_pkg;
/