ALTER TABLE xxntgr_apps.xxntgr_ptm_ac_ff_stg
ADD (RFID VARCHAR(250 BYTE));

ALTER TABLE xxntgr_apps.xxntgr_ptm_odm_shipment_stg
ADD (RFID VARCHAR(250 BYTE));

ALTER TABLE xxntgr_apps.xxntgr_ptm_item_instances
ADD (RFID VARCHAR(250 BYTE));

ALTER TABLE xxntgr_apps.xxntgr_ptm_odm_shipment_arch
ADD (RFID VARCHAR(250 BYTE));